#!/bin/sh
##  qsub -cwd -pe orte 4 ./pyjob
#mpiexec -np 2 python3 /shared/therobot/evostrat_btg#ym.py

#$ -cwd
#$ -N helloworld
#$ -pe mpi 3
#$ -j y
date
/usr/bin/mpirun --mca orte_base_help_aggregate 0 ./mpi_test.py > hello_all.out
