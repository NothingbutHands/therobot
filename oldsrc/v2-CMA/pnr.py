#!/usr/bin/python
# poll and render 
import subprocess
import sys

cluster_name = str(sys.argv[1])
name = str(sys.argv[2])

output = subprocess.check_output('pcluster ssh {} --dryrun'.format(cluster_name), shell=True).decode('utf-8')
ip = output.split('@')[1].split(' ')[0]
subprocess.call('./poll_and_render.sh {} {}'.format(ip, name), shell=True)
