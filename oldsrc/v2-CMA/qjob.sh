#!/bin/sh
# The #$ lines is called bowtie (a qsub thing)

#$ -cwd
#$ -N btevo
#$ -pe mpi 5
#$ -o btevo_output.log
#$ -e btevo_error.log
#$ -r y
date
/usr/bin/mpirun --mca orte_base_help_aggregate 0 --tag-output ./slave.py  >> evo.log
