#!/usr/bin/env bash

while [ 1 ]
do
if [[ -f /shared/kill_slaves ]]
then
    sudo pkill -f slave.py
    echo "murdered slave.py" >> /tmp/slave_killer.out
#    sudo rm /tmp/kill_slaves
fi
sleep 2
done