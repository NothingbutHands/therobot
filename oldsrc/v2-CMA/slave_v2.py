#!/shared/therobot/env-therobot/bin/python3
import json
import multiprocessing
import os
import random
import sys
import time
import traceback

import brain
import numpy as np
import zmq
from brain import env_params, num_worker_trial, data_master_ip, num_episodes, total_workers, lr, \
    learning_rate, compress_bars, es_stats_fp
from btgym import BTgymEnv
from comms import get_candidates_for_slave, save_fitness_to_disk, clear_finished_candidate, kill_on_port
from keras.engine.saving import load_model
from static_utils import min_sec, BTgymError, update_model_params, flatten_weights
from util import simple_log
from util.simple_log import log
from util.tail import tail

sigma_init = brain.sigma_init
rank = int(os.environ['SGE_TASK_ID'])-1
print('slave rank', rank)
cores = multiprocessing.cpu_count()
truncate_logfile = False
logfile_line_limit = 7000
env = None

# setup stdout write to file
if not os.path.exists('/shared/slave_logs'):
    try:
        os.mkdir('/shared/slave_logs')
    except FileExistsError:  # in case some errors are thrown by multiple slaves making dir at same time
        pass
sys.stdout = open('/shared/slave_logs/{}.log'.format(rank), 'a+')


def log(text, level='i'):
    """small wrapper for simple_log, logs to file prefixed with rank"""
    global log_i
    fp = '/shared/slave_logs/{}.log'.format(rank)
    simple_log.log(text, level, fp)

    if truncate_logfile:
        # truncate files
        lines = tail(fp, logfile_line_limit)

        if lines:
            with open(fp, 'w') as f:
                f.write('\n'.join(lines))


def recreate_env(params):
    global env
    log('Recreating env api port {} data port {} ...'.format(params['port'], params['data_port']))
    try:
        env.data_context.destroy()
    except:
        log('error when destroying data context', 'e')
        pass

    try:
        env.close()
        del env
    except:
        log('error when closing env', 'e')
        pass
    sys.stdout.flush()
    sys.stdout = open('/shared/slave_logs/{}.log'.format(rank), 'a+')

    try:
        kill_on_port(params['port'])
    except:
        pass

    try:
        kill_on_port(params['port']-1)
    except:
        pass

    try:
        kill_on_port(params['port']+1)
    except:
        pass

    try:
        kill_on_port(params['data_port'])
    except:
        pass

    try:
        kill_on_port(params['data_port']-1)
    except:
        pass

    try:
        kill_on_port(params['data_port']+1)
    except:
        pass

    time.sleep(2)
    try:
        log('Try to recreate ...', 'debug')
        sys.stdout.flush()
        env = BTgymEnv(**params)
        env.reset()
        sys.stdout = open('/shared/slave_logs/{}.log'.format(rank), 'a+')
        log('env successfully recreated!', 'ok')
    except Exception as e:
        log('Error when recreating env.{} {}\n{}'.format(
            type(e), e, traceback.format_exc()
        ), 'error')

    return env


def reconnect_to_dataserver(env):
    # # Release client-side, if any
    # if self.data_context:
    #     self.data_context.destroy()
    #     self.data_socket = None

    # Set up client channel:
    # self.data_context = zmq.Context()
    # self.data_socket = self.data_context.socket(zmq.REQ)
    # self.data_socket.setsockopt(zmq.RCVTIMEO, self.connect_timeout * 1000)
    # self.data_socket.setsockopt(zmq.SNDTIMEO, self.connect_timeout * 1000)
    # self.data_socket.connect(self.data_network_address)

    # TODO: maybe try sending a {'ctrl': '_stop'} message before recreating context?

    log('Destroying zmq context and nullifying data_socket ...', 'warn')
    env.data_context.destroy()
    env.data_socket = None

    tout = env_params['connect_timeout'] if 'connect_timeout' in env_params else 60
    log('Rebuilding zmq context ...', 'warn')
    env.data_context = zmq.Context()
    env.data_socket = env.data_context.socket(zmq.REQ)
    env.data_socket.setsockopt(zmq.RCVTIMEO, tout * 1000)
    env.data_socket.setsockopt(zmq.SNDTIMEO, tout * 1000)

    # Attempt to reconnect
    log('Attempting reconnect to master dataserver ...', 'warn')
    env.data_socket.connect(env.data_network_address)

    # Check connection:
    log('Pinging data_server at: {} ...'.format(env.data_network_address), 'debug')

    resp = env._comm_with_timeout(
        socket=env.data_socket,
        message={'ctrl': 'ping!'}
    )
    if resp['status'] in 'ok':
        log('Data_server seems ready with response: <{}>'.format(
            resp['message']
        ), 'warn')


def get_reward(model, _env, seed=-1, num_episode=5, std=None, intra_episode_updates=True, max_len=-1, mean_mode=False):
    log('Get reward ...', 'debug')
    timeout = 5

    def set_seed(seed):
        random.seed(seed)
        np.random.seed(seed)
        _env.seed(seed)
        _env._seed(seed)

    reward_list = []
    dct_compress_mode = False

    if seed != -1:
        set_seed(seed)

    for ep_idx, episode in enumerate(range(num_episode)):
        try:
            log('Begin episode {}'.format(ep_idx))
            try:
                obs = _env.reset()
            except ConnectionError:
                log('ConnectionError while taking reset', 'warn')
                # reconnect_to_dataserver(env)
                _env = recreate_env(env_params)
                obs = _env.reset()

            # if dct_compress_mode:
            #     obs = compress_input_dct(obs['raw'])

            total_reward = 0.0
            done = False  # updated by env.step()
            _x = 0
            while not done:
                _x += 1

                flat_obs = obs['raw'].flatten()
                if compress_bars:
                    # mean each x bars together before feeding to model
                    flat_obs = flat_obs.reshape(-1, compress_bars).mean(axis=1)

                flat_obs = flat_obs[np.newaxis, ...]
                action = model.predict(flat_obs, batch_size=1)[0]
                action = action.argmax(axis=0)  # index of largest value
                action = {'default_asset': action}

                # if _x % 5000 == 0:
                #     log(action, 'debug')

                try:
                    obs, reward, done, info = _env.step(action)
                except ConnectionError:
                    log('ConnectionError while taking step', 'err')
                    # reconnect_to_dataserver(env)
                    _env = recreate_env(env_params)

                    # resp = env._comm_with_timeout(
                    #     socket=env.data_socket,
                    #     message={'ctrl': 'ping!'}
                    # )
                    obs, reward, done, info = _env.step(action)

                total_reward += reward

            reward_list.append(total_reward)
            if std and intra_episode_updates:
                print('ep {} applying noise to model weights, std {} ...'.format(ep_idx, std))
                update_model_params(
                    model, flatten_weights(model), std=std,
                    lr=learning_rate, sigma_init=sigma_init,
                    total_workers=total_workers
                )

            log('Episode {} Complete'.format(ep_idx), 'ok')

        except BTgymError:
            log('Caught BTgymError during Episode {}, skipping episode ...'.format(ep_idx), 'error')
            # reconnect_to_dataserver(env)
            _env = recreate_env(env_params)

    return np.mean(reward_list), 0.0


def update_sigma():
    """read es stats and update sigma_init"""
    global sigma_init

    if os.path.exists(es_stats_fp+'_'):
        with open(es_stats_fp+'_') as f:
            stats = json.load(f)
            if stats:
                sigma_init = stats['sigma']
                log('sigma updated to {}'.format(sigma_init))


def slave():
    global  env

    update_sigma()

    log('loading model ...', 'debug')
    model = load_model(brain.keras_model_fp)
    log('model loaded! Param count: {}'.format(model.count_params() if model is not None else 0), 'ok')

    # model = init_keras_model()
    data_master_addr = 'tcp://'+data_master_ip+':'

    dataserver_index = int(rank/50)  # determine by rank which dataserver to connect to
    env_params.update(
        port=env_params['port']+rank,
        data_port=env_params['data_port']+dataserver_index,
        data_master=False,
        data_network_address=data_master_addr,
        task=env_params['task']+rank,
        verbose=0,
    )

    # if rank % cores == 0:
    #     env_params['data_master'] = True
    #     env_params['data_port'] = env_params['data_port'] + (rank/cores)
    # else:
    #     env_params['data_master'] = False

    log('Attempting to create env with api port {} data port {} addr {}'.format(
        env_params['port'], env_params['data_port'], data_master_addr
    ))
    try:
        env = BTgymEnv(**env_params)
    except:
        log('Exception when creating BTgymENV with following params\n{}'.format(json.dumps(
            env_params, indent=2, sort_keys=True
        )), level='error')

    # Creating btgym env seems to change where stdout is piped to
    sys.stdout = open('/shared/slave_logs/{}.log'.format(rank), 'a+')

    log('Begin slave loop')
    while 1:
        seeds_set, solutions = get_candidates_for_slave(rank, num_worker_trial)
        log("Get solutions and seeds_set", 'debug')

        results = []
        asdf = time.time()
        for idx, (solution, seed) in enumerate(zip(solutions, seeds_set)):  # len(solutions) = num_worker_trial
            start_time = time.time()
            log('Processing solution #{}/{}'.format(idx+1, len(solutions)))
            log('Solution taste {}'.format(solution))
            update_model_params(model, solution, None, lr=lr, total_workers=total_workers, sigma_init=sigma_init)
            # model.set_weights(unflatten_dense_weights(model, solution))
            fitness, timesteps = get_reward(
                model, env, seed, num_episode=num_episodes, std=None, intra_episode_updates=True
            )

            results.append([fitness, timesteps])

            delta = time.time()-start_time
            estimate = round((num_worker_trial-1-idx)*delta, 2)
            prediction = 1.20  # 1.20 = 120%
            estimate *= prediction
            mins = int(estimate / 60)
            secs = round(estimate - (mins * 60))
            if idx == 0:
                log('rank {} solution {}/{} fitness function time {}s estimated complation time {}m{}s'.format(
                    rank, idx+1, num_worker_trial, round(delta, 2), mins, secs
                ))

        asdf = time.time() - asdf
        log('Total time {1} trials, {2}'.format(rank, num_worker_trial, min_sec(asdf)))
        sys.stdout.flush()
        save_fitness_to_disk(rank, results)
        clear_finished_candidate(rank)
        log('Finished. Candidate solution cleared...', 'ok')


if __name__ == '__main__':
    try:
        slave()
    except Exception as e:
        log('ERROR {} {} {}'.format(
            type(e), e, traceback.format_exc()
        ), level='critical')
