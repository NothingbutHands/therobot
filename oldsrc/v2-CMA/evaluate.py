import time
from collections import namedtuple

import numpy as np
from btgym import BTgymEnv, BTgymRendering
from es import CMAES
from gym import spaces
from static_utils import simulate, load_model, set_model_params, build_model_dict
# logger
from train import Seeder

fhandle = None
log = None

rank = 0

# ES related
model = None
es = None
use_pretrained_model = True  # whether to load previously trained model or not
load_me = None  # model to load if above is True
renderer = BTgymRendering(
    render_modes=['episode', 'human']
)
renderer.initialize_pyplot()

# ES related params
num_episodes = 3
num_worker = 8  # number of processors
num_worker_trial = 1  # num of solutions for each worker to evaluate
population = num_worker * num_worker_trial  # num candidate solutions returned by es.ask()
num_params = -1
bars = 30  # 180 = 3 hours of 1m bars

PRECISION = 10 ** 7

# +3 because of additional elements added by encode_solution_packets()
SOLUTION_PACKET_SIZE = (3 + num_params) * num_worker_trial

RESULT_PACKET_SIZE = 4 * num_worker_trial

sigma_init = 0.1
sigma_decay = 0.9999

optimizer = 'cma'

datafile_path = 'data/DAT_ASCII_EURUSD_M1_2016.csv'

env_param_list = []
for i in range(2):
    is_master = (i == 0)
    fn = datafile_path
    # if is_master:
    #     filename = '../btgym/examples/data/DAT_ASCII_EURUSD_M1_2016.csv'
    # else:
    #     filename = None
    env_params = dict(filename=fn,
                      start_weekdays=[0, 1, 2, 3, 4, 5, 6],
                      episode_duration={'days': 7, 'hours': 00, 'minutes': 00},
                      time_gap={'days': 5, 'hours': 00, 'minutes': 00},
                      start_00=True,
                      start_cash=10000,
                      broker_commission=0.002,
                      fixed_stake=10,
                      drawdown_call=30,
                      render_state_as_image=True,
                      state_shape=dict(raw=spaces.Box(low=0, high=1, shape=(30, 4))),
                      port=5200 + i,
                      data_port=4800 + i,
                      data_master=is_master,
                      render_enabled=True,
                      iplot=False,
                      task=0.0,
                      verbose=0, )
    env_param_list.append(env_params)


def decode_result_packet(packet):
    r = packet.reshape(num_worker_trial, 4)
    workers = r[:, 0].tolist()
    jobs = r[:, 1].tolist()
    fits = r[:, 2].astype(np.float) / PRECISION
    fits = fits.tolist()
    times = r[:, 3].astype(np.float) / PRECISION
    times = times.tolist()
    result = []
    n = len(jobs)
    for i in range(n):
        result.append([workers[i], jobs[i], fits[i], times[i]])
    return result


# Make environment:
ModelConfig = namedtuple(
    'ModelConfig', ['env_name',
                    'input_size', 'output_size',
                    'layers', 'activation',
                    'noise_bias', 'output_noise']
)

model_config = dict(
    env_name='BTgymEnv',
    input_size=bars*4,  # 4 = o,h,l,c
    output_size=4,  # 4 = o,h,l,c
    time_factor=1,
    time_input=1,
    layers=[64, 64],
    activation='tanh',
    noise_bias=0.0,
    output_noise=[False, False, True],
)

root_dir = 'log/'
configname = 'BTgymEnv.{opt}.{ep}.{pop}.{l1}_{l2}'.format(
    opt=optimizer, ep=num_episodes, pop=population,
    l1=model_config['layers'][0], l2=model_config['layers'][1]
)
filebase = root_dir + configname
filename = filebase + '.pkl'
ploteval = filebase + 'reward_history.pkl'
plotreward = filebase + 'rewardhist.json'
filename_log = filebase + '.# log.json'
filename_hist = filebase + '.hist.json'
filename_best = filebase + '.best.json'

if use_pretrained_model:
    load_me = filename


def encode_result_packet(results):
    r = np.array(results)
    r[:, 2:4] *= PRECISION
    return r.flatten().astype(np.int32)


def encode_solution_packets(seeds, solutions):
    n = len(seeds)
    result = []
    for i in range(n):
        worker_num = int(i / num_worker_trial) + 1
        result.append([worker_num, i, seeds[i]])
        result.append(np.round(np.array(solutions[i]) * PRECISION, 0))
    result = np.concatenate(result).astype(np.int32)
    result = np.split(result, num_worker)
    return result


def decode_solution_packet(packet):
    packets = np.split(packet, num_worker_trial)
    result = []
    for p in packets:
        result.append([p[0], p[1], p[2], p[3:].astype(np.float) / PRECISION])
    return result


def evaluate_batch(optimizer):
    # duplicate model_params
    model_params_quantized = np.array(
        optimizer.current_param()
    ).round(4)

    solutions = []
    for i in range(optimizer.popsize):
        solutions.append(np.copy(model_params_quantized))

    seeds = np.arange(optimizer.popsize)

    packet_list = encode_solution_packets(seeds, solutions)
    fitness_list = []

    for i in range(1, 2):
        packet = packet_list[i - 1]
        solutions = decode_solution_packet(packet)
        results = []
        for idx, solution in enumerate(solutions):  # len(solutions) = num_worker_trial
            worker_id, jobidx, seed, weights = solution
            worker_id = int(worker_id)
            possible_error = "work_id = " + str(worker_id) + " rank = " + str(rank)
            # if worker_id != rank:
            # log.error(sp('error rank', rank, 'worker_id', worker_id))
            # assert worker_id == rank, possible_error
            # jobidx = int(jobidx)
            seed = int(seed)

            # fitness_func returns a mean of the given solution
            # used on num_episodes amount of simulations
            start_time = time.time()
            set_model_params(model, weights)
            r_list, t_list = simulate(model, render_mode=False)
            fitness = np.mean(r_list)
            timesteps = np.mean(t_list)
            # fitness, timesteps = fitness_func(weights, seed)
            # show_state(model.env.render('human'))
            # model.env.render('human')
            renderer.render(['episode', 'human'], model.env.engine)
            fitness_list.append(fitness)

            delta = time.time() - start_time
            estimate = round((num_worker_trial - 1 - idx) * delta, 2)
            mins = int(estimate / 60)
            secs = round(estimate - (mins * 60))
            print(
                'rank', rank, 'solution {}/{}'.format(idx + 1, num_worker_trial),
                'fitness_func time', '{}s'.format(round(delta, 2)),
                'estimated completion time {}m{}s'.format(mins, secs),
            )
            results.append([worker_id, jobidx, fitness, timesteps])

    return np.mean(fitness_list), model_params_quantized


def master():
    global datafile_path, use_pretrained_model, es, reward_evals, improvement_list, model, optimizer, load_me

    model = build_model_dict(**model_config)

    seeder = Seeder()

    print('creating master model env')
    model['env'] = BTgymEnv(**env_param_list[0])

    if not es:
        x0 = load_model(load_me) if use_model else None
        print('x0', x0)
        cma = CMAES(
            num_params,
            sigma_init=sigma_init,
            popsize=population,
            x0=x0
        )
        optimizer = 'cma'
        es = cma

    eval_log = []
    reward_evals = []

    t = 0
    while True:
        print('evaluating...')
        reward_eval, model_params_quantized = evaluate_batch(es)
        reward_evals.append(reward_eval)
        print('reward_eval', reward_eval)
        model_params_quantized = model_params_quantized.tolist()
        # improvement = reward_eval - best_reward_eval
        # improvement_list.append(improvement)
        # with open(reward_history_fp, 'wb') as f:
        #     pkl.dump([reward_evals, improvement_list], f)
        # print('reward_history_fp', reward_history_fp)
        # np.savetxt(reward_history_fp, )

        eval_log.append([t, reward_eval, model_params_quantized])

        # with open(filename_best, 'wt') as out:
        #     res = json.dump([best_model_params_eval, best_reward_eval], out, sort_keys=True, indent=0,
        #                     separators=(',', ': '))
        # print("improvement", t, improvement, "curr",
        #       reward_eval, "prev", prev_best_reward_eval,
        #       "best", best_reward_eval)
        # legend = ['reward_eval', 'improvement']
        # ticks = list(range(len(reward_evals) * 2))[::2]
        # data_lines = [reward_evals, improvement_list]
        # ylim = (np.min(data_lines), np.max(data_lines))
        # print('y', ylim)
        # plot_log(rank, 'evaluation', data_lines,
        #          legend=legend, ticks=ticks,
        #          xlabel='master timestep', ylabel='reward',
        #          ylim=ylim, figsize=figsize,
        #          filebase=name)


if __name__ == '__main__':
    try:
        master()
    except:
        print('CAUGHTSSS!!!')
