#!/bin/sh
# The #$ lines is called bowtie (a qsub thing)

#$ -cwd
#$ -N killall
#$ -pe mpi 40
#$ -j y
date
/shared/therobot/killslaves.sh
