from subprocess import check_output

lines = check_output('qstat -f', shell=True).decode('utf8').split('\n')
hosts = [line.split(' ')[0].split('@')[-1] for line in lines if line.startswith('all.q')]

# all the below does is fix the end of hosts string
# usually it's xxxxx.inter or xxxxx.intern but it should be xxxxx.internal
fixed_hosts = []
for host in hosts:
    fixed_hosts.append('.'.join(host.split('.')[:-1]+['internal']))

# then call qconf -Mq path/to/file
template = """
qname                 all.q
hostlist              @allhosts
seq_no                0
load_thresholds       np_load_avg=1.75
suspend_thresholds    NONE
nsuspend              1
suspend_interval      00:05:00
priority              0
min_cpu_interval      00:05:00
processors            UNDEFINED
qtype                 BATCH INTERACTIVE
ckpt_list             NONE
pe_list               make smp mpi
rerun                 FALSE
slots                 1,{}

tmpdir                /tmp
shell                 /bin/sh
prolog                NONE
epilog                NONE
shell_start_mode      posix_compliant
starter_method        NONE
suspend_method        NONE
resume_method         NONE
terminate_method      NONE
notify                00:00:60
owner_list            NONE
user_lists            NONE
xuser_lists           NONE
subordinate_list      NONE
complex_values        NONE
projects              NONE
xprojects             NONE
calendar              NONE
initial_state         default
s_rt                  INFINITY
h_rt                  INFINITY
s_cpu                 INFINITY
h_cpu                 INFINITY
s_fsize               INFINITY
h_fsize               INFINITY
s_data                INFINITY
h_data                INFINITY
s_stack               INFINITY
h_stack               INFINITY
s_core                INFINITY
h_core                INFINITY
s_rss                 INFINITY
h_rss                 INFINITY
s_vmem                INFINITY
h_vmem                INFINITY
"""

formatted_hosts = []
for host in fixed_hosts:
    h = '[{}=1]'.format(host.strip())
    formatted_hosts.append(h)
hosts_string = ','.join(formatted_hosts)

updated_template = template.format(hosts_string)
with open('new_template', 'w') as f:
    f.write(updated_template)

check_output('qconf -Mq new_template', shell=True).decode('urf8')
