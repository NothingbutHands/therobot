#!/bin/bash

sudo apt-get update
sudo apt-get install python3-dev gfortran python3-pip libopenblas-base python3-mpi4py python3-tk -y
sudo add-apt-repository ppa:jonathonf/python-3.6 -y
sudo apt-get update
sudo apt-get install python3.6 -y
pip3 install numpy pandas cma
sudo apt-get install openmpi-bin python3-mpi4py -y
pip3 install logbook psutil gym
pip3 install -e /shared/btgym/
echo "Completed reinstall"