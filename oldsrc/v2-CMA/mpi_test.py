#!/usr/bin/python3

from __future__ import division
from __future__ import print_function

import numpy as np
from mpi4py import MPI


def pprint(str="", end="\n", comm=MPI.COMM_WORLD):
    """Print for MPI parallel programs: Only rank 0 prints *str*."""
    if comm.rank == 0:
        print(str+end, end=' ') 

comm = MPI.COMM_WORLD

pprint("-"*78)
pprint(" Running on %d cores" % comm.size)
pprint("-"*78)

comm.Barrier()

# Prepare a vector of N=5 elements to be broadcasted...
N = 5
if comm.rank == 0:
    A = np.arange(N, dtype=np.float64)    # rank 0 has proper data
    with open('/home/ubuntu/master.txt', 'a') as f:
        f.write('\n'+str(comm.rank)+' alive '+str(comm.size))
    with open('/shared/therobot/masterlist.txt', 'a') as f:
        f.write('\n'+str(comm.rank)+' alive '+str(comm.size))
else:
    with open('/home/ubuntu/slave.txt', 'a') as f:
        f.write('\n'+str(comm.rank)+' alive '+str(comm.size))
    A = np.empty(N, dtype=np.float64)     # all other just an empty array

# Broadcast A from rank 0 to everybody
comm.Bcast( [A, MPI.DOUBLE] )

# Everybody should now have the same...
print("[%02d] %s" % (comm.rank, A))
