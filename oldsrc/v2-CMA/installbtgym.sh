#!/bin/sh
# The #$ lines is called bowtie (a qsub thing)

#$ -cwd
#$ -N install-btgym
#$ -pe mpi 40
#$ -j y
/shared/therobot/install-btgym.sh >> install-btgym.out
