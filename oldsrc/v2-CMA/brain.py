#!/usr/bin/python3
from __future__ import print_function

import sys
from collections import namedtuple

# Name of run (all file are prefixed with it in some way (or stored in folders matching name))
name = 'timeout'

csv_data_file = 'DAT_ASCII_EURUSD_M1_2013-15.csv'
datafile_path = 'data/'+csv_data_file

# I/O related
optimizer = 'cma'

root_dir = 'log/'
# name = 'BTgymEnv.{opt}.{ep}.{pop}.{l1}_{l2}'.format(
#     opt=optimizer, ep=num_episodes, pop=population,
#     l1=model_config['layers'][0], l2=model_config['layers'][1]
# )

model = None
es = None
use_pretrained_model = True  # whether to load previously trained model or not
keras_model_fp = None  # model to load if above is True. If None, will load current config model (see `filename` variable below)
use_restricted_gaussian_sampler = True  # for high-dimensionality models
distributed_data_server = True  # True: each slave has it's own dataserver False: rank 0 is dataserver
# TODO: try compression with compress_input_dct()
compress_bars = 3  # mean every x bars together before input, 0 = disabled

name = name.replace(' ', '_')
filebase = root_dir + name
model_fp = filebase + '.pkl'
reward_history_fp = filebase + 'reward_history.pkl'
history_fp = filebase + '.hist.hdf5'
model_summary_fp = filebase + '.txt'
cmaes_fp = '{}_ES.pkl'.format(filebase)
std_fp = '{}_std.pkl'.format(filebase)
es_params_fp = '{}_es_params.pkl'.format(filebase)
es_stats_fp = '{}_es_stats.json'.format(filebase)
x0_fp = 'outcmaes/{}_x0.pkl'.format(name)
es_solutions_fp = 'outcmaes/{}_solutions.pkl'.format(name)
master_state_fp = 'outcmaes/{}_random.state'.format(name)
es_fitness_fp = 'outcmaes/{}_fitness.pkl'.format(name)
es_callables_dir = 'outcmaes/{}_callables/'.format(name)
es_opts_fp = 'outcmaes/{}_opts.pkl'.format(name)

if use_pretrained_model:
    keras_model_fp = model_fp


import os
import random
import subprocess

import time

from keras.layers import GaussianNoise


def tensorflow_shutup():
    """
    Make Tensorflow less verbose
    """
    try:
        # noinspection PyPackageRequirements
        import os
        from tensorflow.compat.v1 import logging
        logging.set_verbosity(logging.ERROR)
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

        # Monkey patching deprecation utils to shut it up! Maybe good idea to disable this once after upgrade
        # noinspection PyUnusedLocal
        def deprecated(date, instructions, warn_once=True):
            def deprecated_wrapper(func):
                return func

            return deprecated_wrapper

        from tensorflow.python.util import deprecation
        deprecation.deprecated = deprecated

    except ImportError:
        pass


tensorflow_shutup()

from comms import save_candidate_solutions_to_disk, submit_batch_job_to_qsub, wait_for_completion, delete_job, \
    kill_slaves, read_results_from_disk, clear_folders

if os.path.exists(master_state_fp):
    try:
        state = eval(open(master_state_fp).read().replace('\n', ''))
        random.setstate(state)
    except SyntaxError:
        # empty file
        print('{} is empty'.format(master_state_fp))
        pass

try:
    import numpy as np
except ImportError:
    subprocess.check_call('pip3 install numpy --user', shell=True)

# try:
from btgym import BTgymEnv
from gym import spaces
# except ImportError:
#     # you really don't want this to be happening if you can at all help it
#     subprocess.check_call('cd /shared/btgym/; pip3 install -e .', shell=True)
from scipy.fftpack import dct

# Handy function:
from static_utils import build_model_dict

# Will need following to display rendered
# images inline of jupyter notebook:
# import IPython.display as Display

# comm = MPI.COMM_WORLD
# rank = comm.rank

# debug
last_heartbeat = time.time()
heartbeat_interval = 300

# ES related params
num_episodes = 7
data_master_ip = None
try:
    total_workers = int(sys.argv[1])
    total_nodes = int(sys.argv[2])
except ValueError:
    data_master_ip = sys.argv[1]
    total_workers = int(os.environ['SGE_TASK_LAST'])
except IndexError:
    print('missing total_workers...')
    # total_workers = 5
    total_workers = int(os.environ['SGE_TASK_LAST'])

print('total workers', total_workers)
num_worker_trial = 1  # num of solutions for each worker to evaluate (directly influences population size)

population = total_workers * num_worker_trial  # num candidate solutions returned by es.ask()
print('pop', population)

# num_episodes * num_worker_trail is the total number of episodes that will be processed in simulate() by each slave
# num_episodes * population is the iteration count of the reset, get_action, step loop
# a full iteration cycle is 1 iteration of master(). i.e. call to ask() and tell()
num_params = -1
bars = 180  # 180 = 3 hours of 1m bars

PRECISION = 10 ** 7

mu = 5.0  # for initial param creation. see master2()
sigma_init = 0.5  # std coefficient. higher means larger search, I think...
sigma_decay = 0.999
# sigma = 0.1    # noise standard deviation
alpha = 0.5  # learning rate
learning_rate = alpha
lr = learning_rate


# Make environment:
ModelConfig = namedtuple(
    'ModelConfig', ['env_name',
                    'input_size', 'output_size',
                    'layers', 'activation',
                    'noise_bias', 'output_noise']
)

model_config = dict(
    env_name='BTgymEnv',
    input_size=bars*4 if not compress_bars else int((bars*4)/compress_bars),  # 4 = o,h,l,c
    output_size=4,  # 4 = o,h,l,c
    time_factor=1,
    time_input=1,
    layers=[512, 512],
    activation='tanh',
    noise_bias=0.0,
    output_noise=[False, False, True],
)

env_params = dict(
    filename=datafile_path,
    start_weekdays=[0, 1, 2, 3, 4, 5, 6],
    episode_duration={'days': 21, 'hours': 0, 'minutes': 0},
    time_gap={'days': 8, 'hours': 0, 'minutes': 0},
    # start_00=True,
    start_cash=100000,
    broker_commission=0.002,
    fixed_stake=100,
    drawdown_call=30,
    render_state_as_image=False,
    state_shape=dict(raw=spaces.Box(low=0, high=1, shape=(bars, 4))),  # 180 = past 3 hours of 1m bars
    port=17000,
    data_port=18000,
    data_master=True,
    render_enabled=False,
    task=0.0,
    # connect_timeout=180,  # zmq timeout in seconds
    verbose=0,   # 2 = Debug, 1 = quieter debug 0 = Notice
)


def evaluate_batch(optimizer, job_id, seeds, _datafeed=None, timeout=None):
    clear_folders()
    # duplicate model_params
    model_params_quantized = np.array(
        optimizer.x0
    ).round(4)

    solutions = []
    for i in range(optimizer.popsize):
        solutions.append(np.copy(model_params_quantized))
    solutions = np.asarray(solutions)

    # seeds = np.arange(optimizer.popsize)
    save_candidate_solutions_to_disk(solutions, seeds, total_workers)
    job_id = submit_batch_job_to_qsub(total_workers)

    print('evaluation batch initiated...')
    wait_idx, job_id = wait_for_completion(total_workers, job_id)
    delete_job(job_id)
    kill_slaves(total_workers)

    rewards, timestepsd = read_results_from_disk(total_workers)

    return np.mean(rewards), model_params_quantized

# current_hostname = socket.gethostname()
# master_hostname = sys.argv[1]  # first additional arguement passed to script
# print('current hostname', current_hostname, 'master hostname', master_hostname)


class Seeder:
    def __init__(self):
        self.limit = np.int32(2**31-1)

    def next_seed(self):
        result = np.random.randint(self.limit)
        return result

    def next_batch(self, batch_size):
        result = np.random.randint(self.limit, size=batch_size).tolist()
        return result


def under_the_hood(env):
    """Shows environment internals."""
    for attr in ['dataset', 'strategy', 'engine', 'renderer', 'network_address']:
        print('\nEnv.{}: {}'.format(attr, getattr(env, attr)))

    for params_name, params_dict in env.params.items():
        print('\nParameters [{}]: '.format(params_name))
        for key, value in params_dict.items():
            print('{} : {}'.format(key, value))


def to_string(dictionary):
    """Convert dictionary to block of text."""
    text = ''
    for k, v in dictionary.items():
        if type(v) in [float]:
            v = '{:.4f}'.format(v)
        text += '{}: {}\n'.format(k, v)
    return text


def compress_2d(w, shape=None):
    s = w.shape
    if shape:
        s = shape
    c = dct(dct(w, axis=0, type=2, norm='ortho'), axis=1, type=2, norm='ortho')
    return c[0:s[0], 0:s[1]]


def decompress_2d(c, shape):
    c_out = np.zeros(shape)
    c_out[0:c.shape[0], 0:c.shape[1]] = c
    w = dct(dct(c_out.T, type=3, norm='ortho').T, type=3, norm='ortho')
    return w


def compress_1d(w, shape=None, axis=0):
    s = w.shape
    if shape:
        s = shape
    c = dct(w, axis=axis, type=2, norm='ortho')
    return c[0:s[0], 0:s[1]]


def decompress_1d(c, shape, axis=0):
    c_out = np.zeros(shape)
    c_out[0:c.shape[0], 0:c.shape[1]] = c
    w = dct(c_out, axis=axis, type=3, norm='ortho')
    return w


def compress_input_dct(obs):
    new_obs = np.zeros((8, 8))
    for idx in range(obs.shape[2]):
        new_obs = +compress_2d(obs[:, :, idx] / 255., shape=(8, 8))
    new_obs /= float(obs.shape[2])
    return new_obs.flatten()


renderer_params = dict(
    render_modes=['episode', 'human', 'internal', ],  # 'external'],
    render_state_as_image=True,
    render_ylabel='OHL_diff. / Internals',
    render_size_episode=(12, 8),
    render_size_human=(9, 4),
    render_size_state=(11, 3),
    render_dpi=75,
)

# renderer = BTgymRendering(**renderer_params)
# renderer.initialize_pyplot()
renderer = -1
max_len = -1  # max time steps (-1 means ignore)


def __model_call__(self, fitness_function, solutions, data=(), timeout=None):
    """evaluate a list/sequence of solution-"vectors", return a list
    of corresponding f-values.

    Raises `multiprocessing.TimeoutError` if `timeout` is given and
    exceeded.
    """
    warning_str = ("WARNING: `fitness_function` must be a function,"
                   " not an instancemethod, to work with"
                   " `multiprocessing`")
    if isinstance(fitness_function, type(self.__init__)):
        print(warning_str)
    jobs = [self.pool.apply_async(fitness_function, (x, d))
            for x, d in zip(solutions, data)]
    try:
        return [job.get(timeout) for job in jobs]
    except:
        print(warning_str)
        raise


def create_env(e_params):
    crap_out = 20
    for i in range(crap_out):
        try:
            btenv = BTgymEnv(**e_params)
            return btenv
        except ConnectionError:
            print('killing on ports', e_params['port'], e_params['data_port'])
            subprocess.call('kill $( lsof -i:{port} -t ) > /dev/null 2>&1'.format(port=e_params['port']), shell=True)
            subprocess.call('kill $( lsof -i:{port} -t ) > /dev/null 2>&1'.format(port=e_params['data_port']),
                            shell=True)
            continue
    else:
        raise ConnectionError('after 20 tries still could not create env')


def init_model(env_params, md_config, master=True):
    # global model, num_params, SOLUTION_PACKET_SIZE, RESULT_PACKET_SIZE, num_worker, num_worker_trial, rank

    print('Building model dictionary ...')
    model = build_model_dict(**md_config)
    print('Done.')
    if not master:  # master doesn't need btgym environment
        print('Creating env ...')
        model['env'] = create_env(env_params)
        print('Done.')
    return model


def _init_keras_model():
    start_time = time.time()
    print('Compiling Model ... ')
    model = Sequential()
    model.add(Dense(512, input_shape=(model_config['input_size'],)))
    # model.add(GaussianNoise(stddev=0.1))
    model.add(Activation('tanh'))
    # model.add(Dropout(0.3))
    model.add(Dense(512))
    model.add(GaussianNoise(stddev=0.1))
    model.add(Activation('tanh'))
    # model.add(Dropout(0.3))
    model.add(Dense(model_config['output_size']))  # final output layer
    model.add(Activation('tanh'))
    # nadam = Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004)
    # agrad = Adagrad(lr=0.01, epsilon=None, decay=0.0)
    # optim = RMSprop(lr=0.01, rho=0.9, epsilon=None, decay=0.0)
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print('Model compield in {0} seconds'.format(round(time.time() - start_time, 4)))

    with open(model_summary_fp, 'w') as f:
        lines = []
        model.summary(print_fn=lambda x: lines.append(x))
        f.write('\n'.join(lines))

    return model


from keras.layers import Dense, Input, Activation
from keras.models import Sequential, Model


def init_keras_model(pre_compile=False):
    # change activations based on environment
    state_in = Input(shape=[model_config['input_size']])
    x = Dense(256)(state_in)
    x = Dense(128, activation='tanh')(x)
    x = Dense(64, activation='tanh')(x)
    # x = GaussianNoise(0.2)(x)
    out = Dense(model_config['output_size'], activation='tanh')(x)

    model = Model(inputs=state_in, outputs=out)
    if pre_compile:
        start = time.time()
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        print('Model compiled in {}s'.format(round(time.time()-start, 1)))

    with open(model_summary_fp, 'w') as f:
        lines = []
        model.summary(print_fn=lambda x: lines.append(x))
        f.write('\n'.join(lines))

    return model