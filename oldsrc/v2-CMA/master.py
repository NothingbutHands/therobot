#!/shared/therobot/env-therobot/bin/python3
import os
import random
import time

import brain
import numpy as np
from brain import init_model, Seeder
from comms import save_candidate_solutions_to_disk, submit_batch_job_to_qsub, read_results_from_disk, clear_folders, \
    wait_for_completion, delete_job, kill_slaves, delete_kill_slave_indicator_file, \
    get_current_job_id
from es import CMAES
from static_utils import load_model, update_model_params, pickle_load, min_sec, pickle_dump, sp

csv_data_file = brain.csv_data_file
datafile_path = brain.datafile_path

model = brain.model
es = brain.es
use_pretrained_model = brain.use_pretrained_model  # whether to load previously trained model or not
load_me = brain.keras_model_fp  # model to load if above is True. If None, will load current config model (see `filename` variable)
use_restricted_gaussian_sampler = brain.use_restricted_gaussian_sampler
distributed_data_server = brain.distributed_data_server  # True: each slave has it's own dataserver False: rank 0 is dataserver

# debug
last_heartbeat = brain.last_heartbeat
heartbeat_interval = brain.heartbeat_interval

# ES related params
num_episodes = brain.num_episodes
total_workers = brain.total_workers
print('total workers', total_workers)
num_worker_trial = brain.num_worker_trial  # num of solutions for each worker to evaluate (directly influences population size)

# -1 because the master node (included in the count of comm.size) does not compute a set of solutions (num_worker_trial)
population = total_workers * num_worker_trial  # num candidate solutions returned by es.ask()
print('pop', population)

# num_episodes * num_worker_trail is the total number of episodes that will be processed in simulate() by each slave
# num_episodes * population is the iteration count of the reset, get_action, step loop
# a full iteration cycle is 1 iteration of master(). i.e. call to ask() and tell()
num_params = brain.num_params
bars = brain.bars  # 180 = 3 hours of 1m bars

PRECISION = brain.PRECISION

sigma_init = brain.sigma_init
sigma_decay = brain.sigma_decay
# sigma = 0.1    # noise standard deviation
# alpha = 0.001  # learning rate


# Make environment:
ModelConfig = brain.ModelConfig

model_config = brain.model_config

env_params = brain.env_params

# I/O related
optimizer = brain.optimizer
root_dir = brain.root_dir
configname = brain.name
filebase = root_dir + configname
filename = filebase + '.pkl'
plothist = filebase + 'reward_history.pkl'
filename_hist = filebase + '.hist.pkl'

if use_pretrained_model:
    load_me = filename


def evaluate_batch(optimizer, job_id, seeds, _datafeed=None, timeout=None):
    clear_folders()
    # duplicate model_params
    model_params_quantized = np.array(
        optimizer.current_param()
    ).round(4)

    solutions = []
    for i in range(optimizer.popsize):
        solutions.append(np.copy(model_params_quantized))
    solutions = np.asarray(solutions)

    # seeds = np.arange(optimizer.popsize)
    save_candidate_solutions_to_disk(solutions, seeds, total_workers)
    job_id = submit_batch_job_to_qsub(total_workers)

    # submit_batch_job_to_qsub(total_workers)  # +1 because last worker craps out on BTgymEnv creation...
    # wait_idx = wait_for_completion(total_workers, timeout)
    # while not wait_idx:  # took too long, resubmit
    #     delete_job(job_id)
    #     job_id = submit_batch_job_to_qsub(total_workers)
    #     wait_idx = wait_for_completion(total_workers, timeout)

    print('evaluation batch initiated...')
    wait_idx, job_id = wait_for_completion(total_workers, timeout)
    delete_job(job_id)
    # submit_kill_job_to_qsub(total_workers)
    kill_slaves(total_workers)

    reward_list, timestep_list = read_results_from_disk(total_workers)

    return np.mean(reward_list), model_params_quantized


def master_loop(
        model, t, history, reward_evals, improvement_list,
        best_model_params_eval, wait_avg, seeder, time_list,
        start_time, time_division_fact, timeout, i_died):
    best_reward_eval = 0

    # check if solutions are in progress or not
    in_progress = os.path.exists('solutions_in_progress')
    print('Candidate solutions currently {}in progress'.format(
        '' if in_progress else 'not '
    ))

    # save state
    with open('master.state', 'w') as f:
        f.write(str(random.getstate()))

    if not in_progress:  # don't clear folders if I died during solution processing
        clear_folders()
    t += 1

    asdf = time.time()

    # solutions = es.ask() * PRECISION
    # solutions = solutions.astype(np.int32, subok=True, copy=False)
    if not in_progress:
        solutions = es.ask()
        print(solutions.shape)
        # np.round(solutions, 0, solutions)

        asdf = time.time() - asdf
        print(solutions.dtype, 'got solutions', len(solutions), len(solutions[0]),
              'in {}'.format(min_sec(asdf)))

    # if antithetic:
    #     seeds = seeder.next_batch(int(es.popsize / 2))
    #     seeds = seeds + seeds
    # else:
    #     seeds = seeder.next_batch(es.popsize)

    seeds = seeder.next_batch(es.popsize)

    # save current model params to disk...
    pickle_dump(filename, np.array(es.current_param()).round(4).tolist())

    sent_time = time.time()
    if not in_progress:
        save_candidate_solutions_to_disk(solutions, seeds, total_workers)
        job_id = submit_batch_job_to_qsub(total_workers)  # +1 because last worker craps out on BTgymEnv creation...

        # indicate that candidate solutions are awaiting processing
        with open('solutions_in_progress', 'w') as f:
            f.write('gay')

    # if len(wait_avg) > 3:
    #     # if it takes 50% longer than usual, delete the job and resubmit
    #     timeout = np.mean(wait_avg) * 1.50

    if in_progress:
        # need to get current job_idx
        job_id = get_current_job_id()

    wait_idx, job_id = wait_for_completion(total_workers, job_id, timeout)
    delete_job(job_id)
    # submit_kill_job_to_qsub(total_workers)
    kill_slaves(total_workers)
    os.remove('solutions_in_progress')

    reward_list, timestep_list = read_results_from_disk(total_workers)

    avg_timestep = np.mean(timestep_list)
    asdf = time.time() - sent_time
    print(t, 'total time for all slaves', min_sec(asdf))

    # Multiply a value with a negative power by some factor of 10
    # to retain precision while removing rounding errors.
    # Ex.
    #   x = .00078000000000041
    #   int(x * 10.0**6) = 780 / 10.0**6 = .00078
    # print('rank', rank, 'calculating statistics...')
    p_fact = PRECISION * 1.0  # precision factor

    avg_reward = int(np.mean(reward_list) * p_fact) / p_fact  # average of rewards
    std_reward = int(np.std(reward_list) * p_fact) / p_fact  # standard deviation of rewards

    # log.trace('es.tell reward_list')
    # print('tell reward_list', reward_list.shape, reward_list)
    asdf = time.time()
    es.tell(reward_list)
    asdf = time.time() - asdf
    print('time to tell reward_list, {}m{}s'.format(int(asdf / 60), int(asdf % 60)))

    es_solution = es.result()
    model_params = es_solution[0]  # best historical solution
    reward = es_solution[1]  # best reward
    curr_reward = es_solution[2]  # best of the current batch

    model = update_model_params(model, np.array(model_params).round(4))

    r_max = int(np.max(reward_list) * p_fact) / p_fact
    r_min = int(np.min(reward_list) * p_fact) / p_fact

    curr_time = int(time.time()) - start_time
    time_list.append(round(curr_time / time_division_fact, 1))

    h = (
        t, curr_time, avg_reward, r_min, r_max,
        std_reward,
        int(es.rms_stdev() * 100000) / 100000.,  # rms = Root Mean Square
        avg_timestep
        # mean_time_step + 1., int(max_time_step) + 1, int(min_time_step) + 1
    )
    h_tags = (  # just for logging purposes
        'timestep', 'curr_time', 'avg_reward',
        'r_min', 'r_max', 'std_reward', 'rms_stdev', 'avg_timestep'
        # 'mean_time_step', 'max_time_step', 'min_time_step'
    )
    # print(h[2:7])

    if len(history) > 1:
        print('\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
              '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
              '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')

        print(sp('BTgymEnv h:\n', '\n'.join(
            ['{:15} {:12} {}{}'.format(  # array containing strings of format TAG VALUE +/-CHANGE
                tag, value, '' if value - last < 0 else '+', value - last
            ) for tag, value, last in zip(h_tags, h, history[-1])]
        )))

        print('\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
              '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
              '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')

    print('saving history...')
    history.append(h)
    reward_stats = [[x[idx] for x in history] for idx in range(2, 7)]
    figsize = (20, 10)

    # if plot_data:
    #     plot_log(
    #         rank, 'reward_history',
    #         reward_stats, [tag for tag in h_tags[2:7]],
    #         ticks=time_list, ylabel='reward', xlabel='time',
    #         ylim=(np.min(reward_stats[1]), np.max(reward_stats[2])),  # min(r_min), max(r_max)
    #         figsize=figsize, filebase=name,
    #     )

    np.savetxt(filebase + 'rewardstats.txt', np.array(reward_stats))
    pickle_dump(filename, np.array(es.current_param()).round(4).tolist())
    pickle_dump(filename_hist, history)

    print(t, best_reward_eval)
    if t == 1:
        best_reward_eval = avg_reward
    if t % 8 == 0:  # evalu ate on actual task at hand
        prev_best_reward_eval = best_reward_eval
        print('best_reward_eval', best_reward_eval)
        print('evaluating...')
        reward_eval, model_params_quantized = evaluate_batch(es, job_id, seeds, timeout=timeout)
        reward_evals.append(reward_eval)
        print('reward_eval', reward_eval)
        model_params_quantized = model_params_quantized.tolist()
        improvement = reward_eval - best_reward_eval
        improvement_list.append(improvement)

        pickle_dump(plothist, (reward_evals, improvement_list))

        # eval_log.append([t, reward_eval, model_params_quantized])

        # if len(eval_log) == 1 or reward_eval > best_reward_eval:
        if t == 2 or reward_eval > best_reward_eval:
            best_reward_eval = reward_eval
            best_model_params_eval = model_params_quantized
        else:
            print("reset to previous best params, where best_reward_eval =", best_reward_eval)
            es.set_mu(best_model_params_eval)

        print('\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
              '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
              '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')

        print("improvement", t, improvement, "curr",
              reward_eval, "prev", prev_best_reward_eval,
              "best", best_reward_eval)

        print('\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
              '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
              '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')

        # ticks = list(range(len(reward_evals)*2))[::2]
        data_lines = [reward_evals, improvement_list]
        # legend = ['reward_eval', 'improvement']
        # ylim = (np.min(data_lines), np.max(data_lines))
        # if plot_data:
        #     plot_log(rank, 'evaluation', data_lines,
        #              legend=legend,
        #              xlabel='master timestep', ylabel='reward',
        #              ylim=ylim, figsize=figsize,
        #              filebase=name, major_locator=50)
    return model, t, history, reward_evals, \
           improvement_list, best_model_params_eval, \
           wait_avg, seeder, time_list, \
           start_time, time_division_fact, timeout


def master():
    # global datafile_path, use_pretrained_model, es, reward_evals, improvement_list, \
    #     model, model_config, load_me, optimizer
    global model, es

    model = init_model(env_params, model_config)

    # heartbeat(rank)
    seeder = Seeder()

    if not es:
        if use_pretrained_model:
            print('loading model')
            x0 = load_model(load_me)
            print('model loaded', len(x0) if x0 is not None else 0)
            if x0 is not None:
                model = update_model_params(model, x0)

            cma = CMAES(
                model.param_count,
                sigma_init=sigma_init,
                popsize=population,
                x0=x0,
                use_rgs=use_restricted_gaussian_sampler,
            )
        else:
            print(model.param_count, sigma_init, population)
            cma = CMAES(
                model.param_count,
                sigma_init=sigma_init,
                popsize=population,
                use_rgs=use_restricted_gaussian_sampler,
            )
        optimizer = 'cma'
        es = cma

    time_division_fact = 1000  # make timestep values smaller for sake of plotting
    time_list = []  # for plotting
    history = []
    if use_pretrained_model:
        history = pickle_load(filename_hist, [])
        print('loaded history, len', len(history))
        # if len(history) > 1:
        #     time_list = [round(hist_line[1]/time_division_fact, 1) for hist_line in history[1]]

    # eval_log = []
    best_reward_eval = 0
    best_model_params_eval = None

    start_time = int(time.time())

    # heartbeat()

    step = len(history)  # keep track of total timestep regardless of master.py shutdowns
    reward_evals = []
    improvement_list = []
    if use_pretrained_model:
        try:
            loaded = pickle_load(plothist, None)
            if loaded is not None:
                reward_evals, improvement_list = loaded
                print('loaded reward_history', loaded)
            else:
                print('Nothing loaded from', plothist)
        except OSError:
            pass  # first time, ignore

    wait_avg = []  # for tracking the avereage step time
    timeout = 30  # high estimate, assume it'll get a better average before a timeout

    delete_kill_slave_indicator_file()

    i_died = False
    # did i die?!?!?
    if os.path.exists('you_died'):
        # yes
        i_died = True
        os.remove('you_died')
        print('I died.')

    print('starting master loop')

    print('Overall step', step)
    model, step, history, reward_evals, \
    improvement_list, best_model_params_eval, \
    wait_avg, seeder, time_list, \
    start_time, time_division_fact, timeout = master_loop(
        model, step, history, reward_evals,
        improvement_list, best_model_params_eval,
        wait_avg, seeder, time_list,
        start_time, time_division_fact, timeout, i_died
    )

    i_died = False


if __name__ == '__main__':
    # TODO: deal with master node running out of memory
    try:
        os.remove('evo.log')
    except FileNotFoundError:
        pass
    master()
