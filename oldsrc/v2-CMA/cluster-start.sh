#!/usr/bin/env bash

#export CC=/usr/bin/mpicc.openmpi
#export MPICC=/usr/bin/mpicc.openmpi
sudo apt-get install python3-dev gfortran python3-pip libopenblas-base -y
echo "python3-dev fortran pip3 and libopenblas installed" 
sudo apt-get install python3-mpi4py python3-tk -y
sudo apt-get install python3 python-dev python3-dev \
     build-essential libssl-dev libffi-dev \
     libxml2-dev libxslt1-dev zlib1g-dev \
     python-pip -y
#sudo add-apt-repository ppa:jonathonf/python-3.6 -y
#sudo apt-get update
#sudo apt-get install python3.6 -y
#pip3 install numpy pandas cma
#echo "mpi4py tk and numpy installed"
#cd /shared/btgym/
#pip3 install -e /shared/btgym/
#echo "btgym installed"
sudo apt-get install htop -y
#echo "backend : Agg" | sudo tee /usr/local/lib/python3.5/dist-packages/matplotlib/mpl-data/matplotlibrc
echo "backend : Agg" > /shared/therobot/env-therobot/lib/python3.5/dist-packages/matplotlib/mpl-data/matplotlibrc
echo "Done."
echo "post install complete" 


cp /shared/therobot/slave_killer.sh /tmp/
cp /shared/therobot/run_daemon.sh /tmp/
sudo chmod u+x /tmp/slave_killer.sh
sudo chmod u+x /tmp/run_daemon.sh
sudo cp /shared/therobot/slave_killer.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable slave_killer
sudo service slave_killer start

exit 0