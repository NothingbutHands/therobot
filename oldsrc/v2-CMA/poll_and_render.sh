#!/bin/bash

masterhost=$1
name=$2

sftp -i ~/legend.pem ubuntu@$masterhost:/shared/therobot/log/"$2".hist.hdf5 log/"$2".hist.hdf5
sftp -i ~/legend.pem ubuntu@$masterhost:/shared/therobot/log/"$2"reward_history.pkl log/"$2"reward_history.pkl
sftp -i ~/legend.pem ubuntu@$masterhost:/shared/therobot/log/"$2"rewardstats.txt log/"$2"rewardstats.txt

python plot_data.py $name
