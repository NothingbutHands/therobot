#!/bin/bash

pattern=$1
prefix=$2

cd log/
for f in ${pattern} ; do mv -- "$f" "${prefix}_$f" ; done