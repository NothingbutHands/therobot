#!/usr/bin/python3

import subprocess

# kill any processes on needed ports
port_start = 12000
data_port_start = 13000
fleetports = list(range(port_start, port_start+100+1)) + list(range(data_port_start, data_port_start+100+1))
cmds = ['kill $( lsof -i:{port} -t ) > /dev/null 2>&1'.format(port=p) for p in fleetports]
for cmd in cmds:
    subprocess.call(cmd, shell=True)

subprocess.call('sudo pkill -f "brain.py"', shell=True)
