from __future__ import print_function

import multiprocessing as mp
import time

import brain
import numpy as np
from gym import spaces
from keras.layers import Dense, Activation, Dropout
from keras.models import Sequential
from keras.optimizers import Adagrad

np.random.seed(0)


def worker_process(arg):
    get_reward_func, weights = arg
    return get_reward_func(weights)


class EvolutionStrategy(object):
    def __init__(self, weights, get_reward_func, population_size=50, sigma=0.1, learning_rate=0.03, decay=0.999,
                 num_threads=1):

        self.weights = weights
        self.get_reward = get_reward_func
        self.POPULATION_SIZE = population_size
        self.SIGMA = sigma
        self.learning_rate = learning_rate
        self.decay = decay
        self.num_threads = mp.cpu_count() if num_threads == -1 else num_threads

    def _get_weights_try(self, w, p):
        weights_try = []
        for index, i in enumerate(p):
            jittered = self.SIGMA * i
            weights_try.append(w[index] + jittered)
        return weights_try

    def get_weights(self):
        return self.weights

    def _get_population(self):
        population = []
        for i in range(self.POPULATION_SIZE):
            x = []
            for w in self.weights:
                x.append(np.random.randn(*w.shape))
            population.append(x)
        return population

    def _get_rewards(self, pool, population):
        if pool is not None:
            worker_args = ((self.get_reward, self._get_weights_try(self.weights, p)) for p in population)
            rewards = pool.map(worker_process, worker_args)

        else:
            rewards = []
            for p in population:
                weights_try = self._get_weights_try(self.weights, p)
                rewards.append(self.get_reward(weights_try))
        rewards = np.array(rewards)
        return rewards

    def _update_weights(self, rewards, population):
        std = rewards.std()
        if std == 0:
            return
        rewards = (rewards - rewards.mean()) / std
        for index, w in enumerate(self.weights):
            layer_population = np.array([p[index] for p in population])
            update_factor = self.learning_rate / (self.POPULATION_SIZE * self.SIGMA)
            self.weights[index] = w + update_factor * np.dot(layer_population.T, rewards).T
        self.learning_rate *= self.decay

    def run(self, iterations, print_step=10):
        pool = mp.Pool(self.num_threads) if self.num_threads > 1 else None
        for iteration in range(iterations):

            population = self._get_population()
            rewards = self._get_rewards(pool, population)

            self._update_weights(rewards, population)

            if (iteration + 1) % print_step == 0:
                print('iter %d. reward: %f' % (iteration + 1, self.get_reward(self.weights)))
        if pool is not None:
            pool.close()
            pool.join()


def init_keras_model():
    start_time = time.time()
    print('Compiling Model ... ')
    model = Sequential()
    model.add(Dense(300, input_shape=(model_config['input_size'],)))
    model.add(Activation('relu'))
    model.add(Dropout(0.4))
    model.add(Dense(150))
    model.add(Activation('relu'))
    model.add(Dropout(0.4))
    model.add(Dense(model_config['output_size']))  # final output layer
    model.add(Activation('softmax'))
    # nadam = Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004)
    agrad = Adagrad(lr=0.01, epsilon=None, decay=0.0)
    # rms = RMSprop()
    model.compile(loss='categorical_crossentropy', optimizer=agrad, metrics=['accuracy'])
    print('Model compield in {0} seconds'.format(round(time.time() - start_time, 4)))
    return model


###################################
# INTEGRATE FROM CURRENT CODEBASE #
###################################
bars = 240  # 180 = 3 hours of 1m bars

model_config = dict(
    env_name='BTgymEnv',
    input_size=bars*4,  # 4 = o,h,l,c
    output_size=4,  # 4 = o,h,l,c
    time_factor=1,
    time_input=1,
    layers=[512, 512],
    activation='tanh',
    noise_bias=0.0,
    output_noise=[False, False, True],
)

env_params = dict(
    filename='/home/dave/PycharmProjects/therobot/data/DAT_ASCII_EURUSD_M1_2017.csv',
    start_weekdays=[0, 1, 2, 3, 4, 5, 6],
    episode_duration={'days': 30, 'hours': 0, 'minutes': 0},
    time_gap={'days': 15, 'hours': 0, 'minutes': 0},
    # start_00=True,
    start_cash=100000,
    broker_commission=0.002,
    fixed_stake=100,
    drawdown_call=30,
    render_state_as_image=True,
    state_shape=dict(raw=spaces.Box(low=0, high=1, shape=(bars, 4))),  # 180 = past 3 hours of 1m bars
    port=17000,
    data_port=18000,
    data_master=True,
    render_enabled=False,
    task=0.0,
    verbose=0,
)

# I/O related
optimizer = brain.optimizer
root_dir = brain.root_dir
configname = brain.name
filebase = root_dir + configname
filename = filebase + '.pkl'
plothist = filebase + 'reward_history.pkl'
filename_hist = filebase + '.hist.pkl'


