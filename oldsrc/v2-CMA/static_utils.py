import pickle as pkl
import random
import sys
import time

import numpy as np
from IPython import display
# debug
from util.simple_log import log

last_heartbeat = time.time()
heartbeat_interval = 300
rank = None


def timer(func):
    def timeit(*args, **kwargs):
        start = time.time()
        out = func(*args, **kwargs)
        log('{} took {}s'.format(func.__name__, time.time()-start), level='debug')
        return out

    return timeit


def heartbeat(r=None):
    global last_heartbeat, heartbeat_interval, rank
    if r:
        rank = r
    ct = time.time()
    if ct-last_heartbeat > heartbeat_interval:
        last_heartbeat = ct


def pickle_load(filename, _ret=list()):
    # print('pickle loading from', filename)
    try:
        with open(filename, 'rb') as dumpfile:
            x0 = pkl.load(dumpfile)
    except FileNotFoundError:
        import os
        log('File not found {} from cwd {}'.format(filename, os.getcwd()), 'warn')
        return _ret

    return x0 if (x0 is not None) else _ret


def pickle_dump(fp, obj):
    with open(fp, 'wb') as dumpto:
        pkl.dump(obj, dumpto)


def min_sec(seconds):
    return '{}m{}s'.format(int(seconds/60), int(seconds%60))


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def relu(x):
    return np.maximum(x, 0)


def passthru(x):
    return x


# useful for discrete actions
def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)


def show_state(env, step=0, info="", mode='human'):
    import matplotlib.pyplot as plt
    plt.figure(3)
    plt.clf()
    plt.imshow(env.render(mode=mode))
    plt.title("%s | Step: %d %s" % ('Episode', step, info))
    plt.axis('off')

    display.clear_output(wait=True)
    display.display(plt.gcf())


def get_action(md, x, t=0, mean_mode=False):
    """
    x = observation_space
    t = episode index
    if mean_mode = True, ignore sampling.
    """
    if 'raw' in x:
        h = np.array(x['raw']).flatten()
    else:
        h = np.array(x).flatten()
    if md.time_input == 1:
        md.time_signal = float(t) / md.time_factor
        # add `time_signal` to end of flattened obs_space
        h = np.concatenate([h, [md.time_signal]])
    num_layers = len(md['weight'])
    for i in range(num_layers):
        w = md['weight'][i]
        b = md['bias'][i]
        h = np.matmul(h, w) + b
        if md.output_noise[i] and (not mean_mode):
            out_size = md['shapes'][i][1]
            out_std = md['bias_std'][i]
            output_noise = np.random.randn(out_size) * out_std
            h += output_noise
        h = md['activations'][i](h)

    # if md.sample_output:  # only set if softmax activation is used
    #     h = sample(h)

    action = h.argmax(axis=0)  # index of largest value
    action = {'default_asset': action}

    return action


def fitness_func(model, solution_weights, seed, num_episodes, max_len=-1, rank=0):
    # log.trace('executing fitness function')
    model = update_model_params(model, solution_weights)

    reward_list, t_list = simulate(
        model, train_mode=True,
        render_mode=False, num_episode=num_episodes,
        seed=seed, max_len=max_len, rank=rank
    )
    fitness = np.mean(reward_list)
    timesteps = np.mean(t_list)
    return fitness, timesteps


def load_model(fn):
    log('loading model from file {}'.format(fn))
    data = pickle_load(fn, None)
    if data is None:
        return None
    model_params = np.array(data)
    return model_params


def update_model_params(model, solution_weights, std=None, lr=0.01, sigma_init=0.1, total_workers=None):
    """Model needs to provide
    .layers
    layer.get_weights()
    """
    # adapted for keras model
    pointer = 0
    for i, l in enumerate(model.layers):  # skip input layer, it has no trainable weights
        if l.count_params() > 0:
            weights, bias = l.get_weights()

            s_w = np.product(weights.shape)  # flattened weights length
            s = s_w + bias.shape[0]
            chunk = np.array(solution_weights[pointer:pointer + s])
            new_weights = chunk[:s_w].reshape(weights.shape)

            if std:
                # TODO: if this works, attempt to apply to bias as well
                log('layer #{} computing noise total_workers {} lr {} sigma_init {} std {} ...'.format(
                    i, total_workers, lr, sigma_init, std
                ))
                sys.stdout.flush()
                # compute and apply noise using std compute from previous step
                noise = np.random.randn(weights.shape[0], weights.shape[1]) * sigma_init
                new_weights = new_weights + lr/(total_workers*sigma_init) * (noise * std)

            # set new weights and bias
            l.set_weights((new_weights, chunk[s_w:].reshape(bias.shape)))
            #
            # pointer += s
            # if model.output_noise[i]:  # apply noise to thise layer
            #     s = bias.shape
            #
            #     # create a noise matrix to multiply the base weights by
            #     # uses polynomial distribution
            #     N = np.random.randn(new_w.shape) * model.sigma
            #     B = np.random.randn(bias.shape[0]) * model.sigma_bias
            #
            #     new_w = new_w + N
            #     new_b = new_b + B
            #
            #     l.set_weights((new_w, new_b))
            #
            #     pointer += s
            #
            # if model.output_noise[i]:
            #     s = bias.shape[0]
            #     model.bias_log_std[i] = np.array(solution_weights[pointer:pointer + s])
            #     model.bias_std[i] = np.exp(
            #         model.sigma_factor * model.bias_log_std[i] + model.sigma_bias
            #     )
            #     # if model.render_mode:
            #     #     print("bias_std, layer", i, model.bias_std[i])
            #     pointer += s

    return model


def sp(*args):
    """Python 3 print args concat without printing"""
    return ' '.join([str(a) for a in args])


def get_random_model_params(param_count, stdev=0.1):
    return np.random.randn(param_count) * stdev


def build_model_dict(
        # env_config,
        seed=-1,
        env_name='BTgymEnv',
        input_size=120,
        output_size=4,
        layers=list([40, 40]),
        time_factor=1,
        time_input=1,
        activation='tanh',
        noise_bias=0.0,
        output_noise=list([False, False, True]),
        _sigma_factor=0.5,
):
    class AttrDict(dict):
        def __init__(self, *args, **kwargs):
            super(AttrDict, self).__init__(*args, **kwargs)
            self.__dict__ = self

    layer_1 = layers[0]
    layer_2 = layers[1]

    # log.trace('Building model...')
    md = AttrDict({
        'shapes': [
            # shapes[x][1] = bias
            (input_size + time_input, layer_1),
            (layer_1, layer_2),
            (layer_2, output_size)
        ],
        'weight': [],
        'bias': [],
        'bias_log_std': [],
        'bias_std': [],
        'output_noise': output_noise,
        'sigma_bias': noise_bias,
        'sigma_factor': _sigma_factor,
        'param_count': 0,
        'activations': [],
        'time_factor': time_factor if time_input == 1 else -1,
        'time_input': time_input,

        # env
        'env': None,
        'seed': seed,
        'get_action': get_action
    })

    if activation == 'relu':
        md.activations = [relu, relu, passthru]
    elif activation == 'sigmoid':
        md.activations = [np.tanh, np.tanh, sigmoid]
    elif activation == 'softmax':
        md.activations = [np.tanh, np.tanh, softmax]
        md.sample_output = True
    elif activation == 'passthru':
        md.activations = [np.tanh, np.tanh, passthru]
    else:
        md.activations = [np.tanh, np.tanh, np.tanh]

    # log.trace('Done. Model built.')

    if seed != -1:
        md.env.seed(seed)

    for idx, shape in enumerate(md.shapes):
        md.weight.append(np.zeros(shape=shape))
        md.bias.append(np.zeros(shape=shape[1]))
        md.param_count += (np.product(shape) + shape[1])
        if md.output_noise[idx]:
            md.param_count += shape[1]
        log_std = np.zeros(shape=shape[1])
        md.bias_log_std.append(log_std)
        out_std = np.exp(md.sigma_factor * log_std + md.sigma_bias)
        md.bias_std.append(out_std)

    model_info = 'Mdoel {}\nweight {}\n' \
                 'bias {}\nparams {}\noutput_noise {}\n' \
                 'bias_log_std {}\nbias_std {}'
    # log.trace(model_info.format(
    #     pformat(md, indent=2), md.weight, md.bias,
    #     md.param_count, md.output_noise, md.bias_log_std,
    #     md.bias_std,
    # ))

    md = update_model_params(md, get_random_model_params(md.param_count))
    return md


class BTgymError(Exception):
    pass


def _ensure(env, action, args=None):
    if not args:
        args = []

    if action.lower() == 'step':
        func = env.step
    elif action.lower() == 'reset':
        func = env.reset
    else:
        raise ValueError("Must supply either 'step' or 'reset' as action. Got {}".format(action))

    crap_out = 8
    for _ in range(crap_out):
        try:

            return func(*args)  # ex: env.reset(), env.step(action), ...
        except ConnectionError:
            log('{} {} Connection Error ({}/{})'.format(rank, func, _+1, crap_out), level='error')
    else:
        log('{} cannot connect to dataserver. obliterate env and try again ...', 'warn')
        raise BTgymError


def ensure_reset(env):
    return _ensure(env, 'reset')


def ensure_step(env, act):
    """catch timeouts from .step() and retry until successful"""
    return _ensure(env, 'step', (act,))


def simulate(model, train_mode=False, render_mode=False,
             num_episode=5, seed=-1, max_len=-1, mean_mode=False,
             rank=0,):
    reward_list = []
    t_list = []

    dct_compress_mode = False

    env = model['env']
    max_episode_length = 3000

    if seed != -1:
        random.seed(seed)
        np.random.seed(seed)
        env.seed(seed)
        env._seed(seed)

    # print('iterate episodes...')
    for ep_idx, episode in enumerate(range(num_episode)):
        obs = ensure_reset(env)
        # if dct_compress_mode:
        #     obs = compress_input_dct(obs['raw'])

        total_reward = 0.0
        done = False  # updated by env.step()
        t = 0
        # print('begin episode {} simulation...'.format(ep_idx))
        while not done:
            t += 1

            if mean_mode:
                action = model.get_action(model, obs, t=t, mean_mode=(not train_mode))
            else:
                action = model.get_action(model, obs, t=t, mean_mode=False)

            obs, reward, done, info = ensure_step(env, action)

            # if dct_compress_mode:
            #     obs = compress_input_dct(obs['raw'])

            total_reward += reward

        reward_list.append(total_reward)
        t_list.append(t)

    return reward_list, t_list


def flatten_dense_weights(keras_model):
    return np.concatenate([
        w.flatten() for i, layer in enumerate(keras_model.layers)
        for w in layer.get_weights()
        if layer.count_params() > 0
    ])


def flatten_weights(keras_model, as_list=False):
    if as_list:
        return np.concatenate([w.flatten() for w in keras_model.get_weights()]).tolist()
    else:
        return np.concatenate([w.flatten() for w in keras_model.get_weights()])


def unflatten_dense_weights(keras_model, numpyarray):
    A = numpyarray

    ret = []
    start = 0
    for l in keras_model.layers:
        if l.count_params() > 0:
            for w in l.get_weights():
                d1 = w.shape[0]
                d2 = 1
                if w.ndim > 1:
                    d2 = w.shape[1]

                end = start + (d1 * d2)
                layer = np.reshape(A[start:end], w.shape)
                ret.append(layer)
                start = end

    return ret


def unflatten_weights(keras_model, numpyarray):
    A = numpyarray

    shapes = []
    for w in keras_model.get_weights():
        s1 = w.shape[0]
        s2 = 1
        if len(w.shape) > 1:
            s2 = w.shape[1]

        shapes.append((s1, s2))

    required_length = sum(w[0]*w[1] for w in shapes)
    assert required_length == len(A)

    ret = []
    start = 0
    for w, norm in zip(keras_model.get_weights(), shapes):
        end = start + (norm[0] * norm[1])
        layer = np.reshape(A[start:end], w.shape)
        ret.append(layer)
        start = end

    return ret
