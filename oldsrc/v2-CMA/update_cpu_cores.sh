#!/bin/sh
# Ex. ./update_cpu_cores.sh lt-0841205c13be2ce21 2 1 cfncluster-mycluster-ComputeFleet-1WXY8SJJ080D7

templateid=$1
cores=$2
threads=$3
scalinggroupname=$4
version=$5

aws ec2 create-launch-template-version --launch-template-id $templateid --version-description CustomCpuConfig"$cores"-"$threads" --source-version 1 --launch-template-data "{\"CpuOptions\": {\"CoreCount\": $cores, \"ThreadsPerCore\": $threads}}"

sleep 2

aws ec2 modify-launch-template --launch-template-id $templateid --default-version $version

sleep 2

aws autoscaling update-auto-scaling-group --auto-scaling-group-name $scalinggroupname --launch-template LaunchTemplateId=$templateid,Version=$version

sleep 2
