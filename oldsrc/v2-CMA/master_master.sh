#!/usr/bin/env bash

total_slaves=$1
total_nodes=$2

rm you_died
rm solutions_in_progress
rm master_error
rm /shared/kill_slaves

while [[ 1 ]]
do 
master.py ${total_slaves} ${total_nodes}
echo "MASTER_MASTER.SH: master.py died ..."
echo "Sleeping for 10s ..."
pkill master.py
touch you_died
timeout 21s yes > /dev/null
done
