#!/usr/bin/env bash
# The #$ lines is called bowtie (a qsub thing)

#$ -cwd
#$ -N btevo
#$ -o btevo_output.log
#$ -e btevo_error.log
#$ -r y
date
slave.py $1