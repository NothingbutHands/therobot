import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np


def plot_log(r, title, data, legend, ticks=list(), ylabel='secs',
             xlabel=None, color='blue', ylim=(-1, 1), figsize=None,
             filebase='BTgymEnv', major_locator=-1):
    if figsize:
        plt.figure(figsize=figsize, dpi=80)

    colors = ['blue', 'red', 'green', 'orange', 'purple', 'yellow', 'black', 'brown']

    # if ticks:
    #     di = list(range(len(ticks)))
    if isinstance(data, np.ndarray):
        for i, tag in enumerate(legend):
            plt.plot(data[:, i], color=colors[i], label=legend[i])
    elif isinstance(data[0], list):
        for idx, d in enumerate(data):  # plot data
            if legend:
                plt.plot(d, color=colors[idx], label=legend[idx])
            else:
                plt.plot(d, color=colors[idx])
    else:
        plt.plot(data, color=colors[0], label=legend)

    plt.title(title)
    plt.ylabel(ylabel)
    plt.legend(legend, loc='upper left')

    if xlabel:
        plt.xlabel(xlabel)
    if major_locator != -1:  # x-axis increment amount
        plt.gca().xaxis.set_major_locator(MultipleLocator(major_locator))
    if ylim:
        if ylim[0] == ylim[1]:
            ylim = (ylim[0]-1, ylim[1]+1)
        elif ylim[0] > -1 and ylim[1] < 1:
            ylim = (-1, 1)
        plt.ylim(ylim)
    filepath = 'log/plots/{}--rank-{}_{}.png'.format(
        filebase, r, title
    )

    plt.gca().set_xticks(np.arange(0, len(data), 1), minor=True)
    plt.grid(b=True, which='major', axis='x', color='#5a8293', linestyle='-', linewidth=.35)
    plt.grid(b=True, which='minor', axis='x', color='#5a8293', linestyle='--', linewidth=.15)
    plt.grid(b=True, which='major', axis='y', color='#f76a67', linestyle='-', linewidth=.35)
    plt.grid(b=True, which='minor', axis='y', color='#f76a67', linestyle='--', linewidth=.15)

    print('saving plot', title, 'to', filepath)
    plt.savefig(
        filepath,
        bbox_inches='tight',
    )
    plt.close()
