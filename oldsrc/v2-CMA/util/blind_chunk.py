"""Iterate over any iterable (list, set, file, stream, strings, whatever),
of ANY size (including unknown size), by chunks of x elements"""
# credit to: https://stackoverflow.com/a/11842010
from itertools import chain, islice


def chunks(iterable, size, format=iter):
    it = iter(iterable)
    while True:
        yield format(chain((it.next(),), islice(it, size - 1)))
