#!/shared/therobot/env-therobot/bin/python3
import os
import sys

from pygtail import Pygtail

try:
    from util.simple_log import level_prefix, levels, log_level
except ImportError:
    from simple_log import level_prefix, levels, log_level

total_workers = int(sys.argv[1])
if len(sys.argv) > 2:
    log_level = sys.argv[2].lower()[0]


def main():
    log_dir = '/shared/slave_logs/'
    format_line = '[{:0'+str(len(str(total_workers))-1)+'d}] {}'
    while True:
        for i in range(total_workers):
            fp = os.path.join(log_dir, '{}.log'.format(i))
            if os.path.exists(fp):
                for line in Pygtail(fp):
                    level = 'd'
                    for _, annotation in level_prefix.items():
                        if annotation in line:
                            level = _

                    if level in levels[log_level]:
                        sys.stdout.write(format_line.format(i, line))


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
