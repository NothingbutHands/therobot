# Data cleaning
#
# Currently cleans...
#   Missing Data
#   Duplicate Data
#   Wrong Data
from datetime import datetime, timedelta

DATE    = 0
PERIOD  = 1
OPEN    = 2
HIGH    = 3
LOW     = 4
CLOSE   = 5
VOLUME  = 6


def parse_history_csv(path):
    """
    Reads .csv file and parses out each line into a list
    of lists in the following format:
    [ [ date, period, open, high, low, close, volume], [ ... ], ...]
    :param path: path to currency pair history .csv
    :return: list
    """
    with open(path, 'r') as history:
        complete_history = []
        for line in history:
            complete_history.append(line.split(','))
    return complete_history


def determine_timeframe(history):
    last_entry_valid = False
    last_entry = None
    for h in history[::-1]:
        if h and last_entry_valid:
            last_period = last_entry[PERIOD].split(':')
            previous_to_last_period = h[PERIOD].split(':')
            last = datetime(2000, 1, 1, last_period[0], last_period[1])
            ptl = datetime(2000, 1, 1, previous_to_last_period[0], previous_to_last_period[1])
            delta = last - ptl
            return delta
        if h:
            last_entry_valid = True
            last_entry = h
    raise TypeError('No timeframe could be determined. Bad input? Type: {} Input: {}'.format(
        type(history), history)
    )


def increment_day(previous_date):
    times = previous_date.split('.')
    dt = datetime(times[0], times[1], times[2])  # year, month, day
    dt += timedelta(days=1)
    return dt.strftime('%Y.%m.%d')  # 2001.09.11


def clean_missing(history):
    """
    Fixes missing data in history
    :param history: see `parse_history_csv()`
    :return:  list of lists
    """
    def apply_fix(index, position, fix):
        history[index][position] = fix
        return True

    timeframe = None  # set as timedelta by determine_timeframe()
    previous_entry = history[0]
    fix_made = False

    for idx, entry in enumerate(history[1:]):
        idx += 1
        if not entry[DATE]:  # fix date
            fix = previous_entry[DATE]
            if entry[PERIOD] == '00:00':  # need to increase day
                fix = increment_day(fix)
            fix_made = apply_fix(idx, DATE, fix)

        if not entry[PERIOD]:  # fix period
            period = previous_entry[PERIOD].split(':')  # HOUR:MINUTE
            hours_and_minutes = datetime(2000, 1, 1, period[0], period[1])
            if timeframe is None:
                timeframe = determine_timeframe(history)
            hours_and_minutes += timeframe
            fix = hours_and_minutes.strftime('%H:%M')  # ex. 00:30
            fix_made = apply_fix(idx, PERIOD, fix)

        if not entry[OPEN]:
            fix = previous_entry[CLOSE]
            fix_made = apply_fix(idx, OPEN, fix)

        if not entry[HIGH]:
            # TODO: Fix missing HIGH prices
            pass

        if not entry[LOW]:
            # TODO: Fix missing LOW prices
            pass

        if not entry[CLOSE]:
            try:
                next_entry = history[idx+1]
                fix = next_entry[OPEN]
                fix_made = apply_fix(idx, CLOSE, fix)
            except IndexError:
                # Last entry, nothing we can do to fix
                pass
    return history, fix_made
