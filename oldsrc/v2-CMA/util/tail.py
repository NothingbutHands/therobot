# original source courtesy of: https://gist.github.com/amitsaha/5990310
# modified by me
import os


def tail(fp, lines=1000, _buf_size=1024):
    buf_size = _buf_size
    fsize = os.stat(fp).st_size

    if fsize > buf_size:
        iter = 0
        with open(fp) as f:
            if buf_size > fsize:
                buf_size = fsize-1
            data = []
            while True:
                iter += 1
                try:
                    f.seek(fsize-buf_size*iter)
                    data.extend(f.readlines())
                except ValueError:
                    return [d.replace('\n', '') for d in data[-lines:]]
                if len(data) >= lines or f.tell() == 0:
                    return [d.replace('\n', '') for d in data[-lines:]]
    else:
        return []
