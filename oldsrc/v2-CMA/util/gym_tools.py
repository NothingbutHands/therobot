

def under_the_hood(env):
    """Shows environment internals."""
    for attr in ['dataset', 'strategy', 'engine',
                 'renderer', 'network_address']:
        print('\nEnv.{}: {}'.format(attr, getattr(env, attr)))

    for params_name, params_dict in env.params.items():
        print('\nParameters [{}]: '.format(params_name))
        for key, value in params_dict.items():
            print('{} : {}'.format(key,value))