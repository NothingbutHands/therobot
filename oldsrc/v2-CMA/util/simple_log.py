import inspect
from datetime import datetime

datetime_now = datetime.now  # faster to avoid dot operations
log_level = 'debug'.lower()[0]  # (in order of descending verbosity) critical, info, error, warning, debug


# related to log(), declared globally for performance reasons
level_prefix = dict(
    o='[OK]',
    i='[..]',
    d='[??]',
    w='[WN]',
    e='[ER]',
    c='[!!]'
)
levels = {  # below are the levels which WILL be printed based on log_level
    'd': ['w', 'c', 'e', 'd', 'i', 'o'],
    'w': ['w', 'c', 'e', 'o'],
    'e': ['w', 'c', 'e'],
    'i': ['c', 'i', 'o'],
    'c': ['c', 'e'],
}


def _code_location(frame=None):
    """pass in frame as an inspect.getframinfo(inspect.currentframe()) or currentframe().f_back.f_back..."""
    if not frame:
        frame = inspect.getframeinfo(inspect.currentframe().f_back.f_back)
        filename = frame.filename.split('/')[-1]  # don't need full path
        lineno = frame.lineno
        return '{0}@{1}:'.format(filename, lineno)
    else:
        filename = frame.filename.split('/')[-1]  # don't need full path
        lineno = frame.lineno
        return '{0}@{1}:'.format(filename, lineno)


def tail(f, lines=20):
    """Makes no assumptions about line length.
    Backs through the file one block at a time till it's found the right number of '\n' characters."""
    # lovely function created by: https://stackoverflow.com/a/136368
    total_lines_wanted = lines

    BLOCK_SIZE = 1024
    f.seek(0, 2)
    block_end_byte = f.tell()
    lines_to_go = total_lines_wanted
    block_number = -1
    blocks = [] # blocks of size BLOCK_SIZE, in reverse order starting
                # from the end of the file
    while lines_to_go > 0 and block_end_byte > 0:
        if (block_end_byte - BLOCK_SIZE > 0):
            # read the last block we haven't yet read
            f.seek(block_number*BLOCK_SIZE, 2)
            blocks.append(f.read(BLOCK_SIZE))
        else:
            # file too small, start from begining
            f.seek(0,0)
            # only read what was not read
            blocks.append(f.read(block_end_byte))
        lines_found = blocks[-1].count('\n')
        lines_to_go -= lines_found
        block_end_byte -= BLOCK_SIZE
        block_number -= 1
    all_read_text = ''.join(reversed(blocks))
    return all_read_text.splitlines()[-total_lines_wanted:]


def log(text, level='i', to_file=None, frame=None):
    """basic log function

    Levels:
        o = OK      = [OK]
        i = info     = [..]
        d = debug    = [??]
        w = warn     = [WN]
        e = error    = [ER]
        c = critical = [!!]

    specify to_file as path to file you want appended to instead
    """
    if len(level) > 1:
        level = level[0]

    if level in levels[log_level]:
        if not to_file:
            print('{} {} {} {}'.format(
                datetime_now().strftime('%d/%m/%Y %H:%M:%S'),
                _code_location(frame), level_prefix[level], text
            ))
        else:
            with open(to_file, 'a+') as f:  # a+ means the file will be created if it doesn't exist
                f.write('{} {} {} {}\n'.format(
                    datetime_now().strftime('%d/%m/%Y %H:%M:%S'),
                    _code_location(frame), level_prefix[level], text)
                )


if __name__ == '__main__':
    log('test')


