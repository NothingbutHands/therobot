import requests
from bs4 import BeautifulSoup


def get_site_content(url=None):
    if url is None:
        url = 'https://www.mataf.net/en/forex/tools/correlation'
    r = requests.get(url)
    if r.status_code == 200:
        print('Got OK for site {}'.format(url))
    return r.content


def get_tables_from_content(content):
    soup = BeautifulSoup(content, 'html.parser')
    tables = soup.find_all('table', {'class': 'table table-condensed'})
    if len(tables) == 0:
        print('no tables found with class: table table-condensed')
        return False
    return tables


def _build_correlation_tables(tables):
    def add_pairs(timeframe, pairs):
        for pair in pairs:
            correlation[timeframe][pair] = {}

    def add_correlation(this_pair, to_this_pair, in_this_timeframe, is_this_value):
        correlation[in_this_timeframe][this_pair][to_this_pair] = is_this_value

    timeframes = ['5 min', '1 hour', '4 hour', 'daily']
    correlation = {timeframe: {} for timeframe in timeframes}
    for timeframe, table in zip(timeframes, tables):  # tables is a list, so use timeframes list to keep ordering
        headers = [th.text for th in table.find_all('th', {'class': 'data_correl'})]  # ex. ['EURUSD', ... ]
        add_pairs(timeframe, headers)
        table_rows = table.find_all('tr', {'class': 'data_correl'})
        for row in table_rows:
            curr_pair = row.find_all('th')[0].text  # ex. 'USDJPY'
            cells = row.find_all('td', {'class': 'data_correl'})
            for cell in cells:
                pair = cell['data-inst2']  # ex. 'EURGBP'
                try:
                    correl = float(cell.text)  # ex. 31.9
                except ValueError:
                    print(str(cell.text))
                add_correlation(curr_pair, pair, timeframe, correl)

    return correlation


def get_correlation(url=None):
    return _build_correlation_tables(get_tables_from_content(get_site_content(url)))


if __name__ == "__main__":
    correlation = get_correlation()
    print(correlation)
