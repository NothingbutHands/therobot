# Functions related to the master and slave nodes communicating.
import json
import multiprocessing
import os
import subprocess
import sys
import time
from os.path import join, isfile, exists
from subprocess import call, check_output, CalledProcessError

import numpy as np
import tqdm as tqdm
from static_utils import pickle_dump, timer, pickle_load
from util.simple_log import log

CANDIDATE_BASE = '/shared/candidates/'
CANDIDATE_SUFFIX = '_candidate_solutions'
STD_SUFFIX = '_standard_deviations'

FITNESS_BASE = '/shared/fitness/'
FITNESS_SUFFIX = '_fitness'

debug = False
if debug:
    from slave import slave
    log('local debug enabled', 'debug')
    CANDIDATE_BASE = 'shared/candidates/'
    FITNESS_BASE = 'shared/fitness/'

dead_nodes = []


@timer
def save_candidate_solutions_to_disk(solutions, seeds, stds, total_workers, base_dir=CANDIDATE_BASE):
    if type(solutions) is list:
        solutions = np.array(solutions)

    worker_sets = np.split(solutions, total_workers)
    seed_sets = np.split(np.asarray(seeds), total_workers)
    if stds:
        std_sets = np.split(stds, total_workers)
    else:
        std_sets = [None] * len(seed_sets)

    for i, (soln_set, seed_set, std_set) in enumerate(zip(worker_sets, seed_sets, std_sets)):
        filename = '{}{}'.format(i, CANDIDATE_SUFFIX)
        pickle_dump(join(base_dir, filename), soln_set)
        # save seeds array
        seeds_filename = '{}{}.seeds'.format(i, CANDIDATE_SUFFIX)
        with open(join(base_dir, seeds_filename), 'w') as f:
            json.dump(seed_set.tolist(), f)

        if std_set:
            std_filename = '{}{}.stds'.format(i, STD_SUFFIX)
            with open(join(base_dir, std_filename), 'w') as f:
                json.dump(std_set.tolist(), f)
    return True


def fitness_file_count(folder=FITNESS_BASE):
    return len([f for f in os.listdir(folder) if isfile(join(folder, f))])


def slave_node_died(job_id):
    """Checks qstat to see if a node died and still has an active job assigned to it
    this function exists to compensate for SGE's inability to account for spot nodes. thanks AWS..."""
    output = check_output('qstat -f', shell=True).decode('utf-8')
    lines = output.split('\n')
    node_died = False
    for i, line in enumerate(lines):
        if 'lx-amd64      auo' in line or '-NA-' in line:
            try:
                if '{} 0.'.format(job_id) in lines[i+1]:  # this node died and still has a job assigned to it
                    # print('Found dead node with current job:', lines[i+1])
                    node_died = True
            except IndexError:  # in case the current line is the last in the list
                break
    return node_died


def resubmit_job(job_id, total_workers):
    delete_job(job_id=job_id)
    time.sleep(15)
    _job_id = submit_batch_job_to_qsub(total_workers)
    return _job_id


def wait_for_completion(total_workers, job_id, timeout=None):
    """Watches the /shared/fitness/ until all solutions have been computed"""
    idx = 0
    last_count = 0
    count = fitness_file_count()
    jid = job_id

    prog = tqdm.tqdm(total=total_workers, desc='Slave Simulation', unit='node')

    while count < total_workers:
        if count > last_count:
            prog.update(count-last_count)

        time.sleep(5)
        last_count = count
        count = fitness_file_count()
        idx += 1

        if slave_node_died(jid):
            log('Slave node(s) died. Resubmitting...', 'warn')
            jid = resubmit_job(jid, total_workers)

    if count > last_count:
        prog.update(count - last_count)

    if not idx:
        idx = 1

    return idx % 20, jid


@timer
def delete_job(job_id):
    command = 'qdel {} > /dev/null'.format(job_id)
    try:
        output = check_output(command, shell=True).decode('utf-8')
    except CalledProcessError:
        log('got CalledProcessError, when deleting job {}...'.format(job_id), 'warn')
        output = 'No queue available yet'

    return job_id


def wait_for_submit(job_id):
    command = 'qstat -f'

    output = check_output(command, shell=True).decode('utf-8')
    while 'PENDING JOBS' in output:
        time.sleep(2)
        output = check_output(command, shell=True).decode('utf-8')
    return output


@timer
def submit_batch_job_to_qsub(total_workers):
    if debug:
        log('local debug, starting slave process locally', 'debug')
        jobs = []
        for _ in range(total_workers):
            p = multiprocessing.Process(target=slave())
            jobs.append(p)
            p.start()
        return True

    # command = 'qsub -pe mpi {}  qslave.sh'.format(total_workers)
    master_hostname = subprocess.check_output(['hostname']).decode('utf-8').replace('\n', '').strip()
    command = 'qsub -t 1-{0} qslave.sh {1}'.format(total_workers, master_hostname)
    log('running command {}'.format(command), 'debug')
    sys.stdout.flush()
    try:
        output = check_output(command, shell=True).decode('utf-8')
    except CalledProcessError:
        log('got CalledProcessError, assuming there is no quere available yet...', 'warn')
        output = 'No queue available yet'

    if 'job-array' in output:
        job_id = output.split(' ')[2].split('.')[0]
    else:
        job_id = output.split(' ')[2]

    wait_for_submit(job_id)

    return job_id


def submit_kill_job_to_qsub(total_workers):
    """Does not wait for kill job to complete before returning"""
    command = 'qsub -pe mpi {0} qkill.sh'.format(total_workers)
    log('running command {}'.format(command), 'debug')
    try:
        output = check_output(command, shell=True).decode('utf-8')
    except CalledProcessError:
        log('got CalledProcessError, assuming there is no quere available yet...', 'warn')
        output = 'No queue available yet'

    job_id = output.split(' ')[2]

    return job_id


def delete_kill_slave_indicator_file():
    try:
        os.remove('/shared/kill_slaves')
    except FileNotFoundError:
        return


class NodeError(Exception):
    def __init__(self, error_lines):
        self.lines = error_lines


def qstat_f():
    output = check_output('qstat -f', shell=True)
    return output.decode('utf-8')


def active_queues():
    output = check_output('qstat -f | grep "lx-amd64"', shell=True).decode('utf-8')
    active = 0
    for line in output.split('\n'):
        if line:
            if 'lx-amd64      a' in line:
                if 'lx-amd64      auo' in line or '-NA-' in line:
                    continue
                active += 1
            else:
                active += 1

    return active


def kill_slaves(total_workers):
    """touches /shared/kill_slaves, waits, then deletes the file and returns"""
    # the slave_killer.sh systemd services running on the compute nodes wait for the
    # existence of the kill_slaves file. When it exists, they kill the local slave.py process
    check_output('touch /shared/kill_slaves', shell=True)
    with open('/shared/kill_slaves', 'w') as f:
        f.write('heehee')

    # wait until no queues are overloaded ("lx-amd64      a" appears for a node)
    # try:
    while active_queues() < total_workers:
        log("Queue overloaded. Waiting for cool down ...", 'warn')
        time.sleep(30)
    # except NodeError:
    #     print('Resubmitting slave jobs...')
    #     delete_job(job_id)
    #     submit_batch_job_to_qsub(total_workers)

    # while 'lx-amd64      a' in qstat_f():
    #     print("Queue overloaded. Waiting for cool down...")
    #     time.sleep(30)

    # fitness_files = file_count()
    # while fitness_files < total_workers:
    #     print('waiting for fitness count to be {}, currently {}'.format(total_workers, fitness_files))
    #     time.sleep(30)
    #     fitness_files = file_count()
    time.sleep(1)  # give them time to kill the slave.py processes

    delete_kill_slave_indicator_file()
    return


def parse_fitness_files(base_dir=FITNESS_BASE):
    fitness_files = {}
    for f in os.listdir(base_dir):
        if isfile(join(base_dir, f)):
            # print(f)
            if str(f).endswith(FITNESS_SUFFIX):
                num = int(str(f).split('_')[0])
                fitness_files[num] = join(base_dir, f)

    return fitness_files


def kill_last_job(job_id, last_job):
    command = 'qdel {}.{}'.format(job_id, last_job)
    call(command, shell=True)
    # command = 'qdel {}.{}'.format(job_id, last_job-1)
    # call(command, shell=True)


def delete_contents_of_folder(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            log("Failure during delete contents of folder {} {}".format(type(e), e), level='error')
            continue


def clear_folders():
    """empties the candidates and fitness folders"""
    delete_contents_of_folder(CANDIDATE_BASE)
    delete_contents_of_folder(FITNESS_BASE)


@timer
def read_results_from_disk(total_workers, base_dir=FITNESS_BASE, np_array=True):
    fits = []
    times = []
    fitness_files = parse_fitness_files()
    for n in range(total_workers):
        obj = pickle_load(fitness_files[n])

        if type(obj) is list and np_array:
            arr = np.asarray(obj, np.float)
            fits.extend(-1*arr[:, 0])  # flip rewards value
            times.extend(arr[:, 1])
        elif type(obj) is list and not np_array:
            for row in obj:
                fits.append(-1*row[0])
                times.append(row[1])
        else:
            fits.extend(-1*obj[:, 0])
            times.extend(obj[:, 1])

    # print('fits time', fits, times)
    if not np_array:
        return fits, times

    return np.asarray(fits), np.asarray(times)


@timer
def get_candidates_for_slave(rank, num_worker_trail, base_dir=CANDIDATE_BASE):
    filename = '{}{}'.format(rank, CANDIDATE_SUFFIX)
    seeds_filename = filename + '.seeds'
    std_filename = '{}{}.stds'.format(rank, STD_SUFFIX)
    i = 0
    while not exists(join(base_dir, filename)):
        if i % 60 == 0:  # 12 * 5 = 60, wait 5 minutes
            log('no candidate solution, waiting ...')
        time.sleep(5)
        i += 1

    # if exists(join(base_dir, filename)):  # if candidate solutions for given slave exists
    # print(join(base_dir, filename), 'exists!')
    solution_set = pickle_load(join(base_dir, filename))
    with open(join(base_dir, seeds_filename)) as f:
        seeds_set = json.load(f)
    # print('soln', solution_set)
    return seeds_set, solution_set


@timer
def save_fitness_to_disk(rank, fitness, base_dir=FITNESS_BASE):
    filename = '{}{}'.format(rank, FITNESS_SUFFIX)
    # print('saving {} to directory {}'.format(filename, join(base_dir, filename)))
    pickle_dump(join(base_dir, filename), fitness)
    return True


def clear_finished_candidate(rank, base_dir=CANDIDATE_BASE):
    filename = '{}{}'.format(rank, CANDIDATE_SUFFIX)
    return os.remove(join(base_dir, filename))


def get_current_job_id():
    command = 'qdel ' + ' '.join([str(x) for x in list(range(1, 500))])
    try:
        output = check_output(command, shell=True).decode('utf-8')
    except CalledProcessError:
        pass
    return 420

    output = qstat_f()
    start = output.find('0.55500')
    job_idx_str = output[start-5:start]
    job_idx = int(job_idx_str.strip())
    return job_idx


def kill_on_port(port_number):
    cmd = "kill $( lsof -i:{} -t ) > /dev/null 2>&1".format(port_number)
    os.system(cmd)
    log('attempted kill on port {}'.format(port_number), 'debug')


def clear_slave_ports(total_workers):
    cmd = 'qsub -t 1-{} kill_on_ports.sh'.format(total_workers)

    log('Running "{}" to clear slave ports'.format(cmd), 'debug')

    try:
        output = check_output(cmd, shell=True).decode('utf-8')
    except CalledProcessError:
        log('got CalledProcessError, assuming there is no quere available yet...', 'warn')
        output = 'No queue available yet'

    log('Result: {}'.format(output))
