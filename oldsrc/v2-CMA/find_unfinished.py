#!/shared/therobot/env-therobot/bin/python3
import os

for f in os.listdir('/shared/slave_logs/'):
    if f.endswith('.log'):
        with open(os.path.join('/shared/slave_logs/', f)) as x:
            if 'Finished. Candidate' not in x.read():
                print('{} not finish'.format(f))
