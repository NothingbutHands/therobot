import resource

import objgraph as objgraph


def dump_heap(h, i):
    """
    @param h: The heap (from hp = hpy(), h = hp.heap())
    @param i: Identifier str
    """
    pass

    print("Dumping stats at: {0}".format(i))
    print("Memory usage: {0} (MB)".format(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024))
    print("Most common types:")
    objgraph.show_most_common_types()

    print("heap is:")
    print("{0}".format(h))

    by_refs = h.byrcs
    print("by references: {0}".format(by_refs))

    print("More stats for top element..")
    print("By clodo (class or dict owner): {0}".format(by_refs[0].byclodo))
    print("By size: {0}".format(by_refs[0].bysize))
    print("By id: {0}".format(by_refs[0].byid))
