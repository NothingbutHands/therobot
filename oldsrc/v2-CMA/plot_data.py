# File used to plot data created by training model using brain.py
import sys

import numpy as np
from static_utils import pickle_load
from util.plotting import plot_log

root_dir = 'log/'
try:
    name = sys.argv[1]
except IndexError:
    # set manually
    name = 'the fix'

name = name.replace(' ', '_')
filebase = root_dir + name
filename = filebase + '.pkl'
ploteval = filebase + 'reward_history.pkl'
filename_hist = filebase + '.hist.hdf5'
figsize = (20, 10)


def plot_reward(data, rank=0, figsize=figsize):
    #print(data)
    reward_evals, improvement_list = data
    legend = ['reward_eval', 'improvement']
    data_lines = [reward_evals, improvement_list]
    ylim = (np.min(data_lines), np.max(data_lines))

    plot_log(rank, 'evaluation', data_lines,
             legend=legend,
             xlabel='master timestep', ylabel='reward',
             ylim=ylim, figsize=figsize,
             filebase=name)


def plot_history(history, rank=0, figsize=figsize):
    h_tags = (  # just for logging purposes
        'timestep', 'curr_time', 'avg_reward',
        'r_min', 'r_max', 'std_reward', 'rms_stdev', 'avg_timestep'
        # 'mean_time_step', 'max_time_step', 'min_time_step'
    )

    time_list = []
    if len(history) > 1:
        time_list = [round(hist_line[1]/1000, 1) for hist_line in history]

    h = np.asarray(history)
    # get only values from columns 2-7 for plotting
    try:
        reward_stats = h[:, 2:8]
    except IndexError:
        reward_stats = h[:, 2:7]
    # reward_stats = [[x[idx] for x in history] for idx in range(2, 7)]
    # time_stats = [x[7] for x in history]
    try:
        ylim = np.min(reward_stats[:, 1])-2, np.max(reward_stats[:, 2])+2
    except ValueError:
        ylim = (-1, 1)
    #print(reward_stats.shape)
    # print(len(reward_stats.tolist()[0]))
    plot_log(
        rank, 'reward_history',
        reward_stats, h_tags[2:7],
        ticks=time_list, ylabel='reward', xlabel='time',
        ylim=ylim,  # min(r_min), max(r_max)
        figsize=figsize, filebase=name,
    )


if __name__ == '__main__':
    print('history_fp', filename_hist, 'ploteval', ploteval)
    history = pickle_load(filename_hist)
    #print('history', history)
    print('history len', len(history))
    plot_history(history)

    loaded = pickle_load(ploteval)
    if loaded:
        plot_reward(loaded)
    else:
        print("nothing to load from", ploteval)

    print('finished plotting...')
