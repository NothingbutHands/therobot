#!/shared/therobot/env-therobot/bin/python3
import json
import multiprocessing
import os
import random
import re
import subprocess
import sys
import time
import traceback
from os.path import join

import brain
import cma
import gc
import h5py
import numpy as np
import psutil
from brain import use_pretrained_model, keras_model_fp, history_fp, reward_history_fp, env_params, total_workers, \
    population, \
    total_nodes, es_opts_fp, filebase, init_keras_model, cmaes_fp, name, std_fp, es_params_fp, es_stats_fp, x0_fp, \
    es_solutions_fp, master_state_fp, es_fitness_fp, learning_rate, es_callables_dir, mu
from btgym import BTgymEnv
from btgym.datafeed.derivative import BTgymDataset2
from cma import CMAEvolutionStrategy, restricted_gaussian_sampler
from comms import save_candidate_solutions_to_disk, submit_batch_job_to_qsub, wait_for_completion, delete_job, \
    kill_slaves, read_results_from_disk, clear_folders, get_current_job_id, delete_kill_slave_indicator_file, \
    clear_slave_ports
from keras.engine.saving import load_model
from static_utils import pickle_load, pickle_dump, sp, update_model_params, flatten_weights
from util.simple_log import log

# TODO: don't store history/other lists of historical data, appen to file
# TODO: slave: multiprocessing.Process to run 2 trials. will require 2 models and 2 envs
# TODO: see if it's possible to run multiple data_masters on master node so load can be split

master_ip = subprocess.check_output(['hostname', '-I']).decode('utf-8').replace('\n', '').strip()
master_envs = None
master_env = None
es = None
model = None
logger = None
sample_size = 0.0  # size in bytes of sample set (returned by es.ask())
sigma_init = brain.sigma_init
t = 0

history = []
time_list = []
start_time = time.time()
reward_evals = []
improvement_list = []
time_division_fact = 1000  # make timestep values smaller for sake of plotting


class Seeder:
    def __init__(self):
        self.limit = np.int32(2**31-1)

    def next_seed(self):
        result = np.random.randint(self.limit)
        return result

    def next_batch(self, batch_size):
        result = np.random.randint(self.limit, size=batch_size).tolist()
        return result


def evaluate_batch(optimizer, job_id, seeds, stds, _datafeed=None):
    clear_folders()
    # duplicate model_params
    model_params_quantized = np.array(
        optimizer.x0
    ).round(4)

    solutions = []
    for i in range(optimizer.popsize):
        solutions.append(np.copy(model_params_quantized))
    solutions = np.asarray(solutions)
    # TODO: don't compile slave models when evaluating (otherwise Noise is injected)
    # seeds = np.arange(optimizer.popsize)
    save_candidate_solutions_to_disk(solutions, seeds, stds, total_workers)
    job_id = submit_batch_job_to_qsub(total_workers)

    print('evaluation batch initiated...')
    wait_idx, job_id = wait_for_completion(total_workers, job_id)
    delete_job(job_id)
    kill_slaves(total_nodes)

    rewards, timesteps = read_results_from_disk(total_workers)
    rewards = -rewards

    return np.mean(rewards), model_params_quantized


def get_rewards(candidate_solutions, seeds, stds, in_progress=False):
    if not in_progress:
        save_candidate_solutions_to_disk(candidate_solutions, seeds, stds, total_workers)
        job_id = submit_batch_job_to_qsub(total_workers)  # +1 because last worker craps out on BTgymEnv creation...

        # indicate that candidate solutions are awaiting processing
        with open('solutions_in_progress', 'w') as f:
            f.write('gay')
    else:
        job_id = get_current_job_id()

    wait_idx, job_id = wait_for_completion(total_workers, job_id)
    delete_job(job_id)
    # submit_kill_job_to_qsub(total_workers)
    kill_slaves(total_nodes)
    try:
        os.remove('solutions_in_progress')
    except FileNotFoundError:
        pass

    rewards, timesteps = read_results_from_disk(total_workers)
    # return opposite rewards, converts from a minimizer to a maximizer
    # rewards = -rewards

    return rewards, timesteps, job_id


def get_rewards_parallel(candidate_solutions, seeds, logger):
    """for use with cma.fmin's parallel_objective param"""
    global t

    clear_folders()

    # store solutions since we can't access later when running .fmin
    # pickle_dump(es_solutions_fp, candidate_solutions)
    # print('Dumped full solutions to', es_solutions_fp)

    log("Begin step {} parallel solution evaluation ...".format(t))
    save_candidate_solutions_to_disk(candidate_solutions, seeds, None, total_workers)
    job_id = submit_batch_job_to_qsub(total_workers)  # +1 because last worker craps out on BTgymEnv creation...

    # indicate that candidate solutions are awaiting processing
    with open('solutions_in_progress', 'w') as f:
        f.write('gay')

    wait_idx, job_id = wait_for_completion(total_workers, job_id)
    delete_job(job_id)
    # submit_kill_job_to_qsub(total_workers)
    kill_slaves(total_nodes)
    try:
        os.remove('solutions_in_progress')
    except FileNotFoundError:
        pass

    rewards, _ = read_results_from_disk(total_workers, np_array=False)
    del _
    # return opposite rewards, converts from a minimizer to a maximizer
    # rewards = -rewards

    # get ready for next run
    reset_envs()

    log('tell rewards ...')
    log(rewards, 'debug')

    t += 1

    return rewards


def compute_weight_decay(weight_decay, model_param_list):
    model_param_grid = np.array(model_param_list)
    return -weight_decay * np.mean(model_param_grid * model_param_grid, axis=1)


def apply_weight_decay(weight_decay, candidate_solutions,  rewards):
    if weight_decay > 0:
        l2_decay = compute_weight_decay(weight_decay, candidate_solutions)
        log('reward_table.shape {} l2_decay.shape {}'.format(rewards.shape, l2_decay.shape), 'debug')
        rewards += l2_decay
    return rewards


def _free_memory():
    """returns dict of free memory values, used free total, etc
    parsed from `free -m`"""
    # call the `free` linux module
    # parse out it's output so we can calculate percentage of used RAM
    space_remover = re.compile(r'\s+')
    lines = subprocess.check_output(['free', '-m']).decode('utf-8').split('\n')
    keys = space_remover.sub(' ', lines[0]).strip().split(' ')
    values = space_remover.sub(' ', lines[1].lower().replace('mem:', '')).strip().split(' ')
    return {k: v for k, v in zip(keys, values)}


def free_memory():
    return dict(psutil.virtual_memory()._asdict())


def dump_es():
    """dumps CMAES instance to file"""
    global es
    from pickle import Pickler
    p = Pickler(open(cmaes_fp, "wb"), protocol=4)
    p.fast = True
    start = time.time()
    p.dump(es)  # d is your dictionary
    log('dump_es() in {}s'.format(round(time.time()-start, 1)), 'debug')


def load_es():
    """loads CMAES instance from file"""
    from pickle import load
    with open(cmaes_fp, 'rb') as f:
        return load(f)


def is_life_worth_living(array_size=0, threshold=.8, reset_solutions=False, quiet=True):
    """
    array_size: size of solution sample (what's returned from es.ask()) in bytes
    threshold: % of used memory which triggers a restart
    reset_solutions: if True, deletes current candidate solutions on disk upon next run
    """
    if not quiet:
        log('Should I die?')

    free_dict = free_memory()
    array_size = sample_size

    if not quiet:
        log('Current Free Mem: {} MB'.format(round(free_dict['free'] / (1024 * 1024), 2)), 'debug')
    projected_mem_req = (array_size * 2)
    if not quiet:
        log('projected Mem req: {} MB'.format(round(projected_mem_req / (1024 * 1024), 2)), 'debug')
    remaining_mem = free_dict['free'] - projected_mem_req
    perc_free = remaining_mem / free_dict['total']
    if not quiet:
        log('{}% would be free'.format(round(perc_free, 2) * 100), 'debug')
    if perc_free < .10:  # less then 10% free memory would remain
        log('There would be less than 10% remaining. I must die!')
        return True

    # calculate % used RAM
    percent_used = float(free_dict['used']) / float(free_dict['total'])
    if percent_used >= threshold:
        log('There is currently less than 10% remaining. I must die!')
        return True

    return False


def maybe_ill_kill_myself(array_size=0, threshold=.8, reset_solutions=False):
    """check if overall mem usage of system is above 80%
    if so, commit suicide

    array_size: size of solution sample (what's returned from es.ask()) in bytes
    threshold: % of used memory which triggers a restart
    reset_solutions: if True, deletes current candidate solutions on disk upon next run
    """
    def die():
        if reset_solutions:
            if os.path.exists('solutions_in_progress'):
                os.remove('solutions_in_progress')

        log('I guess I kill myself now', 'ok')
        log('goodbye, world.')
        log('*hugs magnet*')
        sys.stdout.flush()

        close_envs()
        time.sleep(1)
        quit()

    if not is_life_worth_living(array_size, threshold, reset_solutions, quiet=False):
        die()

    log('ney, I shant die!', 'o')


def master():
    global master_env, es, logger

    weight_decay = 0.01  # weight decay coefficient

    time_division_fact = 1000  # make timestep values smaller for sake of plotting
    start_time = time.time()
    reward_evals = []
    improvement_list = []

    model = init_keras_model()

    if use_pretrained_model:
        if os.path.exists(keras_model_fp):
            log('loading model ...', 'debug')
            model = load_model(keras_model_fp)
            log('model loaded! {}'.format(model.count_params() if model is not None else 0), 'ok')
        else:
            log("model not found {}".format(keras_model_fp), 'w')

    history = []
    stds = np.ones(population)
    if use_pretrained_model:
        # load plot history
        history = pickle_load(history_fp, [])
        log('loaded history, len {}'.format(len(history)), 'ok')

        # load previous reward and improvement history
        try:
            loaded = pickle_load(reward_history_fp, None)
            if loaded is not None:
                reward_evals, improvement_list = loaded
                log('loaded reward_history {}'.format(loaded), 'o')
            else:
                log('Nothing loaded from {}'.format(reward_history_fp), 'w')
        except OSError:
            pass  # first time, ignore

        # load previous std array
        x = pickle_load(std_fp, _ret=-1)
        if type(x) is not int:
            stds = x

    # heartbeat(rank)
    seeder = Seeder()

    env_params['port'] = 13370
    env_params['data_network_address'] = 'tcp://'+master_ip+':'
    log(env_params['data_network_address'], 'd')

    dataset_params = dict(
        filename=env_params['filename'],
        episode_duration=env_params['episode_duration'],
        time_gap=env_params['time_gap'],
        # start_00=env_params['start_00'],
        start_weekdays=env_params['start_weekdays'],
    )
    env_params['dataset'] = BTgymDataset2(**dataset_params)

    # create data master env for slaves to connect to
    master_env = BTgymEnv(**env_params)
    master_env.reset()

    # create EvolutionStrategy
    # es = EvolutionStrategy(model.get_weights(), )

    params = None
    if use_pretrained_model:
        if os.path.exists(keras_model_fp):  # ensure there's actually a pre-trained model
            # flatten previously trained model weights
            # so they can be used to init CMAES
            print('flattening weights')
            params = flatten_weights(model, as_list=True)

    if not params:  # check after instead of creating empty list by default, saves space
        log('creating own params')
        params = np.random.randn(model.count_params())

    log('params len {}'.format(len(params)), 'd')
    es = CMAEvolutionStrategy(
        params,  # x0
        sigma_init,  # sigma init
        # restricted gauss sampler to increase performance for high dimensionality
        restricted_gaussian_sampler.GaussVDSampler.extend_cma_options({
            'popsize': population
        }),
    )

    logger = cma.CMADataLogger(name_prefix=name, append=True).load()
    logger.register(es, append=True)

    if not all(np.equal(es.x0, params) == True):
        log('params and es.x0 are not equal\n{}\n{}'.format(es.x0, params), 'warn')
    else:
        log('params and es.x0 are equal', 'o')

    # es.logger.load(name)  # load any previous
    # es.logger.save(name)  # save any new
    # es.logger.save_to()

    delete_kill_slave_indicator_file()

    # check if solutions are in progress or not
    in_progress = os.path.exists('solutions_in_progress')
    log('{}Candidate solutions in progress'.format(
        'No ' if not in_progress else ''
    ))

    solution_array_size = 0
    best_reward_eval = 0
    best_model_params_eval = None
    t = len(history)
    time_list = []
    log("Begin master loop")
    while 1:
        # save state
        with open('master.state', 'w') as f:
            f.write(str(random.getstate()))

        maybe_ill_kill_myself(solution_array_size)

        if not in_progress:  # don't clear folders if I died during solution processing
            clear_folders()

        t += 1
        seeds = seeder.next_batch(population)

        log("Getting step {} candidate solutions ...".format(t))
        if not in_progress:
            start = time.time()
            candidate_solutions = np.array(es.ask())
            log('ask() in {}s'.format(round(time.time()-start, 2)), 'ok')
            solution_array_size = candidate_solutions.nbytes
            log(len(candidate_solutions), 'd')
            log('Size of candidate solutions {} MB'.format(round(candidate_solutions.nbytes/(1024*1024), 2)), 'd')

            maybe_ill_kill_myself(reset_solutions=True)

            with h5py.File('current_candidates.hdf5', "w") as f:
                f.create_dataset("solutions", data=candidate_solutions, chunks=True)

            log('Step:', t)
            # send off to slaves to compute rewards
            rewards, timesteps, job_id = get_rewards(candidate_solutions, seeds, stds)

            rewards = apply_weight_decay(weight_decay, candidate_solutions, rewards)

            log('tell rewards ...')
            start = time.time()
            es.tell(candidate_solutions, rewards.tolist())
            log('tell() in {}s'.format(round(time.time() - start, 2)), 'ok')

            del candidate_solutions
        else:  # master died during candidate solution processing
            # get rewards once current process is complete
            log('Step:', t)
            rewards, timesteps, job_id = get_rewards(None, None, None, in_progress)

            with h5py.File('current_candidates.hdf5', "r") as f:
                candidate_solutions = f['solutions']

            rewards = apply_weight_decay(weight_decay, candidate_solutions, rewards)

            log('tell rewards ...')
            start = time.time()
            es.tell(candidate_solutions, rewards.tolist())
            log('tell() in {}s'.format(round(time.time() - start, 2)), 'ok')

            del candidate_solutions

        # add to cma log
        logger.add(es)
        logger.save()

        es.disp_annotation()
        # es.disp()
        print(es.result_pretty())

        # logger.plot()
        # logger.s.figsave('log/plots/{}_es.png'.format(name))

        log('Computing standard deviation for all rewards ...', 'd')
        # note: rewards is a numpy array
        # polynomial distribution
        stds = ((rewards - np.mean(rewards)) / (np.std(rewards) + 1e-5))

        log('collect', gc.collect(), 'd')

        # p_fact = PRECISION * 1.0  # precision factor

        avg_timestep = np.mean(timesteps)
        avg_reward = np.mean(rewards)
        std_reward = np.std(rewards)
        # avg_reward = int(np.mean(rewards) * p_fact) / p_fact  # average of rewards
        # std_reward = int(np.std(rewards) * p_fact) / p_fact  # standard deviation of rewards

        r = es.result
        es_solution = r[0], -r[1], -r[1], r[6]  # best params so far, historically best reward, curr reward, sigma
        model_params = es.result[5]  # mean solutions
        best_solution = es_solution[0]  # best historical solution
        best_overall_reward = es_solution[1]  # best reward
        best_of_batch = es_solution[2]  # best of the current batch

        print('model params shape', model_params.shape)
        # reward = es_solution[1]  # best reward
        # curr_reward = es_solution[2]  # best of the current batch

        model = update_model_params(model, model_params)
        # model.set_weights(unflatten_dense_weights(model, model_params.round(4)))

        # r_max = int(np.max(rewards) * p_fact) / p_fact
        # r_min = int(np.min(rewards) * p_fact) / p_fact
        r_max = np.max(rewards)
        r_min = np.min(rewards)

        curr_time = int(time.time()) - start_time
        time_list.append(round(curr_time / time_division_fact, 1))

        rms_stdev = es.result[6]
        rms_stdev = np.mean(np.sqrt(rms_stdev * rms_stdev))
        h = (
            t, curr_time, avg_reward, r_min, r_max,
            std_reward,
            int(rms_stdev * 100000) / 100000.,  # rms = Root Mean Square
            avg_timestep, best_of_batch
            # mean_time_step + 1., int(max_time_step) + 1, int(min_time_step) + 1
        )
        h_tags = (  # just for logging purposes
            'timestep', 'curr_time', 'avg_reward',
            'r_min', 'r_max', 'std_reward', 'rms_stdev', 'avg_timestep', 'best_of_batch'
            # 'mean_time_step', 'max_time_step', 'min_time_step'
        )
        # print(h[2:7])

        if len(history) > 1:
            print('\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
                  '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
                  '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')

            print(sp('BTgymEnv h:\n', '\n'.join(
                ['{:15} {:12} {}{}'.format(  # array containing strings of format TAG VALUE +/-CHANGE
                    tag, round(value, 12-len(str(int(value)))), '' if value - last < 0 else '+', value - last
                ) for tag, value, last in zip(h_tags, h, history[-1])]
            )))

            print('\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
                  '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
                  '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')

        print('saving history...')
        history.append(h)
        reward_stats = [[x[idx] for x in history] for idx in range(2, 8)]
        # figsize = (20, 10)

        np.savetxt(filebase+'rewardstats.txt', np.array(reward_stats))
        model.save(keras_model_fp)
        # pickle_dump(es_params_fp, model_params)

        f = flatten_weights(model)
        if not all(np.equal(model_params, f) == True):
            print('Model Params and Flattens model weights are NOT EQUAL')
            print('model_params {} flattened_weights {}'.format(
                model_params, f
            ))
            pickle_dump('model_params', model_params)
            pickle_dump('flattened_weights', f)

        # pickle_dump(filename, np.array(es.x0).round(4).tolist())
        pickle_dump(history_fp, history)
        pickle_dump(std_fp, stds)

        print('collect', gc.collect())

        ##################
        #   Evaluation   #
        ##################

        if t == 1:
            best_reward_eval = avg_reward
        if t % 8 == 0:  # evaluate on actual task at hand
            prev_best_reward_eval = best_reward_eval
            print('best_reward_eval', best_reward_eval)
            print('evaluating...')
            reward_eval, model_params_quantized = evaluate_batch(es, job_id, seeds, stds=stds)
            reward_evals.append(reward_eval)
            print('reward_eval', reward_eval)
            model_params_quantized = model_params_quantized.tolist()
            improvement = reward_eval - best_reward_eval
            improvement_list.append(improvement)

            pickle_dump(reward_history_fp, (reward_evals, improvement_list))

            # eval_log.append([t, reward_eval, model_params_quantized])

            # if len(eval_log) == 1 or reward_eval > best_reward_eval:
            # if t == 2 or reward_eval > best_reward_eval:
            #     best_reward_eval = reward_eval
            #     best_model_params_eval = model_params_quantized
            # else:
            #     print("reset to previous best params, where best_reward_eval =", best_reward_eval)
            #     # es.set_mu(best_model_params_eval)
            #     if best_model_params_eval:
            #         es.x0 = best_model_params_eval

            print('\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
                  '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
                  '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')

            print("improvement", t, improvement, "curr",
                  reward_eval, "prev", prev_best_reward_eval,
                  "best", best_reward_eval)

            print('\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
                  '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
                  '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')


def reward_printout(h, h_tags):
    if len(history) > 1:
        print('\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
              '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
              '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')

        print(sp('BTgymEnv h:\n', '\n'.join(
            ['{:15} {:12} {}{}'.format(  # array containing strings of format TAG VALUE +/-CHANGE
                tag, round(value, 12 - len(str(int(value)))), '' if value - last < 0 else '+', value - last
            ) for tag, value, last in zip(h_tags, h, history[-1])]
        )))

        print('\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n'
              '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n'
              '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n')


def book_keeping(_es):
    global reward_evals, improvement_list, history

    rewards, times = read_results_from_disk(total_workers, np_array=False)

    r = _es.result
    es_solution = r[0], -r[1], -r[1], r[6]  # best params so far, historically best reward, curr reward, sigma
    # model_params = es.result[5]  # mean solutions
    # best_solution = es_solution[0]  # best historical solution
    # best_overall_reward = es_solution[1]  # best reward
    best_of_batch = es_solution[2]  # best of the current batch

    r_max = np.max(rewards)
    r_min = np.min(rewards)
    avg_reward = np.mean(rewards)
    std_reward = np.std(rewards)
    avg_timestep = np.mean(times)

    curr_time = int(time.time()) - start_time
    time_list.append(round(curr_time / time_division_fact, 1))

    rms_stdev = _es.result[6]
    rms_stdev = np.mean(np.sqrt(rms_stdev * rms_stdev))
    h = (
        t, curr_time, avg_reward, r_min, r_max,
        std_reward,
        int(rms_stdev * 100000) / 100000.,  # rms = Root Mean Square
        avg_timestep, best_of_batch
        # mean_time_step + 1., int(max_time_step) + 1, int(min_time_step) + 1
    )
    h_tags = (  # just for logging purposes
        'timestep', 'curr_time', 'avg_reward',
        'r_min', 'r_max', 'std_reward', 'rms_stdev', 'avg_timestep', 'best_of_batch'
        # 'mean_time_step', 'max_time_step', 'min_time_step'
    )

    reward_printout(h, h_tags)

    log('saving history ...', 'debug')
    history.append(h)
    reward_stats = [[x[idx] for x in history] for idx in range(2, 8)]

    np.savetxt(filebase + 'rewardstats.txt', np.array(reward_stats))
    pickle_dump(history_fp, history)
    log('history saved!')


def pickle_opts(_es):
    log('Performing Full pickle dump of es stats ...', 'debug')
    pickle_dump(es_opts_fp, _es.opts)
    log('Stats dumped to {}'.format(es_opts_fp), 'ok')


def unpickle_opts():
    log('Loading fully pickled opts from {}'.format(es_stats_fp))
    opts = pickle_load(es_opts_fp)
    return opts


def dump_additional_es_stats(_es):
    skip_keys = ['randn', 'is_feasible', 'BoundaryHandler']

    if not os.path.exists(es_callables_dir):
        os.mkdir(es_callables_dir)

    for key, value in _es.opts.items():
        if callable(value) or key == 'seed':
            fn = '{}.pkl'.format(join(es_callables_dir, key))
            log('dumping callable to {} ...'.format(fn))
            pickle_dump(fn, value)

    log('finished dumping callables', 'ok')

    data = dict(
        countiter=_es.countiter,
        countevals=_es.countevals,
        sigma=_es.sigma,  # current, potentially degraded sigma
        sigma0=_es.sigma0,  # initial sigma, sigma the es instance was created with
        seed=_es.opts['seed'],
        raw_opts={k: v
                  for k, v in _es.opts.items()
                  if not callable(v) or k not in skip_keys
                  and not any(bad in str(v) for bad in ['<function', '<class'])},
    )

    with open(es_stats_fp, 'w') as f:
        json.dump(data, f)

    with open(es_stats_fp+'_', 'w') as f:
        json.dump({'sigma': _es.sigma}, f)

    log('Dumped es stats to {}'.format(es_stats_fp), 'ok')


def load_additional_es_stats():
    try:
        with open(es_stats_fp) as f:
            stats = json.load(f)

        stats['callables'] = {}

        # try to unpickle callables
        for filename in os.listdir(es_callables_dir):
            fp = join(es_callables_dir, filename)
            if os.path.isfile(fp):
                if filename.endswith('.pkl'):
                    log('Attempting to unpickle callable {}'.format(fp))
                    obj = pickle_load(fp, None)
                    if obj:
                        stats['callables'][filename.replace(name+'_', '').replace('.pkl', '')] = obj
                    else:
                        log("Failed to load callable {}".format(fp), 'warn')

    except Exception as e:
        log("Could not load additional stats because {} {}".format(type(e), e), 'warn')
        return {}

    return stats


def checkpoint_es(cmaes_instance):
    """saves the relevant cmaes data
    this function is called after each iteration of cma.fmin
    note it's passed to the callback kwarg, that's what it's for"""
    global start_time, reward_evals, improvement_list, history, model

    # dump_heap(hp.heap(), 'checkpoint init')

    log('Checkpoint! {}'.format(t))

    with open(master_state_fp, 'w') as f:
        f.write(str(random.getstate()))

    log('state of random lib saved', 'ok')

    logger.add(cmaes_instance)
    logger.save()
    log('cma logger saved', 'ok')
    dump_additional_es_stats(cmaes_instance)
    pickle_opts(cmaes_instance)
    # save the mean of samples
    log('saving best solution weights to disk ...')
    pickle_dump(es_params_fp, cmaes_instance.result[0])
    log('cma params dumped to {}'.format(es_params_fp), 'ok')

    model = update_model_params(model, cmaes_instance.result[0])
    model.save(keras_model_fp)

    # print('[..] saving current candidate solutions ...')
    # pickle_dump(
    #     es_solutions_fp,
    #     [
    #         [x for x in tup]
    #         for tup in cmaes_instance.sent_solutions.keys()
    #     ]
    # )
    # print('[OK] cma solutions dumped to', es_solutions_fp)

    # print('[..] saving current fitness results ...')
    # rewards, times = read_results_from_disk(total_workers, np_array=False)
    # pickle_dump(es_fitness_fp, rewards)
    # print('[OK] dumped fitness results to ', es_fitness_fp)

    log("Performing book keeping ...")
    book_keeping(cmaes_instance)
    log('finished book keeping', 'ok')

    start_time = time.time()
    # TODO: do es.logger plotting

    log('Checkpoint {} complete!'.format(t), 'ok')

    log('Clearing slave ports for next run...', 'debug')
    clear_slave_ports(total_workers)

    # dump_heap(hp.heap(), 'checkpoint complete')


def load_master_state():
    if os.path.exists(master_state_fp):
        try:
            state = eval(open(master_state_fp).read().replace('\n', ''))
            random.setstate(state)
        except SyntaxError:
            # empty file
            log('empty master.state', 'warn')
            pass
    else:
        log('{} not found'.format(master_state_fp), 'warn')


def create_env(params):
    env = BTgymEnv(**params)
    env.reset()
    log('Env created. api port {} data port {}'.format(params['port'], params['data_port']), 'ok')

    return env


def create_btgym_envs(tworkers):
    """creates btgym data master envs on incremental ports
    will create:
        1 server  if   0 < tworkers < 50
        2 servers if  50 < tworkers < 100
        3 servers if 100 < tworkers < 150
        ...
    """
    servers_required = int(tworkers/50)
    servers_required += 1 if int(tworkers/50) != tworkers/50 else 0  # ie. if there's a remainder, add 1
    jobs = []

    log('{} master data servers are required'.format(servers_required), 'debug')
    for i in range(servers_required):
        port = env_params['port'] + i
        data_port = env_params['data_port'] + i
        task = env_params['task'] + i

        log('Attempting to create Env. api port {} data port {}'.format(port, data_port))

        env_params.update(
            port=port,
            data_port=data_port,
            task=task,
        )
        p = multiprocessing.Process(target=create_env, args=(env_params,))
        p.daemon = False
        jobs.append(p)
        p.start()

    return jobs


def master2():
    global master_envs, history, t, reward_evals, improvement_list, logger, sample_size, sigma_init, model

    # dump_heap(hp.heap(), 'init')

    history = []
    reward_evals = []
    improvement_list = []
    if use_pretrained_model:
        # load plot history
        history = pickle_load(history_fp, [])
        log('loaded history, len {}'.format(len(history)), 'ok')

        # load previous reward and improvement history
        try:
            loaded = pickle_load(reward_history_fp, None)
            if loaded is not None:
                reward_evals, improvement_list = loaded
                log('loaded reward_history {}'.format(loaded), 'ok')
            else:
                log('Nothing loaded from {}'.format(reward_history_fp), 'warn')
        except OSError:
            pass  # first time, ignore

    t = len(history)

    # init master BTGym Env
    env_params['port'] = 13370
    env_params['data_network_address'] = 'tcp://' + master_ip + ':'
    log(env_params['data_network_address'], 'debug')

    dataset_params = dict(
        filename=env_params['filename'],
        episode_duration=env_params['episode_duration'],
        time_gap=env_params['time_gap'],
        # start_00=env_params['start_00'],
        start_weekdays=env_params['start_weekdays'],
    )
    env_params['dataset'] = BTgymDataset2(**dataset_params)

    # create data master env for slaves to connect to
    master_envs = create_btgym_envs(total_workers)

    seeder = Seeder()

    model = init_keras_model()

    first_run = (not use_pretrained_model)  # first_run is inverse of use_pretrained_model setting
    if use_pretrained_model:
        if os.path.exists(keras_model_fp):
            log('loading model ...', 'debug')
            model = load_model(keras_model_fp)
            log('model loaded! Param count {}'.format(model.count_params() if model is not None else 0), 'ok')
        else:
            log("model not found {}".format(keras_model_fp), 'w')
            first_run = True

    if first_run:
        log('saving keras model ...', 'debug')
        model.save(keras_model_fp)
        log('keras model saved to {}'.format(keras_model_fp), 'ok')

    params = 0
    first_run = True
    if use_pretrained_model:
        print('flattening weights')
        params = flatten_weights(model)
        # params = pickle_load(es_params_fp, 0)
        first_run = False

    if type(params) is int:  # check after instead of creating empty list by default, saves space
        log('Creating own params ...')
        params = sigma_init * np.random.randn(model.count_params()) + mu
        pickle_dump(es_params_fp, params)
        log('Params created. x0 dumped to {}'.format(x0_fp), 'ok')

        log('Dump random state ...')
        with open(master_state_fp, 'w') as _f:
            _f.write(str(random.getstate()))
        log('random state dumped to {}'.format(master_state_fp), 'ok')

    options = dict(
        randn=np.random.randn,
        popsize=population,
        termination_callback=is_life_worth_living,
        CMA_active=True,  # negative update, conducted after the original update -- set to False by GaussVDSampler
        CMA_cmean=learning_rate,  # learning rate for the mean value
        CMA_diagonal=True,  # number of iterations with diagonal covariance matrix, True for always
        CMA_dampsvec_fac=0.5,  # tentative and subject to changes, 0.5 would be a "default" damping for sigma vec update
        CMA_dampsvec_fade=0.1,  # tentative fading out parameter for sigma vector update
        mean_shift_line_samples=False,  # versatile: sample two new solutions co-linear to previous mean shift
        minstd=.01,  # versatile: minimal std (scalar or vec) in any coordinate direction, cave interference with tol*
        # maxstd=?,  # versatile: maximal std in any coordinate direction
    )
    options.update(**restricted_gaussian_sampler.GaussVDSampler.extend_cma_options({
        'popsize': population
    }))

    if use_pretrained_model:
        # attempt to load and set stats from end of previous run
        stats = load_additional_es_stats()
        if stats:
            # options.update(seed=stats['seed'])
            # set sigma_init of current es to final sigma of previous run
            sigma_init = stats['sigma']

            for option, setting in stats['callables'].items():
                log('Setting {} to value {}'.format(option, setting), 'debug')
                options[option] = setting

    # options_plus_defaults = cma.CMAOptions(options).complement()

    # skip_keys = ['randn', 'is_feasible', 'BoundaryHandler', 'termination_callback', 'CMA_eigenmethod']
    # default_options = {k: v for k, v in cma.CMAOptions().items() if k not in skip_keys}
    # options_plus_defaults = default_options
    # options_plus_defaults.update(**options)
    # options_plus_defaults = cma.CMAOptions({k: v for k, v in options.items() if k not in skip_keys}).complement()

    # x0 = np.random.randn(dimensionality)
    log('setting esobj with params of type {} {}'.format(type(params), str(params)[:15]), 'debug')
    esobj = CMAEvolutionStrategy(
        params,  # x0
        sigma_init,  # sigma init
        # restricted gauss sampler to increase performance for high dimensionality
        inopts=options,
    )

    if not use_pretrained_model or first_run:
        esobj.opts['randn'] = np.random.randn

    if use_pretrained_model and stats:
        # let's try this...
        opts = unpickle_opts()
        esobj.opts = opts


    dump_additional_es_stats(esobj)
    pickle_opts(esobj)

    sample_size = esobj.result[5].nbytes*population

    if 4 > 20:
        print('Feeding for resume ...')
        # feed for resume
        # feed_for_resume is unfeasible. Save everything as before and try to resume manually
        # it seemed to work before, pre-keras implementation. It should work
        # Instead:
        #   - Save and load master random state (maybe fuck with incremental seeds? set a seed by
        #   number and reset seed every step with incremented number?)
        #   - Save and load CMAES params (es.result[5]). set as x0 when creating instance on restart

        X = pickle_load(es_solutions_fp)
        fitness, times = pickle_load(es_fitness_fp)
        esobj.feed_for_resume(X, fitness)

    logger = cma.CMADataLogger(name_prefix='outcmaes/'+name, append=True).load()
    logger.register(esobj, append=True)
    esobj.logger = logger

    seeds = seeder.next_batch(population)

    esobj.disp_annotation()
    esobj.disp()

    # dump_heap(hp.heap(), 'before fmin()')

    log('Running fmin with \n{}'.format('\n'.join('{}: {}'.format(k, v) for k, v in esobj.opts.items())), 'debug')

    args = (seeds, logger)  # additional positional arguments passed to get_rewards_parallel
    result = cma.fmin(
        None,  # objective_function is None because we're using parallel_objective
        x0=esobj, sigma0=sigma_init,
        parallel_objective=get_rewards_parallel,
        args=args, callback=checkpoint_es,
        options={},  # avoids userwarnings when fmin attempts to set esobj's options with default CMAOptions dict
    )

    log('Result')
    for x in result:
        print(x)

    esobj.result_pretty()

    return


def close_envs():
    global master_envs

    for i, env in enumerate(master_envs):
        try:
            env.terminate()
            log('Env #{} successfully closed!'.format(i), 'ok')
        except Exception as e:
            log('Error when closing env #{}. {} {}\n{}'.format(
                i, type(e), e, traceback.format_exc()
            ), 'error')


def reset_envs():
    global master_envs

    for i, env in enumerate(master_envs):
        try:
            env.reset()
            log('Env #{} reset'.format(i))
        except Exception as e:
            log('Error when resetting env #{}. {} {}\n{}'.format(
                i, type(e), e, traceback.format_exc()
            ), 'error')


if __name__ == '__main__':
    try:
        os.remove('evo.log')
    except FileNotFoundError:
        pass

    clear_slave_ports(total_workers)

    try:
        if use_pretrained_model:
            if not os.path.exists(es_params_fp):
                log('First run, {} not found'.format(keras_model_fp))
                use_pretrained_model = False
            else:
                load_master_state()

        master2()
        close_envs()
        time.sleep(1)
        quit()
    except Exception as e:
        close_envs()
        with open('master_error', 'w') as f:
            err_message = 'ERROR {} {} {}'.format(
                type(e), e, traceback.format_exc()
            )
            f.write(err_message)
            log(err_message, 'critical')

        time.sleep(1)
        quit()
