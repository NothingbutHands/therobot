#!/shared/therobot/env-therobot/bin/python3
import os
import subprocess
import sys
import time
import traceback

import brain
from brain import init_model, create_env
from comms import get_candidates_for_slave, save_fitness_to_disk, clear_finished_candidate
from static_utils import load_model, update_model_params, fitness_func, min_sec

# from mpi4py import MPI

# comm = MPI.COMM_WORLD
# rank = comm.rank
rank = int(os.environ['SGE_TASK_ID'])-1

# setup stdout write to file
if not os.path.exists('/shared/slave_logs'):
    try:
        os.mkdir('/shared/slave_logs')
    except:  # in case some errors are thrown by multiple slaves making dir at same time
        pass
sys.stdout = open('/shared/slave_logs/{}.log'.format(rank), 'a+')

# print('getting rank...')
print('got rank', rank)


# comm.close()
# # check for other running process
# from subprocess import check_output
# try:
#     output = check_output('ps aux | grep slave.py', shell=True).decode('utf-8')
#     if '/usr/bin/python3 ./slave.py' in output:
#         print('one already running')
#         check_output('echo "one already running" >> /shared/therobot/selfkill', shell=True)
#         sys.exit(0)
# except:
#     pass

def log(*s):
    print('{0:03d}: {1}'.format(rank, ' '.join([str(x) for x in s])))


csv_data_file = brain.csv_data_file
datafile_path = brain.datafile_path
from shutil import copyfile
copyfile(datafile_path, '/tmp/{}'.format(csv_data_file))
datafile_path = '/tmp/{}'.format(csv_data_file)

model = brain.model
es = brain.es
use_pretrained_model = brain.use_pretrained_model  # whether to load previously trained model or not
load_me = brain.keras_model_fp  # model to load if above is True. If None, will load current config model (see `filename` variable)
use_restricted_gaussian_sampler = brain.use_restricted_gaussian_sampler
distributed_data_server = brain.distributed_data_server  # True: each slave has it's own dataserver False: rank 0 is dataserver

# debug
last_heartbeat = brain.last_heartbeat
heartbeat_interval = brain.heartbeat_interval

# ES related params
num_episodes = brain.num_episodes
# total_workers = comm.size
log('getting total workers...')
# total_workers = comm.size
total_workers = int(os.environ['SGE_TASK_LAST'])
# try:
#     total_workers = int(os.environ['SGE_TASK_LAST'])
# except Exception as e:
#     log('exception hit when getting total_workers', type(e), e)

# if total_workers == rank:  # last guy
#     # kill yourself because BTgymEnv will not create properly
#     log('killing myself...')
#     sys.exit(0)

log('total workers', total_workers)
num_worker_trial = brain.num_worker_trial  # num of solutions for each worker to evaluate (directly influences population size)

# -1 because the master node (included in the count of comm.size) does not compute a set of solutions (num_worker_trial)
population = total_workers * num_worker_trial  # num candidate solutions returned by es.ask()
log('pop', population)

# num_episodes * num_worker_trail is the total number of episodes that will be processed in simulate() by each slave
# num_episodes * population is the iteration count of the reset, get_action, step loop
# a full iteration cycle is 1 iteration of master(). i.e. call to ask() and tell()
num_params = brain.num_params
bars = brain.bars  # 180 = 3 hours of 1m bars

PRECISION = brain.PRECISION

sigma_init = brain.sigma_init
sigma_decay = brain.sigma_decay
# sigma = 0.1    # noise standard deviation
# alpha = 0.001  # learning rate


# Make environment:
ModelConfig = brain.ModelConfig

model_config = brain.model_config

env_params = brain.env_params


# I/O related
optimizer = brain.optimizer
root_dir = brain.root_dir
configname = brain.name
filebase = root_dir + configname
filename = filebase + '.pkl'
plothist = filebase + 'reward_history.pkl'
filename_hist = filebase + '.hist.pkl'

if use_pretrained_model:
    load_me = filename


def slave():
    global model, env_params
    log("Begin slave...")

    env_params.update(**model_config)
    env_params['filename'] = datafile_path
    env_params['port'] += rank
    if distributed_data_server:
        env_params['data_port'] += rank
    env_params['task'] += rank
    env_params['render_enabled'] = False
    env_params['data_master'] = distributed_data_server

    log('init_keras_model')
    model = init_model(env_params, model_config, master=False)
    log('finished model init')

    log('Binding model to ports', env_params['port'], ':', env_params['data_port'])
    if not model['env']:
        model['env'] = create_env(env_params)

        if use_pretrained_model:
            x0 = load_model(load_me)
            if x0 is not None:
                model = update_model_params(model, x0)
                log('Model loaded and params set...')
    log('Finished creating model...')

    while 1:
        seeds_set, solutions = get_candidates_for_slave(rank, num_worker_trial)
        log('seeds', seeds_set)
        log(solutions.shape)

        results = []
        fitness_list = []
        asdf = time.time()
        for idx, (solution, seed) in enumerate(zip(solutions, seeds_set)):  # len(solutions) = num_worker_trial
            start_time = time.time()
            log('Processing solution #{}/{}'.format(idx+1, len(solutions)))
            fitness, timesteps = fitness_func(
                model, solution_weights=solution, seed=seed,
                num_episodes=num_episodes, rank=rank
            )

            fitness_list.append(fitness)
            results.append([fitness, timesteps])

            delta = time.time()-start_time
            estimate = round((num_worker_trial-1-idx)*delta, 2)
            prediction = 1.20  # 1.20 = 120%
            estimate *= prediction
            mins = int(estimate / 60)
            secs = round(estimate - (mins * 60))
            if idx == 0:
                log(
                    'rank', rank, 'solution {}/{}'.format(idx+1, num_worker_trial),
                    'fitness_func time', '{}s'.format(round(delta, 2)),
                    'estimated completion time {}m{}s'.format(mins, secs),
                )

        asdf = time.time() - asdf
        print('Total time {1} trials, {2}'.format(rank, num_worker_trial, min_sec(asdf)))
        save_fitness_to_disk(rank, results)
        clear_finished_candidate(rank)
        log('Finished. Candidate solution cleared...')


if __name__ == '__main__':
    try:
        slave()
    except Exception as e:
        print('Hit an error. Killing self')
        print(traceback.format_exc())
        pass
    if model['env']:
        model['env'].close()
    # kill process on port
    print('killing on ports (myself?)', env_params['port'], env_params['data_port'])
    subprocess.call('kill $( lsof -i:{port} -t ) > /dev/null 2>&1'.format(port=env_params['port']), shell=True)
    subprocess.call('kill $( lsof -i:{port} -t ) > /dev/null 2>&1'.format(port=env_params['data_port']),
                    shell=True)
    sys.exit(0)
