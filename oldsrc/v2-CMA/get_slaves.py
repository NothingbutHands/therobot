import boto3

ec2 = boto3.client('ec2')
resource = boto3.resource('ec2')

master_instance_id = ''


def get_slave_ids(master_id):
    slave_ids = []
    instances = resource.instances.all()
    for instance in instances:
        if instance.id == master_id:
            continue
        slave_ids.append(instance.id)


def get_slave_ips(slave_ids):
    slave_ips = [  # get internal ip address of all slave instances
        ec2.describe_instances(
            InstanceIds=[iid]
        )['Reservations'][0]['Instances'][0]['PrivateIpAddress'] for iid in slave_ids
    ]
    return slave_ips


if __name__ == '__main__':
    ips = get_slave_ids(get_slave_ids(master_instance_id))
    print('slave internal ips', ips)
