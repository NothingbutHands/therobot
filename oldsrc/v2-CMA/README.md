# TheRobot

TheRobot (needs better name) is intended to be a trading robot. Currently set to trade on Forex markets.

Through many iterations, it has been decided to use a deep machine learning approach to potentially develop working strategies.

Currently using [pycma](https://github.com/CMA-ES/pycma) and the underlying CMAEvolutoinStrategy class' .ask() .tell() interface for implementing [CMA-ES](https://sci-hub.se/https://doi.org/10.1162%2Fevco.2007.15.1.1).

This project pulled heavily from [estool](https://github.com/hardmaru/estool) and modified for compatibility with BTgym.

[BTgym](https://github.com/Kismuz/btgym) is an OpenAi gym environemtn for the [BackTrader](https://github.com/backtrader/backtrader) trading framewrok.


### config
* Using aws cloud formation via `parallelcluster`
* MPI used for messaging between workers (both local and remote)
* [mpi4py](https://github.com/mpi4py/mpi4py) built with `--mpicc=/usr/bin/mpicc.openmpi` for interfacing python with mpi backend
* [BTgym](https://github.com/Kismuz/btgym) built from source
* [OpenBLAS](https://github.com/xianyi/OpenBLAS) installed (Highly optimized Basic Linear Algebra Routines)
* numpy built linked against OpenBLAS (faster matrix computation)
* `base_os = ubuntu1604` (see .parallelcluster/config file)s
* `post_install` setup script provided by s3

#### How To: setup and train (broad-strokes)
* Setup AWS account and [aws cli](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)
* Install and setup [parallelcluster](https://github.com/aws/aws-parallelcluster)
* Replace ~/.parallelcluster/config with the following...
```
[aws]
aws_region_name = us-east-1
aws_access_key_id = 
aws_secret_access_key = 

[cluster default]
vpc_settings = public
key_name = legend
cluster_type = spot
master_instance_type = c5.2xlarge
compute_instance_type = c5.large
initial_queue_size = 20
max_queue_size = 20
maintain_initial_size = true
post_install = s3://clusterstartscripts/cluster-start.sh
#compute_root_volume_size = 40
base_os = ubuntu1604

extra_json = { "cluster" : { "ganglia_enabled" : "yes" } }

ebs_settings = helloebs

[vpc public]
master_subnet_id = subnet-cfbc36a8
vpc_id = vpc-d3cd6ba9
use_public_ips = true

[global]
update_check = true
sanity_check = true
cluster_template = default

[aliases]
ssh = ssh {CFN_USER}@{MASTER_IP} {ARGS}

[ebs helloebs]
ebs_snapshot_id = snap-04b25fa424eb17a9d
```
* Upload post-install script to s3 (or public accessible url)  
    `$ aws s3 cp --acl public-read /path/to/cluster-setup.sh s3://<bucket_name>/cluster-setup.sh`
    
cluster-start.sh  
```
#!/usr/bin/env bash

export CC=/usr/bin/mpicc.openmpi
export MPICC=/usr/bin/mpicc.openmpi
sudo apt-get install python3-dev gfortran python3-pip libopenblas-base -y
echo "python3-dev fortran pip3 and libopenblas installed" >> /shared/postinstall.out
sudo apt-get install python3-mpi4py python3-tk -y
pip3 install numpy
echo "mpi4py tk and numpy installed" >> /shared/postinstall.out
pip install pandas
cd /shared/btgym/
pip3 install -e .
echo "btgym installed" >> /shared/postinstall.out
pip3 install cma
sudo apt-get install htop -y
echo "backend : Agg" | sudo tee /usr/local/lib/python3.5/dist-packages/matplotlib/mpl-data/matplotlibrc
echo "Done."
echo "post install complete" >> /shared/postinstall.out

cp /shared/therobot/slave_killer.sh /tmp/
cp /shared/therobot/run_daemon.sh /tmp/
sudo chmod u+x /tmp/slave_killer.sh
sudo chmod u+x /tmp/run_daemon.sh
sudo cp /shared/therobot/slave_killer.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable slave_killer
sudo service slave_killer start

exit 0
```
* Ensure that the /shared/ dir (accessible by all nodes in cluster) has the following...  
    *Required dependancies compiled & installed by the post-install script*  
    1. btgym/        #git cloned source
    1. therobot/     # this projects source files  
* Start the cluster  
    `$ parallelcluster create mpicluster`  "mpicluster" is the name of the cluster
* Output of successful cluster creation will provide a public master node IP, ssh into master node  
    `$ ssh -i path/to/keyfile.pem ubuntu@MASTERNODEIP`  
    *note: make sure to `chmod 400` the pem file or aws will reject it*
* Run the training job  
    `ubuntu@masternode:$ cd /shared/therobot/`  
    `ubuntu@masternode:/shared/therobot/$ ./master.py total_workers`  
    
qslave.sh   
```
#!/usr/bin/env bash
# The #$ lines is called bowtie (a qsub thing)

#$ -cwd
#$ -N btevo
#$ -o btevo_output.log
#$ -e btevo_error.log
#$ -r y
date
/usr/bin/mpirun --mca orte_base_help_aggregate 0 --tag-output ./slave.py  >> evo.log
```
* View status of cluster  
    `ubuntu@masternode:$ qhost  # overview of cluster`  
    `ubuntu@masternode:$ qstat -f  # overview of job queue`

##### OPTIONAL: Modify launch template to set CPU spec
*Note: Only certain instance types can have omdified cpu spec (ex m5.large)*  

* create cluster  
    `parallelcluster create mycluster`  
    
* create launch template version and set as default (get launch template version from AWS console > Launch Templates)  
    `aws ec2 create-launch-template-version --launch-template-id lt-0841205c13be2ce21 --version-description OneCoreOneThread --source-version 1 --launch-template-data '{"CpuOptions": {"CoreCount": 1, "ThreadsPerCore": 1}}'`  
    `aws ec2 modify-launch-template --launch-template-id <new_template_id> --default-version 2`  
    
* modify auto scaling group to use version 2 of the launch template (get auto-scaling-group-name from AWS console > Auto Scaling Groups)  
    *note, after the previous two steps are done, only the next command needs to be run for future cluster creations*  
    `aws autoscaling update-auto-scaling-group --auto-scaling-group-name parallelcluster-fatboy-ComputeFleet-1WC24U7XTVN3Y --launch-template LaunchTemplateId=lt-07e7a40556bbad60f,Version=2`  
    
* stop and start cluster  
    `parallelcluster stop mycluster`  
    `parallelcluster start mycluster`  
    
* check that each instance only has one core and one thread per core  
    `aws ec2 describe-instances --filters 'Name=instance-state-name,Values=running' | egrep -i '(CoreCount|ThreadsPerCore)'`  

*> [list of instances that support cpu settings](https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/instance-optimize-cpu.html#cpu-options-supported-instances-values)*


##Update (11/5/2018): Phoenix
Finally got the project integrated with AWS.
This project can now be trained on a [High Performance Computing](https://en.wikipedia.org/wiki/Supercomputer) (HPC) cluster.

![16 core aws ec2 compute cluster ganglia console](https://imgoat.com/uploads/a7cdfad1a5/163137.png)
*Training on 16 core (8x t2.large nodes) aws ec2 compute cluster. [ganglia console]*


##Update (11/13/2018): Headbang
Worked out a bunch of kinks with the `post_install` script as well as the python code for training.  
Can now more efficiently train on a larger (>20 cores) aws compute cluster without wasting more CPU time to system (I/O, memory management, etc.).
![60 core aws ec2 compute cluster ganglia console](https://i.imgur.com/BZ2nbW1.png)  
*training on 60 core (30x c4.xlarge - 2 cpu 1 thread each) aws ec2 compute cluster. [ganglia console]* (ganglia is reporting the RAM incorrectly)


##Update (11/14/2018) Robinhood
Even batter performance on larger clusters.  
Setting the qsub parallel environments fill policy from $fill_up to $round_robin and then only giving 1-2 jobs per node seems to help tremendously.  
    `sudo su - sgeadmin`  
    `qconf -mp mpi`  
Also, returning to each slave being it's own `data_master` is better for performance over 1 `data_master` on larger clusters.  
To avoid any waste, I set each node to only have 1 core and 1 thread.  

#####Setup:
* nodes: 40 (1 "compute node" is `data_master`)
* episodes: 5  
* trials: 5  
* bars: 90  
* layers: [128, 128]  
* popsize = (nodes-1) * tials = 195
* memuse = ~1.4GB/node

![40 core c4.xlarge compute cluster](https://i.imgsafe.org/c440b5e942.png)
![40 core c4.xlarge compute cluster](https://i.imgur.com/fo6F6Rc.png)
    *40 core c4.xlarge compute cluster. [ganglia console]*  (ganglia is reporting the RAM incorrectly) 
The node not doing much [1, 1] is `data_master` who only does work to get candidate solutions, send them, receive them, and tell them to `cma`.

##Update (11/26/2018) swapboy
Using swap on rank 0 node for financial reasons. The master node doesn't need to have the fastest mem anyways.  
Necessary because the compute nodes need significantly less memory than the master node (e.x. 12.8GB for rank 0, 2.9GB for compute nodes)

Next up: handle when a spot instance goes down.


#####Setup:
* nodes: 10 (1 "compute node" is `data_master`)
* episodes: 5  
* trials: 5  
* bars: 90  
* layers: [1024, 1024]  
* popsize = (nodes-1) * tials = 45
* memuse = ~2.9GB/compute node
* master: 7.2GB mem + 5.6GB swap = 12.8GB
* total trial time = ~13 mins

![10 core c4.xlarge compute cluster](https://i.imgur.com/9zLcjVU.png)  
![10 core c4.xlarge compute cluster](https://i.imgur.com/F09sr0Q.png)  
*10 core c4.xlarge compute cluster*


##Update (11/27/2018) swapman
Using c4.2xlarge for compute fleet. 35GB of total memory for rank 0. In progress of handling when instances go down.

Have to handle when rank 0 node goes down as well. Have beginnings of rank 0 resubmitting when compute nodes go down.  
Currently untested.


#####Setup:
* nodes: 40 (1 "compute node" is `data_master`)  
* instance type: c4.2xlarge  
* episodes: 3  
* trials: 6  
* bars: 180  
* layers: [690, 690]  
* popsize = (nodes-1) * tials = 234
* memuse = ~3.4GB/compute node
* master: 12.0GB mem + 23.2GB swap = 35.2GB
* total trial time = ~22 mins
* ask time = ~2 mins
* tell time = ~6 mins

![10 core c4.2xlarge compute cluster](https://i.imgur.com/iUdIimr.png)  
![10 core c4.2xlarge compute cluster](https://i.imgsafe.org/d92b940ad7.png)  
![10 core c4.2xlarge compute cluster](https://i.imgur.com/0PqxXnp.png)  

*40 core c4.2xlarge compute cluster (ganglia is reporting the RAM incorrectly)*

##Update 3/31/19 Beta v0.1
TheRobot is now trainable on an AWS HPC EC2 compute cluster with a dedicated master node and spot instance compute 
nodes to make it more economically viable.

Whole buttload of stuff was updated. No longer sends packets along network using openMPI, instead using a shared network
 drive as means of communicating candidate solutions and receiving subsequent fitness results. However, openMPI is still 
 required for BTgym to create properly (don't ask, I have no idea).

####Lesson learned, never have more than 1 cpu per compute node...
You should never have more than 1 core on a compute node instance when running this..
For whatever reason, if more than one instance of `slave.py` is running on a node, even if there's more than enough RAM,
it will perform poorly, giving much CPU time over to `system`, instead of `user`, as is desired. 

So make use of `update_cpu_cores.sh` to change launch template to only have 1 cpu per node and `restart_cluster.sh` to apply the changes.

#####Setup:
* nodes: 20 
* instance type: c5.large
* episodes: 4  
* trials: 4  
* bars: 120  
* layers: [88, 88]  
* popsize = nodes * trials = 80
* memuse = ~1.1GB/compute node
* total trial time = ~3 mins

![ganglia](https://i.imgsafe.org/11/11193d5ff6.png)
**MAN! Look how nice that memusage is. Almost all user!**
![ganglia](https://i.imgsafe.org/11/11193565ed.png)


##Update 4/1/19 Making progress
Mkaing some forward progress. Was able to run a training round overnight without any degredation in performance or 
failtures due to lack of fault tolerance.

#####Setup:
* nodes: 20 
* master type: c5.2xlarge
* compute type: c5.large
* episodes: 4  
* trials: 6  
* bars: 180  
* trading days: 7
* layers: [166, 166]  
* popsize = nodes * trials = 120
* memuse = ~1.2GB/compute node, ~8.8CB/master node
* total trial time = ~5 mins

![ganglia](https://i.imgsafe.org/1f/1fa7a38dfa.png)

**Reward stats**
![reward stats](https://i.imgsafe.org/20/20ac104d31.png)
**Evaluation**
![evaluation](https://i.imgsafe.org/20/20ac0e2947.png)

##Update 4/3/19 Banger
More.

#####Setup:
* nodes: 20 
* master type: c5.4xlarge
* compute type: c5.large
* episodes: 4  
* trials: 6  
* bars: 240  
* trading days: 7
* layers: [256, 256]  
* popsize = nodes * trials = 120
* memuse = ~2.2GB/compute node, ~11.1CB/master node
* total trial time = ~6 mins

**Reward stats**
![reward stats](https://i.imgsafe.org/55/5587e1d259.png)
**Evaluation**
![evaluation](https://i.imgsafe.org/55/5587e04ac7.png)


##Update 6/08/19 Beta v1
*OHHH BABY!* I have finally gotten the project to run without degradation or crashing. Currently running for over five days straight and counting. 
I believe it is also fully fault tolerant. 

Converted over to `parallelcluster` from `cfncluster` as the latter is deprecated in favor of the former.  
This README has been updated to show setting up `pcluster` instead of `cfncluster`.

The major changes which facilitated the ability to train are...  
1. Created a service (`slave_killer.service`) that runs on the compute nodes which will run `pkill slave.py` when an indicator file is located  
(`/shared/kill_slaves`). This indicator file is created by master.py after each step.  
2. Run `master.py` in a loop (`master_master.sh`. When it eventually dies due to memory overuse, it will restart and pick up where it left off. 
(no less of compute time if it dies in the middle of processing a solution set)  
3. Store the seed state of `master.py` before each run so it can pick up where it left off if it dies. Furthermore, 
master now sends seeds along with the solution sets. This may be the reason the avg_reward spiked as you see below.  

The first two solutions are to alleviate the issue of python not releasing it's unused memory. This is apparently a common 
problem with python and a number of other languages. Forcing a `gc.collect()` has to effect here.

#####Setup:
* nodes: 20 
* master type: c5.2xlarge
* compute type: c5.large
* episodes: 5  
* trials: 6  
* bars: 240  
* trading days: 30
* layers: [512, 512]  
* popsize = nodes * trials = 120
* memuse = ~2.2GB/compute node, (master node will always fill up its usable memory)
* total trial time = ~30 mins

#### Reward graph
![reward graph](https://malzo.com/i/2019/06/12/tKQ.png)

### Ganglia

![ganglia console](https://ist6-1.filesor.com/pimpandhost.com/1/_/_/_/1/8/f/l/j/8flj0/mOQ.png)
![ganglia cluster](https://malzo.com/i/2019/06/08/mSR.png)

#### CPU view
![cpu view](https://i.ibb.co/SxKwnyp/image.png)

#### Processes view
![processes view](https://malzo.com/i/2019/06/16/t1m.png)

#### Memory View
![memory view](https://jssocial.pw/ppkey/fget/pic8/upload/5qdwn4vhoU.png)


###Useful References
- [.parallelcluster/config reference](https://parallelcluster.readthedocs.io/en/latest/configuration.html#base-os)
- [parallelcluster using startup script docs](https://parallelcluster.readthedocs.io/en/latest/pre_post_install.html)
- [qsub reference](http://docs.adaptivecomputing.com/torque/4-0-2/Content/topics/commands/qsub.htm)
- [mpi4py scatter/gather example](https://github.com/jbornschein/mpi4py-examples/blob/master/02-broadcast)
- [reference for building openmpi](https://github.com/open-mpi/ompi)
