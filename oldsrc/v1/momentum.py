import numpy as np
import oandapyV20.endpoints.orders as orders
import pandas as pd
from oandapyV20 import API, V20Error
from oandapyV20.contrib.requests import MarketOrderRequest, TakeProfitDetails, StopLossDetails
from oandapyV20.endpoints.pricing import PricingStream


class MomentumTrader(PricingStream):
    def __init__(self, momentum, account_id, access_token, hc,
                 instrument='EUR_USD', download_history=True,
                 *args, **kwargs):

        params = {'instruments': instrument}
        PricingStream.__init__(self, accountID=account_id, params=params,
                               *args, **kwargs)
        self.ticks = 0
        self.position = 0
        self.df = pd.DataFrame()
        self.momentum = momentum
        self.units = 100000
        self.connected = False
        self.accountID = account_id
        self.access_token = access_token
        self.instrument = instrument
        self.client = API(access_token=self.access_token)
        self.hc = hc

        if download_history:
            # hours = self.momentum / (5*60)  # (5_sec_intervals*mins_in_hour)
            data = self.hc.get_history(
                'M1', self.instrument
            )
            self.df = data['M1'][self.instrument]['candles']
            # self.df = get_history(
            #     self.client, self.instrument, 'M1',
            #     date_from=datetime.now()-timedelta(hours=hours)
            # )

    def create_order(self, units, **params):
        stop_loss = take_profit = None
        if 'stop_loss' in params:
            stop_loss = StopLossDetails(price=params['stop_loss']).data
        if 'take_profit' in params:
            take_profit = TakeProfitDetails(price=params['take_profit']).data
        order = orders.OrderCreate(
            accountID=self.accountID,
            data=MarketOrderRequest(
                instrument=self.instrument, units=units,
                takeProfitOnFill=take_profit,
                stopLossOnFill=stop_loss,
            ).data
        )
        response = self.client.request(order)
        print('\t', response)

    def on_success(self, data):
        self.ticks += 1
        print("ticks=", self.ticks)
        # print(self.ticks, end=', ')
        closeoutAsk = pd.to_numeric(data['closeoutAsk'])
        print('closeoutAsk({})={}'.format(type(closeoutAsk), closeoutAsk))
        # appends the new tick data to the DataFrame object
        self.df = self.df.append(
            pd.DataFrame(
                [
                    {
                        'time': data['time'],
                        'closeoutAsk': closeoutAsk
                    }
                ],
                index=[data["time"]]
            )
        )
        # transforms the time information to a DatetimeIndex object
        self.df.index = pd.DatetimeIndex(self.df["time"])

        # Convert items back to numeric (Why, OANDA, why are you returning strings?)
        self.df['closeoutAsk'] = pd.to_numeric(
            self.df["closeoutAsk"], errors='ignore'
        )

        # resamples the data set to a new, homogeneous interval
        dfr = self.df.resample('5s').last().bfill()

        # calculates the log returns
        dfr['returns'] = np.log(dfr['closeoutAsk'] / dfr['closeoutAsk'].shift(1))

        # derives the positioning according to the momentum strategy
        dfr['position'] = np.sign(
            dfr['returns'].rolling(self.momentum).mean()
        )

        stop_loss = .0007  # .07%
        take_profit = .001  # .1%

        print("position=", dfr['position'].ix[-1])
        if dfr['position'].ix[-1] == 1:
            params = {'stop_loss': closeoutAsk * (1 - stop_loss),
                      'take_profit': closeoutAsk * (1 + take_profit)}
            print("go long")
            if self.position == 0:
                self.create_order(self.units, **params)
            elif self.position == -1:
                self.create_order(self.units * 2, **params)
            self.position = 1
        elif dfr['position'].ix[-1] == -1:
            params = {'stop_loss': closeoutAsk * (1 + stop_loss),
                      'take_profit': closeoutAsk * (1 - take_profit)}
            print("go short")
            if self.position == 0:
                self.create_order(-self.units, **params)
            elif self.position == 1:
                self.create_order(-self.units * 2, **params)
            self.position = -1
        if self.ticks == 25000:
            print("close out the position")
            if self.position == 1:
                self.create_order(-self.units)
            elif self.position == -1:
                self.create_order(self.units)
            self.disconnect()

    def disconnect(self):
        self.connected = False

    def rates(self, **params):
        self.connected = True
        params = params or {}
        ignore_heartbeat = None
        if "ignore_heartbeat" in params:
            ignore_heartbeat = params['ignore_heartbeat']
        while self.connected:
            response = self.client.request(self)
            try:
                for tick in response:
                    if not self.connected:
                        break
                    if not (ignore_heartbeat and tick["type"] == "HEARTBEAT"):
                        print(tick)
                        self.on_success(tick)
            except V20Error as e:
                print('ERROR({type}): {errMsg}'.format(
                    type=type(e), errMsg=e
                ))
                continue
