import numpy as np
import oandapyV20.endpoints.instruments as instruments
import pandas as pd
import seaborn as sns
from dateutil import parser
from matplotlib import pyplot as plt
# v20 OANDA API - 3rd party
from oandapyV20 import API  # the client

from oldsrc.v1.history import HistoryCache
from oldsrc.v1.momentum import MomentumTrader

sns.set()

from oandapyV20.definitions.instruments import definitions as InstrDeffs
granularities = InstrDeffs.keys()
# ACCESS_TOKEN = os.environ.get('OANDA_API_ACCESS_TOKEN', None)
# ACCOUNT_ID = os.environ.get('OANDA_API_ACCOUNT_ID', None)
access_token = '35c6e5997b201ee4251db9fef3ad0770-2dea22e3458e32c614791089af73efcb'
account_id = '101-001-9244391-001'

client = API(access_token=access_token)

# The v20 api handles from times a little differently - be careful of the timezone
params = {
    "from": parser.parse("2016-12-07 18:00:00 EDT").strftime('%s'),
    "to": parser.parse("2016-12-10 00:000:00 EDT").strftime('%s'),
    "granularity": 'M1',
    "price": 'A'
}
r = instruments.InstrumentsCandles(instrument="EUR_USD", params=params)
data = client.request(r)
results = [
    {
        "time": x['time'],
        "closeAsk": float(x['ask']['c'])
    } for x in data['candles']
]
df = pd.DataFrame(results).set_index('time')
df.index = pd.DatetimeIndex(df.index)
df.info()

df['returns'] = np.log(df['closeAsk'] / df['closeAsk'].shift(1))

cols = []
for momentum in [15, 30, 60, 120]:
    col = 'position_%s' % momentum
    df[col] = np.sign(df['returns'].rolling(momentum).mean())
    cols.append(col)

# Matplotlib
strats = ['returns']

for col in cols:
    strat = 'strategy_%s' % col.split('_')[1]
    df[strat] = df[col].shift(1) * df['returns']
    strats.append(strat)

df[strats].dropna().cumsum().apply(np.exp).plot()

plt.show()

hc = HistoryCache(client)
# Set momentum to be the number of previous 5 second intervals to calculate against
# For two hours, momentum = 120 * 5 = 600
# I am using momentum = 6 to speed things up (but it is a terrible prediction!)
mt = MomentumTrader(
    momentum=600, account_id=account_id, hc=hc,
    access_token=access_token, instrument='EUR_USD'
)

mt.rates(ignore_heartbeat=True)
