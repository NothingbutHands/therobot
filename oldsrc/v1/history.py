import time
from collections import defaultdict
from datetime import datetime, timedelta
from functools import wraps, lru_cache

import pandas as pd
from oandapyV20 import API
from oandapyV20.contrib.factories import InstrumentsCandlesFactory
from oandapyV20.endpoints import instruments

from oldsrc.v1.main import granularities


def stringify_dates(f):
    """
    Converts all kwargs starting with 'date_'-
    into proper format, that is %Y-%m-%dT%H:%M:%SZ
    Assumes kwargs are datetime objects, ignores otherwise.
    Stores original object in _KWARG, as such functions need-
    a **kwargs argument
    """
    @wraps(f)  # preserve __name__ & other attributes
    def wrapper(*args, **kwargs):
        date_args = [arg for arg in kwargs.keys() if arg.startswith('date_')]
        for key in date_args:
            kwarg = kwargs[key]
            try:
                stringified = kwarg.strftime('%Y-%m-%dT%H:%M:%SZ')
            except AttributeError:
                continue
            else:
                print('stringified {}'.format(key))
                kwargs['_'+key] = kwarg  # preserve original object
                kwargs[key] = stringified
        return f(*args, **kwargs)
    return wrapper


def cnv(r, h):
    # get all candles from the response and write them as a record to the filehandle h
    for candle in r.get('candles'):
        ctime = candle.get('time')[0:19]
        try:
            rec = "{time},{complete},{o},{h},{l},{c},{v}".format(
                time=ctime,
                complete=candle['complete'],
                o=candle['mid']['o'],
                h=candle['mid']['h'],
                l=candle['mid']['l'],
                c=candle['mid']['c'],
                v=candle['volume'],
            )
        except Exception as e:
            print(e, r)
        else:
            h.write(rec+"\n")


@stringify_dates
def get_history(cl, instrument, granularity, date_from, **kwargs):
    params = {
        "from": date_from,
        "granularity": granularity,
        "includeFirst": True,
        "count": 5000,  # max
    }

    r = instruments.InstrumentsCandles(instrument=instrument, params=params)
    response = cl.request(r)
    df = pd.DataFrame()
    candles = response['candles']
    for candle in candles:
        mid = candle['mid']
        df = df.append(
            pd.DataFrame(
                [
                    {
                        'time': candle['time'],
                        'closeoutAsk': mid['c']
                    }
                ],
                index=[candle["time"]]
            )
        )
    return df


@stringify_dates
def download_large_history(cl, instrument, granularity, date_from, to=None, **kwargs):
    """
    Downloads history and write to file.
    Useful for large history downloads as other method has limits
    filename format: EUR_USD M15 Sep 08 1969-Sep 11 2001.hist
    """
    to = to or datetime.now().strftime('%b %d %Y')
    df = kwargs['_date_from'].strftime('%b %d %Y')  # See decorator
    filename = '{instr} {g} {df}-{dt}.hist'.format(
        instr=instrument, dt=to, g=granularity, df=df
    )
    filepath = 'history/{}'.format(filename)

    params = {
        'from': str(date_from),
        'granularity': granularity
    }

    with open(filepath, "w") as f:
        n = 0
        for r in InstrumentsCandlesFactory(instrument=instrument, params=params):
            rv = cl.request(r)
            cnt = len(r.response.get('candles'))
            print("REQUEST: {} {} {}, received: {}".format(r, r.__class__.__name__, r.params, cnt))
            n += cnt
            cnv(r.response, f)
        print("Check the datafile: {} under /tmp!,"
              " it contains {} records".format(filepath, n))


@lru_cache(16)
def granularity_to_minutes(granularity):
    g2m = {
        'M1':   1,    'M5': 5,
        'M15':  15,  'M30': 30,
        'H1':   60,   'H2': 120,
        'H4':   240,  'D1': 1440
    }

    if granularity not in g2m:
        errMsg = 'granularity {} not recognized'.format(granularity)
        err = AttributeError(errMsg)
        raise err
    else:
        return g2m[granularity]


def string_to_datetime(date_string, _format='%Y-%m-%dT%H:%M:%S.%fZ'):
    # datetime does not support nanoseconds.
    # Oanda returns strings with nanoseconds.
    # The following preserves up to the microsecond
    # so datetime.strptime can be used.
    start = date_string.rfind('.')+1
    nanos = date_string[start:-1]
    if len(nanos) > 6:
        date_string = date_string[:start+6]+'Z'
    return datetime.strptime(date_string, _format)


class HistoryCache:
    """
    Holds candle histories.
    Retrieves new candles if outdated.
    Useful for reducing requests to download candles.
    { granularity: { instrument: {
                        'candles': pd.DataFrame,
                        'time': '2018-01-01T00:00:00.000000Z'
                    }, ... }, ... }
    """
    def __init__(self, client, cap=3000):
        self.client = client
        self.cap = cap
        self.history = {
            gran: defaultdict(dict) for gran in granularities
        }
        # self.history = {
        #     'M1':   defaultdict(dict),
        #     'M5':   defaultdict(dict),
        #     'M15':  defaultdict(dict),
        #     'M30':  defaultdict(dict),
        #     'H1':   defaultdict(dict),
        #     'H2':   defaultdict(dict),
        #     'H4':   defaultdict(dict),
        #     'D1':   defaultdict(dict),
        # }

    def __getitem__(self, key):
        return self.history[key]

    def __add__(self, other):
        """
        Adds candles without duplicates.
        `other` entries take priority over self.history
        :param other: { gran: { instr: { 'candles': pd.DataFrame or [candles]
                            'time': datetime } }, ... }
        """
        for gran in other.keys():
            for instr, instr_dict in other[gran].items():
                candles = instr_dict['candles']
                time = instr_dict['time']
                hdf = self[gran][instr]['candles']
                if type(candles) is list:
                    candles = pd.DataFrame(
                        data=candles,
                        index=[candle['time'] for candle in candles]
                    )
                self[gran][instr]['candles'] = hdf.combine_first(candles)
                self[gran][instr]['time'] = time

    def is_fresh(self, granularity, instrument):
        if instrument not in self[granularity]:
            return False

        minutes = granularity_to_minutes(granularity)

        last_refresh = self[granularity][instrument]['time']
        last_refresh = string_to_datetime(last_refresh)
        now = datetime.now()

        # Must subtract current interval from current time
        # This is to ensure that freshness is based on complete
        # candles, not the current candle
        if minutes < 60:
            curr_time = int((now.minute-minutes)/minutes)
            last_time = int(last_refresh.minute/minutes)
            if curr_time != last_time:
                # not fresh
                return False
            else:
                return True
        elif 60 <= minutes < 1440:  # Hours
            hours = minutes/60
            curr_time = int((now.hour-hours)/hours)
            last_time = int(last_refresh.hour/hours)
            if curr_time != last_time:
                # not fresh
                return False
            else:
                return True
        else:
            days = minutes/1440
            curr_time = int((now.day-days)/days)
            last_time = last_refresh.day/days
            if curr_time != last_time:
                # not fresh
                return False
            else:
                return True

    def get_history(self, granularity='M5', instruments=list('EUR_USD')):
        if type(instruments) is not list:
            instruments = [instruments]
        ret = {granularity: defaultdict(dict)}
        for instrument in instruments:
            if instrument not in self[granularity]:
                self.download_and_replace_history(
                    granularity, instruments, self.cap
                )

            if self.is_fresh(granularity, instrument):
                ret[granularity][instrument]['candles'] = self[granularity][instrument]['candles']
                ret[granularity][instrument]['time'] = self[granularity][instrument]['time']
            else:
                # update history
                hist = self._download_history(
                    granularity, instrument,
                    date_from=self[granularity][instrument]['time'],
                    count=5  # reduce computation time when inserting
                )
                self + hist
                ret[granularity][instrument]['candles'] = self[granularity][instrument]['candles']
                ret[granularity][instrument]['time'] = self[granularity][instrument]['time']
        return ret

    def download_and_replace_history(self, granularity, instruments, count):
        minutes = granularity_to_minutes(granularity) * count
        date_from = datetime.now() - timedelta(minutes=minutes)

        if len(instruments) <= 0:
            raise AttributeError('No instruments provided')

        for instrument in instruments:
            candles, time = self.__download_history(
                granularity, instrument, date_from=date_from
            )
            self[granularity][instrument]['candles'] = pd.DataFrame(
                data=candles, index=[candle['time'] for candle in candles]
            )
            self[granularity][instrument]['time'] = time

    def _download_history(self, granularity, instrument, date_from, count=5000):
        candles, time = self.__download_history(
            granularity, instrument, count=count,
            date_from=date_from,
        )
        formatted = {
            granularity: {
                instrument: {
                    'candles': candles,
                    'time': time
                }
            }
        }
        return formatted

    @stringify_dates
    def __download_history(self, granularity, instrument, date_from, count=5000, **kwargs):
        params = {
            "from": date_from,
            "granularity": granularity,
            "count": count,  # max is 5000
        }

        r = instruments.InstrumentsCandles(
            instrument=instrument, params=params
        )
        response = self.client.request(r)
        candles = response['candles']
        if not candles[-1]['complete']:  # last candle is usually incomplete
            candles = candles[:-1]
        return candles, candles[-1]['time']


if __name__ == '__main__':
    client = API(access_token='35c6e5997b201ee4251db9fef3ad0770-2dea22e3458e32c614791089af73efcb')
    # download_large_history(
    #     client, 'EUR_USD', 'M15',
    #     date_from=datetime(2016, 3, 1)
    # )
    hc = HistoryCache(client)
    print(hc.get_history('M1', ['EUR_USD']))
    time.sleep(90)
    print(hc.get_history('M1', ['EUR_USD']))
