import warnings

import joblib

warnings.simplefilter(action='ignore', category=FutureWarning)

import tensorflow as tf
tf.get_logger().setLevel(3)

import os
import shutil
import time
from math import ceil

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import tensorflow as tf
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler
from tensorflow import keras
from tensorflow.keras.callbacks import Callback
from tensorflow.python.keras import Input
from tensorflow.python.keras.callbacks import ModelCheckpoint
from tensorflow.python.keras.engine.training import Model
from tensorflow.python.keras.layers import GaussianNoise, Dense, Dropout, LSTM, Bidirectional, LocallyConnected2D
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.utils import to_categorical

from util.data_loader import import_data, label_distribution
from util.config_manager import config_select_prompt, optimizer_lookup
from util.utils import l_AYER, Indicator

if not os.getcwd().replace('/', '').endswith('therobot'):
    os.chdir('../')

METRICS = [
    keras.metrics.TruePositives(name='tp'),
    keras.metrics.FalsePositives(name='fp'),
    keras.metrics.TrueNegatives(name='tn'),
    keras.metrics.FalseNegatives(name='fn'),
    keras.metrics.CategoricalAccuracy(name='accuracy'),
    keras.metrics.Precision(name='precision'),
    keras.metrics.Recall(name='recall'),
    keras.metrics.AUC(name='auc'),
    keras.metrics.MeanSquaredError(name='mse'),
    keras.metrics.RootMeanSquaredError(name='rmse'),
    keras.metrics.CategoricalHinge(name='categorical_hinge'),
    keras.metrics.SensitivityAtSpecificity(0.99, name='TP_0.99'),
    keras.metrics.SpecificityAtSensitivity(0.99, name='TN_0.99')
]


def make_model_sequential(metrics=METRICS, output_bias=None):
    if output_bias is not None:
        output_bias = keras.initializers.Constant(output_bias)

    model = keras.Sequential([
        keras.layers.Dense(
            cfg.hidden_units[0], activation='softsign',
            input_shape=(train_features.shape[-1],)
        ),
        keras.layers.GaussianNoise(stddev=cfg.noise_std),
        keras.layers.Dense(
            cfg.hidden_units[1], activation='softsign',
        ),
        keras.layers.GaussianNoise(stddev=cfg.noise_std),
        keras.layers.Dense(
            cfg.hidden_units[2], activation='tanh'
        ),
        keras.layers.Dropout(0.2),
        keras.layers.Dense(4, activation='softmax',
                           bias_initializer=output_bias),
    ])

    model.compile(
        optimizer=keras.optimizers.Adam(lr=cfg.learning_rate),
        loss=keras.losses.CategoricalCrossentropy(),
        metrics=metrics
    )

    return model


def make_model(metrics=METRICS, output_bias=None):
    if output_bias is not None:
        output_bias = keras.initializers.Constant(output_bias)

    input_layer = Input(shape=train_features.shape[-1], name='input-layer', dtype='float32')

    x = None
    for layer in cfg.layers:
        if layer.type == 'noise':
            x = GaussianNoise(layer.noise_std)(input_layer if x is None else x)
        elif layer.type == 'hidden':
            x = Dense(layer.units, activation=layer.activation)(input_layer if x is None else x)
        elif layer.type == 'dropout':
            x = Dropout(layer.dropout)(input_layer if x is None else x)
        elif layer.type == 'gumbel_softmax':
            # not currently functional
            raise NotImplementedError("Not fully implemented yet, sorry :(")
            # x = GumbelSoftmax()(input_layer if x is None else x, layer.temperature)
        elif layer.type == 'fully_connected':
            x = LocallyConnected2D(layer.units, kernel_size=layer.kernel_size, strides=layer.stride)(input_layer if x is None else x)
        elif layer.type == 'lstm':
            x = tf.expand_dims(x, axis=-1)  # lstm layer expects 3D input
            lstm = LSTM(layer.units,
                        activation=layer.activation or 'tanh',
                        recurrent_activation=layer.recurrent_activation or 'hard_sigmoid')
            if layer.bidirectional:
                lstm = Bidirectional(lstm)
            
            x = lstm(input_layer if x is None else x)

    x = Dense(4, activation=cfg.output_activation, bias_initializer=output_bias)(x)  # tf.random_normal_initializer(mean=0.0, stddev=2, seed=cfg.seed)

    model = Model(input_layer, x)
    optimizer = cfg.optimizer
    optimizer = optimizer_lookup[optimizer](learning_rate=cfg.learning_rate)
    model.compile(
        optimizer=optimizer,
        # optimizer=keras.optimizers.Adagrad(learning_rate=cfg.learning_rate),
        loss='categorical_crossentropy',
        metrics=metrics
    )

    return model


def plot_loss(history, label, n, to_file=None):
    # Use a log scale to show the wide range of values.
    plt.semilogy(history.epoch, history.history['loss'],
                 color=colors[n], label='Train ' + label)
    plt.semilogy(history.epoch, history.history['val_loss'],
                 color=colors[n], label='Val ' + label,
                 linestyle="--")
    plt.xlabel('Epoch')
    plt.ylabel('Loss')

    plt.legend()

    if to_file:
        plt.savefig(to_file)


def plot_metrics(history, to_dir=None):
    if not os.path.exists(to_dir):
        try:
            os.mkdir(to_dir)
        except:
            pass

    metrics = ['loss', 'auc', 'precision', 'recall', 'mse', 'rmse', 'categorical_hinge', 'TP_0.99', 'TN_0.99']
    for n, metric in enumerate(metrics):
        name = metric.replace("_", " ").capitalize()
        plt.subplot(ceil(len(metrics)/2.), 2, n + 1)
        plt.plot(history.epoch, history.history[metric], color=colors[0], label='Train')
        plt.plot(history.epoch, history.history['val_' + metric],
                 color=colors[0], linestyle="--", label='Val')
        plt.xlabel('Epoch')
        plt.ylabel(name)
        if metric == 'loss':
            plt.ylim([0, plt.ylim()[1]])
        elif metric == 'auc':
            plt.ylim([0.85, 1])
        elif metric.startswith('TP_') or metric.startswith('TN_'):
            plt.ylim(min(history.history[metric]), max(history.history[metric]))
        else:
            plt.ylim([0, 1])

        plt.legend()

    if to_dir:
        plt.savefig(os.path.join(to_dir, 'metrics.png'))


def plot_cm(labels, predictions, to_file=None):
    cm = confusion_matrix(labels, predictions)
    plt.figure(figsize=(5, 5))
    sns.heatmap(cm, annot=True, fmt="d")
    plt.title('Multi-Class Confusion matrix')
    plt.ylabel('Actual label')
    plt.xlabel('Predicted label')

    if to_file:
        plt.savefig(os.path.normpath(to_file))


class NBatchLogger(Callback):
    """
    A Logger that log average performance per `display` steps.
    """
    def __init__(self, display):
        super().__init__()

        self.step = 0
        self.display = display
        self.metric_cache = {}
        self.last_print = None

    def on_epoch_end(self, batch, logs={}):
        for k in self.params['metrics']:
            if k in logs:
                self.metric_cache[k] = self.metric_cache.get(k, 0) + logs[k]
        if self.step % self.display == 0:
            metrics_log = ''
            for (k, v) in self.metric_cache.items():
                val = v / self.display
                if abs(val) > 1e-3:
                    metrics_log += ' - %s: %.4f' % (k, val)
                else:
                    metrics_log += ' - %s: %.4e' % (k, val)
            print('epoch: {}/{} time since last {} ... {}'.format(
                      self.step,
                      cfg.epochs,
                      round(time.time()-self.last_print, 2) if self.last_print else None,
                      metrics_log
                    ))
            print()
            self.metric_cache.clear()
            self.last_print = time.time()
        
        self.step += 1


def format_model_summary(string):
    def chunker(seq, size):
        return [seq[pos:pos + size] for pos in range(0, len(seq), size)]
    
    first_line = string[:string.find('___')]
    s = string[string.find('___'):]
    x = chunker(s, 65)
    x = [str(os.linesep).join(f) for f in x]
    return str(os.linesep).join([first_line]+x)


mpl.rcParams['figure.figsize'] = (12, 10)
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']


if __name__ == '__main__':
    inherit_from = ''
    model_name = 'test'
    cfg = config_select_prompt(model_name, inherit_from, **dict(
        training_data_fp='annotations2.json',
        layers=[
            l_AYER(u=256, activation='softplus'),
            l_AYER('noise', noise_std=.1),
            # l_AYER('dropout', dropout=0.1),
            l_AYER(u=512, activation='sigmoid'),
            # l_AYER('noise', noise_std=.1),
            # l_AYER('dropout', dropout=0.1),
            l_AYER(u=1024, activation='sigmoid'),
            l_AYER('locally_connected', u=128, kernel_size=(5, 5), stride=(1, 1)),
            l_AYER(u=40, activation='softplus')
            # l_AYER('lstm', units=64, bidirectional=False, activation='tanh', recurrent_activation='hard_sigmoid')
        ],
        output_activation='softmax',
        optimizer='adam',
        batch_size=4096,
        window_size=120,  # total number of input bars
        include_time=True,
        compute_weight_ratios=False,
        as_df=True,
        log_norm_volume=True,
        numeric_labels=True,
        track_position_state=True,
        epochs=20000,
        learning_rate=10 ** -5,
        save_every_x_steps=50,
        norm_all=True,
        augment={
            'reverse': True,
            'SMOTE': {},
            'label_radius': {'radius': 3}
        },
        early_stopping=dict(
            monitor='val_auc',
            min_delta=10 ** -4,
            verbose=1,
            patience=500,
            mode='max',
            restore_best_weights=True
        ),
        indicators=[
            Indicator('SMA', period=24),
            Indicator('AO', slow_period=34, fast_period=5),
            Indicator('OBV', norm=True),
            Indicator('MACD'),
            Indicator('ICHIMOKU'),
            Indicator('EMA'),
            Indicator('BASPN'),
        ]
    ))

    ret = import_data(
        cfg.training_data_fp, bar_count=cfg.window_size,
        test_split=cfg.test_split, shuffle=cfg.shuffle, seed=cfg.seed,
        truncate=cfg.truncate, include_time=cfg.include_time,
        compute_weight_ratios=cfg.compute_weight_ratios,
        as_df=cfg.as_df, log_norm_volume=cfg.log_norm_volume,
        numeric_labels=cfg.numeric_labels,
        min_max_tohlcv=cfg.norm_all, augment=cfg.augment,
        indicators=cfg.indicators, track_position_state=cfg.track_position_state,
    )
    if len(ret) == 4:
        train, test, validate, ratios = ret
    else:
        train, test, validate = ret

    total, hold_labels_count, c = label_distribution(cfg.training_data_fp)
    other_labels_count = sum([c[1] for c in c.most_common()[1:]])
    print('Total: {}\nHold: {} ({:.2f}% of total)\n'.format(
        total, hold_labels_count, 100 * hold_labels_count / total
    ))

    # Form np arrays of labels and features.
    train_labels = np.array(train.pop('Label'))
    bool_train_labels = train_labels != 0
    val_labels = np.array(validate.pop('Label'))
    test_labels = np.array(test.pop('Label'))

    train_features = np.array(train, dtype='float32')
    val_features = np.array(validate, dtype='float32')
    test_features = np.array(test, dtype='float32')

    scaler = StandardScaler()
    train_features = scaler.fit_transform(train_features)

    # save scaler
    joblib.dump(scaler, os.path.join(cfg.model_dir_path, 'scaler.gz'))

    val_features = scaler.transform(val_features)
    test_features = scaler.transform(test_features)

    train_features = np.clip(train_features, -5, 5)
    val_features = np.clip(val_features, -5, 5)
    test_features = np.clip(test_features, -5, 5)

    print()

    print('Training labels shape:', train_labels.shape)
    print('Validation labels shape:', val_labels.shape)
    print('Test labels shape:', test_labels.shape)

    print('Training features shape:', train_features.shape)
    print('Validation features shape:', val_features.shape)
    print('Test features shape:', test_features.shape)

    #############
    # CALLBACKS #
    #############

    early_stopping = keras.callbacks.EarlyStopping(
        monitor=cfg.early_stopping['monitor'],
        min_delta=cfg.early_stopping['min_delta'],
        verbose=cfg.early_stopping['verbose'],
        patience=cfg.early_stopping['patience'],
        mode=cfg.early_stopping['mode'],
        restore_best_weights=cfg.early_stopping['restore_best_weights']
    )

    tensorboard_callback = keras.callbacks.TensorBoard(
        log_dir='.{}Graph'.format(os.sep),
        histogram_freq=200,
        write_graph=True,
        write_images=True,
        write_grads=True
    )

    try: os.mkdir(cfg.model_dir_path)
    except: pass
    time.sleep(1.2)

    ckpoint = ModelCheckpoint(
        cfg.model_path,
        monitor=cfg.early_stopping['monitor'],
        verbose=0,
        save_best_only=cfg.early_stopping['restore_best_weights'],
        mode=cfg.early_stopping['mode'],
        period=cfg.save_every_x_steps
    )

    cat_train_labels = to_categorical(train_labels)
    if False:
        no_init_model = make_model()
        print('Without Bias Init ...')
        # print(no_init_model.predict(train_features[:10]))

        results = no_init_model.evaluate(train_features, to_categorical(train_labels), batch_size=cfg.batch_size, verbose=0)
        print("Loss: {:0.4f}".format(results[0]))

        initial_bias = np.log([other_labels_count / hold_labels_count])
        print(initial_bias)

        model = make_model(output_bias=initial_bias)
        print('\nWith Bias Init ...')
        # print(model.predict(train_features[:10]))

        results = model.evaluate(train_features, to_categorical(train_labels), batch_size=cfg.batch_size, verbose=0)
        print("Loss: {:0.4f}".format(results[0]))

        zero_bias_history = model.fit(
            train_features,
            cat_train_labels,
            batch_size=cfg.batch_size,
            epochs=15,
            validation_data=(val_features, to_categorical(val_labels)),
            verbose=2
        )

        initial_weights = os.path.join(tempfile.mkdtemp(), 'initial_weights')
        model.save_weights(initial_weights)

        keras.backend.clear_session()  # needed when using GPU as GPU won't flush memory and can crash

        model = make_model()
        model.load_weights(initial_weights)
        careful_bias_history = model.fit(
            train_features,
            cat_train_labels,
            batch_size=cfg.batch_size,
            epochs=15,
            validation_data=(val_features, to_categorical(val_labels)),
            verbose=2
        )

        print('Careful Bias Init - Evaluating Loss @Epcoh20 ...')
        plot_loss(zero_bias_history, "Zero Bias", 0)
        plot_loss(careful_bias_history, "Careful Bias", 1, to_file=os.path.normpath('{}/zero_vs_careful_bias_loss.png'.format(cfg.plot_path)))
        print('Careful Bias Init - Evaluation Complete')

        keras.backend.clear_session()  # needed when using GPU as GPU won't flush memory and can crash

    try:
        # remove tensorboard graph dir, if exists.
        shutil.rmtree('Graph{}'.format(os.sep))
        time.sleep(1.2)
    except:
        pass

    if not cfg.load_model:
        if cfg.compute_weight_ratios:
            # create model, applying bias initialization to output layer
            initial_bias = np.log([other_labels_count / hold_labels_count])
            print('Creating model with bias_initializer set to:', initial_bias)

            model = make_model(output_bias=initial_bias)
        else:
            # create model, without bias init
            model = make_model()
    else:
        # load previously checkpointed model
        m_path = cfg.model_path
        if len(cfg.inherits_from) > 0:
            m_path = cfg.parent_model_path
        print('Loading model from {} ...'.format(m_path))
        time.sleep(2)
        model = load_model(m_path)

    # model.load_weights(initial_weights)
    baseline_history = model.fit(
        train_features,
        cat_train_labels,
        batch_size=cfg.batch_size,
        epochs=cfg.epochs,
        verbose=0,
        callbacks=[tensorboard_callback, early_stopping, NBatchLogger(cfg.save_every_x_steps), ckpoint],
        validation_data=(val_features, to_categorical(val_labels))
    )
    plot_metrics(baseline_history, to_dir=cfg.plot_path)

    train_predictions_baseline = model.predict(train_features, batch_size=cfg.batch_size)
    test_predictions_baseline = model.predict(test_features, batch_size=cfg.batch_size)

    baseline_results = model.evaluate(test_features, to_categorical(test_labels),
                                      batch_size=cfg.batch_size, verbose=0)

    out_metrics = []
    for name, value in zip(model.metrics_names, baseline_results):
        out_metrics.append('{}: {}'.format(name, value))

    with open(os.path.join(cfg.config_path, 'metrics_summary.txt'), 'w') as f:
        f.write('\n'.join(out_metrics))

    stringlist = []
    model.summary(print_fn=lambda x: stringlist.append(x))
    short_model_summary = "\n".join(stringlist)
    with open(os.path.join(cfg.config_path, 'model_summary.txt'), 'w') as f:
        f.write(short_model_summary)

    print('plotting  cm')
    plot_cm(test_labels, test_predictions_baseline.argmax(axis=1),
            to_file='{}/confusion_matrix.png'.format(cfg.plot_path))

    test_predictions_baseline.argmax(axis=1)
