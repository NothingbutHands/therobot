# The Robot
*Still need a better name*

:clock1: Previous Revisions ...

* [v1](oldsrc/v1)  -- *you can ignore this*
* [v2](oldsrc/v2-CMA)  -- *fully implemented approach*

#### v3.0 Codename: "Trader Joe"
I have now made two attempts at implementing this trading model. 

The old sources files can be found in `oldsrc/` 

```
v1 = first attempt, abandoned before completion
v2-CMA = second attempt, fully implemented, using CMA-Evoluation Strategy for blackbox optimization
```

### Why abandon v2?

Mainly because it's inefficient. The CMA-ES implementation does not lend itself well to parallel 
computing when the dimensionality increases to arbitrarily large sizes, ie. n > 10,000. With the 
implementation of a Restricted Gaussian Sampler allows for the dimension to increase to >100,000 
with drastic performance improvements; HOWEVER, there is still a significant bottleneck in computing time for the master step when the 
dimension increases past 100,000-200,000. In such a case, a single step can take multiple minutes to compute *after* the 
candidate solutions have been evaluated.

Additionally, the blackbox optimization approach seems impractical when one considers that it is easy to 
determine when an action should have been taken. Hindsight is 20/20. Looking at historical market data, 
it is rudimentary to identify points at which a long/short/hold/close should be executed. Hence, it's 
inefficient to require that the agent "learns" how to trade on its own each time, when instead the agent could be 
fed all the *labeled* market data, which indicates explicitly when an action should be taken.

### New Approach

The **new approach** is to ...

1. Manually label market data to explicitly define when actions should be taken
1. Train a classification model -- change architecture and/or features until profitable model achieved

I seldom see projects where market data is labeled with what actions should be taken, and then a classifier is 
trained to generalize the actions. Instead, most AI/robot trading algo's take some other 
approach to heuristically, or otherwise mathematically, discerning patterns to indicate trade opportunities. 
A good example of a heuristic strategy is using indicator events (such as a MACD or Moving Average crossover),
to indicate when long/short positions should be opened and/or closed. It's fixed mathematics, but 
here, the desire is to utilize the "fuzzy-logic" achieved by ML approaches. 

So the question became, "why bother with a complex blackbox ML approach when, instead, all the actions the agent should take are 
predefined?" The approach of using labeled market data is more scalable. As well, it's easier to test various models and architectures 
as the training dataset remains constant. There's no need to change the training data labels, as they will already 
define the most profitable actions; instead, the focus becomes one of feeding the agent 
enough data, and using the right model, for it to generalize effectively outside of the observed dataset. 

#### Dropping BackTrader (for now)
This time around, I'm going to work on implementing the model training process and training data 
compilation, before I bother integrating it into something like [BackTrader](https://github.com/mementum/backtrader).

Using BackTrader is critical when the project is at the point where evaluation of the 
actions taken by the agent need to be analyzed (ie. see a market bar chart with all actions marked on the char). 
Until then, the focus will be on the raw accuracy scores produced when the model is evaluated against the test set. 

This should be interesting...

# v3.0 - "Trader Joe"
Let's begin.

:mag: Overview

:ballot_box_with_check: Develop Annotation Tool to label market data (produce training data)  
:ballot_box_with_check: Label a bunch of market data (at least 5 yrs of EUR/USD, to start)  
:ballot_box_with_check: Develop tensorflow models trained on labeled training dataset  
:ballot_box_with_check: Implement with Keras  
    - :ballot_box_with_check: Implement data augmentation on training data import  
    - :ballot_box_with_check: Implement techniques to deal with Imbalanced dataset (SMOTE, class weighting, bias initialization, etc.)  
    - :sparkle: Implement more sophisticated network architectures  
:sparkle: Train, test, retest, train some more, test, train, test, test, test a bit more, train some, test, try something new, test, train, test, train, train, test, ...  
:sparkle: Once accuracy scores are satisfying, proceed with BackTrader integration  
:sparkle: Train, test, retest, until profitable strategy produced  
:sparkle: Implement in live trading scenario  
:sparkle: ???  
:sparkle: Profit (*Lord willing*)  

### Annotation Tool
*See [annotate/ dir](annotate) for more details*

(4/3/20) - v1.0 of Annotation Tool is now complete. Below is a screenshot of the Ui in its current state.

The next step is to label a bunch fo training data.

![Screenshot of Annotation Tool](https://i.imgur.com/CwPKsxR.png)

### Label Training Data
Starting with the past 5 years of 1H EUR/USD data (pulled from [forexsb.com](https://forexsb.com/historical-forex-data)). 
From Jan 1st 2015 to Jan 1st 2020.

-----------------------------

### Update 4/6/20 - Saving Models + Configurations
Not only implemented saving/loading of models, but also implemented a simple configuration management 
system. These configurations contain all the settings of a given model, including everything from hyperpatameters 
to the data import arguments applied to the training dataset. 

Below is an example of a saved config file. They are stored in the highly-transportable JSON format. 
You will notice the already quite large set of configuration options available.

**Example**

``` json
{
    "training_data_fp": "annotations.json",
    "hidden_units": [
        512,
        768,
        128
    ],
    "model_name": "keras_smote",
    "learning_rate": 1e-05,
    "label_radius": 3,
    "save_every_x_steps": 10000,
    "n_classes": 4,
    "labels": [
        "0",
        "1",
        "2",
        "3"
    ],
    "str_labels": {
        "HOLD": "0",
        "LONG": "1",
        "SHORT": "2",
        "CLOSE": "3"
    },
    "epochs": 2000,
    "dropout": 0.2,
    "window_size": 80,
    "batch_size": 1024,
    "num_parallel_calls": 12,
    "inherits_from": [],
    "test_split": 0.15,
    "shuffle": true,
    "seed": 1337,
    "truncate": true,
    "include_time": false,
    "compute_weight_ratios": false,
    "optimizer": "adam",
    "as_df": true,
    "log_volume": false,
    "log_per_window": true,
    "numeric_labels": true,
    "augment": {
        "reverse": true,
        "SMOTE": {}
    },
    "use_noise": false,
    "noise_std": 0.075,
    "log_norm_volume": true,
    "norm_all": true
}
```

## Update 4/13/20 - Great progress + with and without SMOTE resampling comaprison
Implemented [SMOTE - Synthetic Minority Over-sampling Technique](https://arxiv.org/pdf/1106.1813.pdf)
to help deal with the imbalanced dataset we are dealing with in this project. 

Below is a confusion matrix of two identical training sessions, only difference being the second run used 
SMOTE resampling to synthetically increase the number of minority classes in the data (ie. LONG, SHORT, and CLOSE classes)

|                             |                         No SMOTE, yes Class Weights                         |                        Yes SMOTE, no Class Weights                        |
|:---------------------------:|:---------------------------------------------------------------------------:|:-------------------------------------------------------------------------:|
|     Bias Init Comparison    |                   ![loss](https://i.imgur.com/3wtt7yd.png)                  |                  ![loss](https://i.imgur.com/tWbVDFr.png)                 |
|           Metrics           |                 ![metrics](https://i.imgur.com/SS2DtPK.png)                 |                ![metrics](https://i.imgur.com/cjt8W2v.png)                |
| Confusion Matrix (Test Set) | ![confusion matrix - only class weighting](https://i.imgur.com/PHlyjrL.png) | ![confusion matrix - SMOTE oversampling](https://i.imgur.com/YiUprDi.png) |

## Update 4/27/20 - Even more progress + Custom Layer Class
Been making great progress on improving accuracy of the model.

As well, I've also added a new layer class, to make it easy to add and update layers, *even right from the .config file!*

*Example Config File*


``` json
{
    "training_data_fp": "annotations2.json",
    "layers": [
        {
            "type": "hidden",
            "units": 256,
            "activation": "softplus"
        },
        {
            "type": "noise",
            "noise_std": 0.09
        },
        {
            "type": "hidden",
            "units": 512,
            "activation": "softplus"
        },
        {
            "type": "noise",
            "noise_std": 0.09
        },
        {
            "type": "hidden",
            "units": 1024,
            "activation": "tanh"
        }
    ],
    "model_name": "keras_smote_test",
    "output_activation": "softmax",
    "learning_rate": 1e-05,
    "label_radius": 3,
    "save_every_x_steps": 10000,
    "n_classes": 4,
    "labels": [
        "0",
        "1",
        "2",
        "3"
    ],
    "str_labels": {
        "HOLD": "0",
        "LONG": "1",
        "SHORT": "2",
        "CLOSE": "3"
    },
    "epochs": 5,
    "dropout": 0.2,
    "window_size": 80,
    "batch_size": 2048,
    "num_parallel_calls": 12,
    "inherits_from": [],
    "test_split": 0.15,
    "shuffle": true,
    "seed": 1337,
    "truncate": true,
    "include_time": false,
    "compute_weight_ratios": false,
    "optimizer": "adam",
    "as_df": true,
    "log_volume": false,
    "log_per_window": true,
    "numeric_labels": true,
    "augment": {
        "reverse": true,
        "SMOTE": {}
    },
    "use_noise": false,
    "noise_std": 0.09,
    "early_stopping": {
        "monitor": "val_auc",
        "min_delta": 0.0001,
        "verbose": 1,
        "patience": 200,
        "mode": "max",
        "restore_best_weights": true
    },
    "config_path": "util/configs/keras_smote_test/",
    "log_norm_volume": true,
    "norm_all": true,
    "plot_path": "util/configs/keras_smote_test/plots"
}
```

## Update 5/1/20 - finTA'stic Indicators!
Up until now, I've been training models using only TOHLCV market data (Time, Open, High, Low, Close, Volume). 
While this data has provided a good starting point, I want to include more features for the model to 
learn from. 

Financial indicators are an excellent way to provide more features, as they only require the input of OHLC[V] data. 
Thus, computing the indicator values, and including into the observation space, can be done easily in 
both training, and inference scenarios. 

The wonderful package `finTA` has been implemented into the project, with a custom Indicator class provided 
to allow for configuration manager compatibility. All 75+ indicators are supported. Adding indicator's 
to training data is as simple as adding the following to the config

``` python
...
indicators=[
    Indicator('SMA', period=24),  # Simple Moving Average
    Indicator('AO', slow_period=34, fast_period=5),  # Awesome Ocillator
    Indicator('OBV', norm=True),  # On Balance Volume
    Indicator('MACD'),   # Moving Average Crossover
    ...
]
...
```

Indicators are saved to the config file in the form

``` json
...
"indicators": [
    {
        "type": "sma",
        "_attr_name": "SMA",
        "norm": false,
        "norm_method": "minmax",
        "period": 24,
        "indicator_kwwargs": {
            "period": 24
        }
    },
    {
        "type": "ao",
        "_attr_name": "AO",
        "norm": false,
        "norm_method": "minmax",
        "slow_period": 34,
        "fast_period": 5,
        "indicator_kwwargs": {
            "slow_period": 34,
            "fast_period": 5
        }
    },
    {
        "type": "obv",
        "_attr_name": "OBV",
        "norm": true,
        "norm_method": "minmax",
        "feature_range": [
            -1,
            1
        ],
        "indicator_kwwargs": {}
    },
    {
        "type": "macd",
        "_attr_name": "MACD",
        "norm": false,
        "norm_method": "minmax",
        "indicator_kwwargs": {}
    },
],
...
```

### With and Without Indicators
Trained  two equally configured models, with one having indicators, and one not having indicators.

Results show a marked improvement in the model with indicators included.

Indicators used: `Simple Moving Average, Awesome Oscillator, On Balance Volume, 
Moving Average Crossover, Ichimoku Cloud, Exponential Moving Average, Normalized 
Buy and Sell Pressure`

|                             |                             No Indicators                            |                             With Indicators                            |
|:---------------------------:|:--------------------------------------------------------------------:|:----------------------------------------------------------------------:|
|      Avg Time per Epoch     |                              ~25 seconds                             |                               ~81 seconds                              |
|           Metrics           |              ![metrics](https://i.imgur.com/caKXpEk.png)             |               ![metrics](https://i.imgur.com/KAOU1hs.png)              |
| Confusion Matrix (Test Set) | ![confusion matrix - no indicators](https://i.imgur.com/F6mTvkr.png) | ![confusion matrix - with indicators](https://i.imgur.com/rQltTPV.png) |
| Metrics Summary             | ![metrics summary](https://i.imgur.com/T1bMFjz.png)                  | ![metrics summary]( https://i.imgur.com/LFeKwrg.png)                   |

*Important note: Bitbucket is terrible when it comes to markdown formatting. Tons of caveats and 
ways in which Bitbucket does things differently (and worse) than the typical standard. Shame bitbucket, shame*

#### Key Takeaways
Two key results from adding indicators: 1) Converges to better, lower cost, solution. 2) Converges 
significantly faster than without indicators.

This is the expected outcome, as the indicator values add additional, correlated, 
data to the observation space, giving the model additional relevant characteristics 
(features) of the market. 

## Update 5/4/20 - Things are getting cool... 
There are some really cool ideas I've come up with to, potentially drastically, improve the 
current model's accuracy. 

*Changes will be committed to a private repo from here on out. Want to avoid others taking the 
process and potentially eradicating the market inefficiency ;)*
