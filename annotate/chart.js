/* CONFIG */
var WINDOW_SIZE = 400;  // number of bars in chart window
var STEP_SIZE = 20;  // number of bars to increment on scroll forward/back
var START_POSITION = 'beginning';  // beginning = start position at index 0, end = start position at index -1
/* END CONFIG */


var chart = null;
var reader = null;
var ctrl_down = null;
var position_open = false;

/* Keep track of CTRL key status */
$(document).keydown (function(evt) {
    if(evt.which === 17) {
        // CTRL key pressed
        ctrl_down = true;
    }
}).keyup(function(evt) {
    if(evt.which === 17) {
        // CTRL key released
        ctrl_down = false;
    }
});

document.addEventListener( 'wheel', ( evt ) => {
	var direction = evt.deltaY;
	var scroll_down = false;
	if (direction > 0) {
		scroll_down = true;
	}
	
	if(!scroll_down) {
		reader.up();
	} else {
		reader.down();
	}
}, { capture: false, passive: true });

function range(start, count) {
    // source: https://stackoverflow.com/a/19506234
    return Array.apply(0, Array(count))
        .map((element, index) => index + start);
}

function Reader(data, window_size, step_size, start_at, load_bars, alt_start_date) {
	this.istart = 0;
	this.iend = 0;
	this._data = data;
	this.bars = [];
	this.bars_mapping = {};
	this.total_bars = 0;
	this.win_size = window_size;
	this.window = [];
	this.step = step_size;
	this._updating = false;
	this.labeled = [];

	if(!start_at) {
	    this.start_at = 'beginning'
    } else {
	    this.start_at = start_at
    }

	var self = this;

	this.processData = function(d) {
		/* Processes csv market data and saves to the bars dataset */
		var out = [];
		$.each(d, function(index, value) {
			value['Time'] = new Date(value['Time']);
			var to_array = [value['Time'], value['Open'], value['High'], value['Low'], value['Close'], value['Volume']];
			out.push(to_array);
			self.total_bars++;

			// index lookup
            self.bars_mapping[Date.parse(value['Time'])] = index;
		});
		self.bars = out;
	};

	this.update_annotation_total = function() {
	    $('#total').html(self.labeled.length);
    };

	this.update_most_recents = function() {
	    /* Update the most-recent textarea with latest labels */
        let out_str = '';
        let counter = 0;

        for(let i = self.labeled.length-6;i < self.labeled.length;i++) {
            if(i < 0) {
                continue
            }

            let labeled = self.labeled[i];
            out_str += labeled[0] + ' - ' + labeled[1];

            if(counter < 5) {
                out_str += '\n';
            }
            counter++;
        }

        $('#most-recent').val(out_str);

        // update annotation total
        self.update_annotation_total();
    };

	this.color_labeled_bars = function() {
	    return;
        let times = [];

        $.each(self.window.kc, function(index, value) {
            times.push(value[0].toString());
        });

	    for(let key in self.labeled) {
            if (times.indexOf(key.toString()) > -1) {
                // a labeled bar is currently in view of window
                let label = self.labeled[key];
                let bar_index = times.indexOf(key);  // index of the bar as it's currently viewed in window
                let chart_bar = document.querySelectorAll('[data-ac-wrapper-id="'+bar_index+'"]')[0];

                if(label === 'LONG') {
                    chart_bar.style = 'fill: darkgreen';
                } else if(label === 'SHORt') {
                    chart_bar.style = 'fill: darkred';
                } else if(label === 'CLOSE') {
                    chart_bar.style = 'fill: darkgrey';
                }
            }
        }
    };

    this.up = function() {
		if(self.iend+self.step >= self.total_bars) {
			// at end of dataset, do nothing
			return;
		}

		self._updating = true;

		self.istart += self.step;
		self.iend += self.step;

		$.each(range(0, self.step), function() {
		    self.window.remove(0);
        });

        self.window.append(...self.bars.slice(self.iend-self.step, self.iend));

        self.color_labeled_bars();

		self._updating = false;
	};

	this.down = function() {
		if (self.istart-self.step < 0) {
			// at start of dataset, do nothing
			return;
		}

		self._updating = true;

		self.istart -= self.step;
		self.iend -= self.step;

		let remove_idx = range(self.win_size-self.step, self.step);
		$.each(remove_idx, function(index, value) {
		    self.window.remove(self.win_size-index-1);
        });
        let to_insert = self.bars.slice(self.istart, self.istart + self.step);
        for (var i = to_insert.length-1;i >= 0;i--) {
            self.window.insert(to_insert[i], 0)
        }

        self.color_labeled_bars();

		self._updating = false;
	};

	this.seek_to_date = function(date) {
        /* NOT FULLY IMPLEMENTED */

	    let index = self.bars_mapping[Date.parse(date)];
        self.seek(index);
    };

	this.seek = function(index) {
	    /* NOT FULLY IMPLEMENTED */

        // let new_slice = self.bars.slice(index-self.win_size, index);  // new window

        // push new_slice last-first into window dataset by inserting into index 0
        // and removing last index from window
        // *This is to supplement for anychart.js's lack of ability to clear dataset and refill*
        // $.each(new_slice, function() {
        //     self.window.remove(self.window.getRowsCount() - 1);
        // });
        // self.window.append(...new_slice);
        // $.each(new_slice.reverse(), function(index, new_bar) {
        //     self.window.remove(self.win_size-1);
        //     self.window.insert(new_bar, 0);
        // });

        chart.removeSeriesAt(0);
        self.window = anychart.data.set(self.bars.slice(index-self.win_size, index));

        // create a japanese candlestick series and set the data
        var series = chart.candlestick(self.window);
        series.pointWidth(14);

        reader.update_most_recents();
    };

	this.save = function() {
        window.location = "data:application/octet-stream," + encodeURIComponent(JSON.stringify(self.bars));
    };

	this.download = function(text, name, type) {
        var a = document.getElementById("aaa");
        var file = new Blob([text], {type: type});
        a.href = URL.createObjectURL(file);
        a.download = name;
    };

	let position_override = null;
	if(load_bars != null) {
        // load bars from json string
        this.bars = JSON.parse(load_bars);

        self.total_bars = this.bars.length;

        // index lookup
        $.each(this.bars, function(index, value) {
            self.bars_mapping[Date.parse(value[0])] = index;
            value[0] = new Date(value[0]);
            self.bars[index] = value;

            // check if bar has been labeled
            if(value.length > 6) {
                // add to labeled array
                self.labeled.push([value[0], value[6]])
            }
        });

        // determine position of last annotation
        $.each(self.bars.reverse(), function(index, value) {
            if (value.length > 6) {
                position_override = self.bars.length - index;
                return false;  // break out of jquery loop
            }
        });
        self.bars.reverse();  // flip back to original order (.reverse() is in-place
    } else {
	    this.processData(this._data);
    }

	// init window indices
    if(this.start_at.charAt(0).toLowerCase() === 'b'){
        // start at beginning of dataset
    	this.istart = 0;
        this.iend = this.win_size;
    } else {
        // start at end of dataset
        this.istart = this.total_bars - this.win_size;
        this.iend = this.total_bars-1;
    }

    if(position_override !== null && alt_start_date === undefined) {
        this.iend = position_override;
        this.istart = position_override-this.win_size;
    } else if(alt_start_date !== undefined) {
        let date_ms = Date.parse(alt_start_date);
        let index = undefined;
        while(index === undefined) {
            index = this.bars_mapping[date_ms];
            date_ms += 3600000;  // 1 hour
        }

        this.iend = index;
        this.istart = index-this.win_size;
    }
    if(this.istart < 0) {
        this.istart = 0;
        this.iend = this.win_size;
    } else if(this.iend > this.bars.length) {
        this.istart = this.bars.length-this.win_size;
        this.iend = this.bars.length;
    }

	this.window = anychart.data.set(this.bars.slice(this.istart, this.iend));
}


function chart_clicked(evt) {
	// let marker = evt.eventMarket;

	var index = evt.iterator.getIndex();
	var row = reader.window.row(index);

	if (ctrl_down) {
		// short
		if(position_open) {
			// close out
			row.label = "CLOSE";
			position_open = false;
		} else {
			row.label = "SHORT";
			position_open = true;
		}
	} else {
		// long
		if(position_open) {
			// close out
			row.label = "CLOSE";
			position_open = false;
		} else {
			row.label = "LONG";
			position_open = true;
		}
	}

	// update bars
    reader.bars[reader.bars_mapping[Date.parse(row[0])]] = [row[0], row[1], row[2], row[3], row[4], row[5], row.label];

    // record labeled record
    reader.labeled.push([row[0], row.label]);

	reader.window.row(index, row);

	reader.color_labeled_bars();
	reader.update_most_recents();
}

function dl() {
    reader.download(JSON.stringify(reader.bars), 'annotations.json', 'text/plain')
}

function delete_last_annotation() {
	let removed = reader.labeled.pop();
	let bar_index = reader.bars_mapping[Date.parse(removed[0])];
	let bar = reader.bars[bar_index];
	reader.bars[bar_index] = [bar[0], bar[1], bar[2], bar[3], bar[4], bar[5]];  // omit the label

	if(removed[1] === 'SHORT' || removed[1] === 'LONG') {
		// reset open position flag
		position_open = false;
	} else {
		position_open = true;
	}
	reader.update_most_recents();
}

function read_file(json_string, date) {
    if (json_string === undefined) {
        json_string = this.result
    }

    reader = new Reader(null, WINDOW_SIZE, STEP_SIZE, START_POSITION, json_string, date);

    // remove current chart and replace
    $('#container').empty();

    // create a chart
	var chart = anychart.candlestick();

	// set the interactivity mode
	chart.interactivity("by-x");

	// create a japanese candlestick series and set the data
    var series = chart.candlestick(reader.window);
    series.pointWidth(14);

    // set the chart title
    chart.title("Forex Market Data");

    // set the titles of the axes
    chart.xAxis().title("Date");
    chart.yAxis().title("Price, $");

    // set the container id
    chart.container("container");

    // set click listener
	chart.listen('pointClick', chart_clicked);

    // initiate drawing the chart
    chart.draw();

    reader.update_most_recents();
}

function show_prompt() {
    let date = prompt("Enter Start Date (mm/dd/yy) -- empty = start at last annotation");
    if(date === '') {
        date = undefined;
    }
    read_file(this.result, date)
}

anychart.onDocumentReady(function() {
    $("#download-annotations").on('click', function() {
        reader.save();
    });

    $('#load-file').on('change', function() {
          var file = this.files[0];
          var fr = new FileReader();
          fr.onload = show_prompt;
          fr.readAsText(file);
          // fr.readAsDataURL(file);
    });

	var csv_data = $.csv.toObjects(csv_text);
	reader = new Reader(csv_data, WINDOW_SIZE, STEP_SIZE, START_POSITION, null);

	// create a chart
	chart = anychart.candlestick();

	// set the interactivity mode
	chart.interactivity("by-x");

	// create a japanese candlestick series and set the data
    var series = chart.candlestick(reader.window);
    series.pointWidth(14);

    // set the chart title
    chart.title("Forex Market Data");

    // set the titles of the axes
    chart.xAxis().title("Date");
    chart.yAxis().title("Price, $");

    // set the container id
    chart.container("container");

    // set click listener
	chart.listen('pointClick', chart_clicked);
	
    // initiate drawing the chart
    chart.draw();
});

// $(document).ready(function(){
//     // Datepicker
//     $('#datepicker').datepicker({
//         // dateFormat: 'yy-mm-dd',
//         // inline: true,
//         // minDate: new Date(2010, 1 - 1, 1),
//         // maxDate:new Date(2010, 12 - 1, 31),
//         altField: '#datepicker_value',
//         onSelect: function(){
//             let date = $("#datepicker").datepicker('getDate').getDate();
//             // var month1 = $("#datepicker").datepicker('getDate').getMonth() + 1;
//             // var year1 = $("#datepicker").datepicker('getDate').getFullYear();
//             // var fullDate = year1 + "-" + month1 + "-" + day1;
//             // var str_output = "<h1><center><img src=\"/images/a" + fullDate +".png\"></center></h1><br/><br>";
//             // $('#page_output').html(str_output);
//             alert(date);
//             reader.seek_to_date(date);
//         }
//     });
// });
