# Annotation Tool
To develop training data for this project, an Annotation Tool has to be developed. 

I decided to try and make my own simple web frontend to display chart data, traverse it, and 
label it with actions. The end result will be a tool where one can load up chart data, and indicate where 
long/short positions should be opened and closed. 

### Current State
:ballot_box_with_check: Load market data from csv  
:ballot_box_with_check: View market data in sliding window  
:ballot_box_with_check: Enable scrolling to control sliding window  
:ballot_box_with_check: Increase scroll step size  
:ballot_box_with_check: Enable labeling of Long/Short/Close actions on market data  
:ballot_box_with_check: Allow for deleting most recent annotation  
:ballot_box_with_check: Enable Saving Annotations to file  
:ballot_box_with_check: Enable Loading from saved Annotations file  
:ballot_box_with_check: Display most recent annotations in UI  


Below is a screenshot of the tool in its current state as of (4/2/20)

![Screenshot of Annotation Tool](https://i.imgur.com/CwPKsxR.png)
