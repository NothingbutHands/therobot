from dateutil import parser
from finta import TA


class Loaded(BaseException):
    pass


class Configable:
    def __init__(self, x):
        self._handle_restore(x)

    def _restore(self, x):
        for k, v in x.items():
            setattr(self, k, v)

    def _handle_restore(self, x):
        # Check if loading from dict
        if type(x) is dict:
            # Loading from to_json() dict
            self._restore(x)
            raise Loaded

    def to_json(self):
        return self.__dict__


class l_AYER(Configable):  # L-AYER!
    def __init__(self, layer_type='hidden', noise_std=0.01, units=256, activation=None, dropout=0.01,
                 recurrent_activation=None, bidirectional=False, temperature=1, kernel_size=(3, 3), stride=(1, 1),
                 **kwargs):
        try:
            super().__init__(layer_type)
        except Loaded:
            return

        if 'u' in kwargs:
            if kwargs['u'] is not None:
                # replace `units` input arg with non-null alias `u`
                units = kwargs['u']

        self.type = str(layer_type).lower().strip()

        if self.type == 'noise':
            self.noise_std = noise_std
        elif self.type == 'hidden':
            self.units = units
            self.activation = activation or 'relu'
        elif self.type == 'dropout':
            self.dropout = dropout
        elif self.type == 'lstm':
            self.units = units
            self.activation = activation or 'tanh'
            self.recurrent_activation = recurrent_activation
            self.bidirectional = bidirectional
        elif self.type == 'gumbel_softmax':
            self.temperature = temperature
        elif self.type == 'locally_connected':
            self.units = units
            self.kernel_size = kernel_size
            self.stride = stride
        else:
            raise ValueError(('"{}" is not a valid layer. Options '
                              'include: hidden, noise, dropout, lstm'
                              'gumbel_softmax'.format(layer_type)))


class Indicator(Configable):
    def __init__(self, indicator_type, norm=False,
                 norm_method='minmax', feature_range=(-1, 1),
                 **kwargs):
        try:
            super().__init__(indicator_type)
        except Loaded:
            return

        self.type = str(indicator_type).strip().lower()
        self._attr_name = self.type.upper()
        self.compute = getattr(TA, self._attr_name)

        self.norm = norm
        self.norm_method = str(norm_method).strip().replace('_', '').replace(' ', '').lower()
        if self.norm:
            if self.norm_method == 'minmax':
                self.feature_range = feature_range

        kwargs = dict(list(filter(lambda x: x[1 is not None], kwargs.items())))  # remove key: None pairs
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.indicator_kwwargs = kwargs

    def _restore(self, x):
        super()._restore(x)
        self.compute = getattr(TA, self._attr_name)

    def to_json(self):
        d = self.__dict__.copy()
        d.pop('compute')
        return d


class Position(Configable):
    """Contains the information of a trade position (aka. round-trip; open trade, close trade)"""
    def __init__(self, open_bar, close_bar, direction, explicit, period=3600):
        """
        Define a discrete round-trip, open-and-close, trade position
        Open and Close bars must be in TOHLC[V] format (Time, Open, High, Low, Close, [Volume])

        :param open_bar: list - bar in which the position is entered (opened)
        :param close_bar: list - bar in which the position is exited (closed)
        :param direction: str - LONG or SHORT
        :param explicit: bool - whether or not the trade was opened and closed by model explicitly
        :param period: int - time period of input bars, in seconds
        """
        try:
            super().__init__(open_bar)
        except Loaded:
            return

        self.open_bar = open_bar
        self.close_bar = close_bar
        self.direction = str(direction).strip().upper()
        self.period = period*1000  # converted to ms
        self.explicit = explicit
        self.profitable = None
        self.duration = None
        self.net_pips = None
        self.open_time = None
        self.close_time = None

        assert self.direction in ['SHORT', 'LONG']

        self._calculate()

    def _calculate(self):
        """Computes the various properties of the position based on the open and close bars"""
        self.open_time = self.open_bar[0]
        self.close_time = self.close_bar[0]
        if type(self.open_time) is str:
            self.open_time = parser.parse(self.open_time).timestamp()*1000
        if type(self.close_time) is str:
            self.close_time = parser.parse(self.close_time).timestamp()*1000

        self.duration = (self.close_time - self.open_time) / self.period

        o_close = self.open_bar[4]
        c_close = self.close_bar[4]

        pip_open = int(str(o_close).replace('.', '').ljust(5, '0'))
        pip_close = int(str(c_close).replace('.', '').ljust(5, '0'))

        if self.direction == 'SHORT':
            if o_close > c_close:
                self.profitable = True
            else:
                self.profitable = False

            self.net_pips = pip_open - pip_close
        else:
            # LONG
            if o_close < c_close:
                self.profitable = True
            else:
                self.profitable = False

            self.net_pips = pip_close - pip_open


def parse_market_strategy(market_data):
    for i, row in market_data.iterrows():
        pass