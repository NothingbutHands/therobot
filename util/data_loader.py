import json
import multiprocessing
import random
import sys
import traceback
from collections import Counter
from datetime import datetime
from functools import partial
from multiprocessing import Pool
from multiprocessing.pool import ThreadPool

import jsonlines as jsonlines
import numpy as np
import pandas as pd
from dateutil import parser
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import minmax_scale
from sklearn.utils import class_weight

from util.augment import reverse_dataset, smote_balance

column_names = ['Time', 'Open', 'High', 'Low', 'Close', 'Volume']
label_map = {'HOLD': 0, 'LONG': 1, 'SHORT': 2, 'CLOSE': 3}
label_map_reversed = {v: k for k, v in label_map.items()}


def train_test_split_numpy(arr, test_split):
    msk = np.random.rand(len(arr)) < (1.0 - test_split)
    return arr[msk], arr[~msk]


def augment_label_radius(array, label_radius, numeric_labels):
    current_label = ''
    for i, data in enumerate(array):
        if current_label == 'CLOSE' and data['label'] == 'CLOSE':
            print('Two CLOSE in a raw!', i)
        if current_label == data['label']:
            print('Matching labels {}. {}'.format(current_label, i))

        if numeric_labels:
            # ensure label is in string format
            current_label = label_map_reversed[data['label']]
        else:
            current_label = data['label']

        index = i - 1
        index = 0 if index < 0 else index
        try:
            row_label = array[index]['label']
        except KeyError:
            print(index, len(array))
            sys.exit()
        if current_label in ['LONG', 'SHORT', 'CLOSE'] and row_label not in ['LONG', 'SHORT', 'CLOSE',
                                                                                         1, 2, 3]:
            # change label of the previous & following `label_radius` number of bars, to current label
            l = current_label
            if numeric_labels:
                # flip back to numeric
                l = label_map[current_label]

            for o in range(1, label_radius + 1):
                array[i-o]['label'] = l
                try:
                    array[i+o]['label'] = l
                except IndexError:
                    pass
    return array


def augment_label_radius_df(df, label_radius, numeric_labels):
    current_label = ''
    for i, row in df.iterrows():
        if current_label == 'CLOSE' and row['Label'] == 'CLOSE':
            print('Two CLOSE in a raw!', i)
        if current_label == row['Label']:
            print('Matching labels {}. {}'.format(current_label, i))

        if numeric_labels:
            # ensure label is in string format
            current_label = label_map_reversed[row['Label']]
        else:
            current_label = row['Label']

        index = i - 1
        index = 0 if index < 0 else index
        try:
            row_label = df.at[index, 'Label']
        except KeyError:
            print(index, len(df))
            sys.exit()
        if current_label in ['LONG', 'SHORT', 'CLOSE'] and row_label not in ['LONG', 'SHORT', 'CLOSE',
                                                                                         1, 2, 3]:
            # change label of the previous & following `label_radius` number of bars, to current label
            l = current_label
            if numeric_labels:
                # flip back to numeric
                l = label_map[current_label]

            for o in range(1, label_radius + 1):
                df.at[i-o, 'Label'] = l
                try:
                    df.at[i+o, 'Label'] = l
                except IndexError:
                    pass


def augmentation(train, test, validate, augment, numeric_labels, include_time=False):
    if augment:
        if 'SMOTE' in augment:
            print('Resampling train dataset with SMOTE ...')
            labels = train.pop('Label')
            df1 = train[train.isna().any(axis=1)]
            print(df1)
            print('len train', len(train))
            x, y = smote_balance(train.values.tolist(), labels, **augment['SMOTE'])
            train = to_df([{'data': row, 'label': lbl} for row, lbl in zip(x, y)], include_time=include_time)

    print('train head', train.head())
    return train, test, validate


def _to_df_wrapper(tr, evaluate=False):
    trd = tr['data']
    trd = np.array(trd, dtype='float32').flatten()

    if not evaluate:
        # include label when training model
        trl = tr['label']
        trd = np.concatenate([trd, np.array([trl])])

    return trd.tolist()


def to_df(d, include_time=False, evaluate=False, low_memory=False):
    column_length = np.array(d[0]['data']).flatten().shape[0]
    if evaluate:
        column_length += 1

    col_names = ['V{}'.format(i) for i in range(column_length)]
    if include_time:
        col_names = ['Time'] + col_names[:-1]

    if not evaluate:
        col_names += ['Label']

    func = partial(_to_df_wrapper, evaluate=evaluate)

    print('Converting final dataset, please wait ...')
    if low_memory:
        # perform without multiprocessing
        tpool = ThreadPool(processes=4)
        out = list(tpool.imap(func, d, chunksize=1024))
        tpool.close()
    else:
        pool = Pool(processes=multiprocessing.cpu_count())
        out = list(pool.imap(func, d, chunksize=1024))
        pool.close()
    print('Conversion complete!')

    return pd.DataFrame(out, columns=col_names).reset_index(drop=True)


def tt_split(df, test_size):
    t, _t = train_test_split(df, test_size=test_size)
    return t.reset_index(drop=True), _t.reset_index(drop=True)


def label_distribution(filepath):
    with open(filepath) as f:
        bars = json.load(f)

    labels = ['CLOSE', 'HOLD', 'LONG', 'SHORT']
    c = []
    for bar in bars:
        if str(bar[-1]).upper() in labels:
            c.append(str(bar[-1]).upper())
        else:
            c.append('HOLD')

    counter = Counter(c)
    return len(c), counter['HOLD'], counter


def log_norm_volume_func(d):
    # Convert to log-space.
    eps = 0.001  # 0 => 0.1¢
    _data = np.array(d)
    _data[:, -1] = np.log(_data[:, -1].astype('float32') + eps)
    return _data.tolist()


def process_bar(data, bars=None, up_to=1, numeric_labels=False, log_norm_volume=False,
                min_max_tohlcv=False, include_time=False, evaluate=False, bar_count=0):
    bar = data[1]
    i = data[0]
    if len(bar) > up_to:
        # labeled datapoint
        label = bar[-1]
    else:
        label = 'HOLD'

    if numeric_labels and not evaluate:
        try:
            label = label_map[label]
        except KeyError as e:
            print(type(e), str(e))
            print(traceback.format_exc())
            print('Failed to find label for value {}. Did you mean to pass evaluate=True?'.format(label))
            sys.exit(1)

    data = bars[i:i + bar_count]
    data = [d[1:up_to] for d in data]

    if log_norm_volume and not min_max_tohlcv:
        # Normalize volume column
        data = log_norm_volume_func(data)

    if not data:
        print('NO DATA ', data, i - bar_count + 1, i + 1)

    if min_max_tohlcv:
        d = np.asarray(data, dtype='float32')

        if include_time:
            to = 6
        else:
            to = 5

        d[:, :to] = minmax_scale(d[:, :to], axis=0, feature_range=(-1, 1))
        data = d.tolist()

    if not include_time:
        # remove time column
        data = [d[1:] for d in data]

    return {'data': data, 'label': label}


def easy_import_data(cfg, evaluate=False):
    """Wrapper functions for import_data() for convenience
    Takes in a config dict as input"""
    ret = import_data(
        cfg.training_data_fp, bar_count=cfg.window_size,
        test_split=cfg.test_split, shuffle=cfg.shuffle, seed=cfg.seed,
        truncate=cfg.truncate, include_time=cfg.include_time,
        compute_weight_ratios=cfg.compute_weight_ratios,
        as_df=cfg.as_df, log_norm_volume=cfg.log_norm_volume,
        numeric_labels=cfg.numeric_labels,
        min_max_tohlcv=cfg.norm_all, augment=cfg.augment,
        indicators=cfg.indicators, track_position_state=cfg.track_position_state,
        evaluate=evaluate,
    )
    if type(ret) is pd.DataFrame:
        return ret, None, None, None
    if len(ret) == 3:
        return (*ret, None)
    return ret


def import_data(
        filepath,
        bar_count=10,  # number of bars per data point
        test_split=.2,  # percentage of total to holdout for eval
        shuffle=True,  # whether to shuffle the dataset or not
        seed=1337,  # how to grow a dairy farm
        truncate=True,  # whether to truncate dataset to include up to last annotation
        include_time=False,  # include time as feature
        compute_weight_ratios=False,
        as_df=False,  # whether or not to convert the list output as dataframe
        log_norm_volume=False,
        log_per_window=True,  # whether or not to norm using whole dataset or just for each window
        numeric_labels=False,
        min_max_tohlcv=True,
        augment=None,  # specify data augmentation operations. eg. {'reverse': True}}
        indicators=tuple(),
        track_position_state=True,
        evaluate=False,  # whether or not the data will be used for model evaluation
):
    random.seed(seed)
    np.random.seed(seed)

    if filepath.strip().lower().endswith('.csv'):
        # load un-labeled market data
        df = pd.read_csv(filepath, sep='\t')
        bars = df.values.tolist()
        print(bars[0])
    else:
        # load annotated market data
        bars = json.load(open(filepath))
    out = []  # final dataset

    # for computing technical indicators
    ohlcv = pd.DataFrame(
        [bar[1:6] for bar in bars],
        columns=['open', 'high', 'low', 'close', 'volume'],
        dtype='float32'
    )

    last_annotation = 0  # index of last annotation
    first_annotation = 0
    if include_time:
        bars = [[parser.parse(bar[0]).timestamp()*1000]+bar[1:] for bar in bars]

    labels_list = []
    for bar in bars:
        if str(bar[-1]).strip().upper() in ['LONG', 'SHORT', 'CLOSE']:
            labels_list.append(str(bar[-1]).strip().upper())
        else:
            labels_list.append('HOLD')

    ##############
    # INDICATORS #
    ##############

    if indicators:
        np_bars = np.array([bar[1:6] for bar in bars])  # ohlcv only
        delays = set()  # keep track of start periods which do not have a computed value
        for indicator in indicators:
            # execute the indicator function on the open-high-low-close-volume data
            indicator_values = indicator.compute(ohlcv, **indicator.indicator_kwwargs).to_numpy(dtype='float32')
            if indicator_values.ndim == 1:
                # convert to column vector
                indicator_values = indicator_values.reshape(indicator_values.shape[0], 1)

            if indicator.norm:
                # apply normalization
                if indicator.norm_method == 'minmax':
                    indicator_values = minmax_scale(indicator_values, axis=0, feature_range=indicator.feature_range)

            total_values = indicator_values.size
            nulls = np.count_nonzero(~np.isnan(indicator_values))
            print('{}/{} nulls'.format(total_values-nulls, total_values))

            x = 0
            for v in indicator_values[:, 0]:
                if str(v) != 'nan': break
                else: x += 1
            delays.add(x)

            indicator_values[np.isnan(indicator_values)] = 0.0  # replace nan with 0.0
            np_bars = np.append(np_bars, indicator_values, axis=1)

        # add in time and label
        bars = np.append(np.array([[b[0]] for b in bars]), np_bars, axis=1).tolist()
        for i, bar in enumerate(bars):
            if labels_list[i] != 'HOLD':
                bar.append(labels_list[i])

        highest_delay = max(list(delays))
        print(('Removing {} starting bars where at least 1 indicator '
               'lacks sufficient periods to compute.'.format(highest_delay+1)))
        bars = bars[highest_delay:]

    if track_position_state:
        # include 0/1 flag defining whether a position is open or not
        position_open = False
        p_flags = []  # position indicator flags

        # flags
        pos_open = 1.
        no_pos = 0.

        for i, bar in enumerate(bars):
            label = str(bar[-1]).strip().upper()
            if label in ['LONG', 'SHORT', 'CLOSE']:
                if label in ['LONG', 'SHORT']:
                    # sanity check
                    length = len(bar)
                    for b in bars[i+1:]:
                        if len(b) == length:
                            assert str(b[-1]).strip().upper() == 'CLOSE'
                            break

                position_open = True
                p_flags.append(pos_open)
                bar.insert(1, pos_open)
            elif position_open:
                p_flags.append(pos_open)
                bar.insert(1, pos_open)
            else:
                p_flags.append(no_pos)
                bar.insert(1, no_pos)

            if label == 'CLOSE':
                position_open = False

    print('\nPeak at data before pre-processing ...\n\t1337 Bar: \n\t\t{}\n'.format(bars[1337]))

    up_to = max([len(bar) for bar in bars])-1
    # For each bar in the dataset, create a window of the previous bar_count number of
    # bars before it. Mimics the typical live-view of market data, where you're looking at the
    # most recently closed bar, and the previous x bars before it
    for i, bar in enumerate(bars[bar_count-1:]):
        if len(bar) > up_to:
            # labeled datapoint
            last_annotation = i

            if not first_annotation:
                first_annotation = i

    pool = Pool(processes=multiprocessing.cpu_count())
    func = partial(process_bar, bars=bars,  up_to=up_to, numeric_labels=numeric_labels, log_norm_volume=log_norm_volume,
                   min_max_tohlcv=min_max_tohlcv, include_time=include_time, evaluate=evaluate, bar_count=bar_count)
    out = list(pool.map(func, enumerate(bars), chunksize=1024))
    pool.close()

    if evaluate:
        return to_df(out, include_time=include_time)

    if truncate:
        # remove everything after last annotation
        out = out[first_annotation-(bar_count*2):last_annotation+1]

    if augment:
        print('Performing augmentation: {}'.format(list(augment.keys())))
        if 'reverse' in augment:
            reversed_out = reverse_dataset(out, time_included=include_time, numeric_labels=numeric_labels)
            out = out + reversed_out

        if 'label_radius' in augment:
            label_radius = augment['label_radius']['radius']
            out = augment_label_radius(out, label_radius, numeric_labels)

    if shuffle:
        random.shuffle(out)

    out = np.array(out)

    if as_df:
        df = to_df(out, include_time=include_time)
        train, test = tt_split(df, test_size=test_split)
        train, validate = tt_split(train, test_size=test_split)
        train, test, validate = augmentation(train, test, validate, augment, numeric_labels, include_time)

        print('Test')
        compute_ratios(test)
        print('\nValidate')
        compute_ratios(validate)

        if compute_weight_ratios:
            print('\nTrain')
            class_weights = compute_ratios(train)
            return train, test, validate, class_weights

        return train, test, validate

    train, test = train_test_split_numpy(out, test_split)
    train, validate = train_test_split_numpy(train, test_split)
    train, test, validate = augmentation(train, test, validate, augment, numeric_labels, include_time)

    if compute_weight_ratios:
        class_weights = compute_ratios(train, )
        return train, test, validate, class_weights

    return train, test, validate


def compute_ratios(train):
    train_labels = list(train['Label'].values)
    c = Counter(train_labels)
    print('label distrib:', c.most_common())
    return class_weight.compute_class_weight(
        'balanced',
        np.unique(train_labels),
        train_labels
    )


def jsonl_write(obj, fp):
    with jsonlines.open(fp, 'w') as writer:
        writer.write_all(obj)


def prepare_and_write_training_file(annotation_filepath, **kwargs):
    train, test, validate = import_data(annotation_filepath, **kwargs)
    rundate = datetime.now().strftime('%F_%H:%M')

    train_fp = 'training/train_dataset_{}.jsonl'.format(rundate)
    test_fp = 'training/test_dataset_{}.jsonl'.format(rundate)
    jsonl_write(train, train_fp)
    jsonl_write(test, test_fp)

    return train_fp, test_fp
