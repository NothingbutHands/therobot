from collections import Counter

import numpy as np
from imblearn.over_sampling import SMOTE

action_labels = ('HOLD', 'LONG', 'SHORT', 'CLOSE')
labels_map_reversed = {0: 'HOLD', 1: 'LONG', 2: 'SHORT', 3: 'CLOSE'}
labels_map = {v: k for k, v in labels_map_reversed.items()}


def reverse_window(window, label, preceding_label=None, time_included=False):
    """Class preserving. Takes a bar window as input and reverses the order. Does alter labels.
    In addition: it flips trade positions: LONG+CLOSE -Reversed-> SHORT+CLOSE
    Specify `subset` as a list of labels to apply the augmentation to"""
    action_map = {'HOLD': 'HOLD', 'LONG': 'CLOSE', 'SHORT': 'CLOSE'}

    if type(window) is not np.ndarray:
        window = np.array(window)

    if time_included:
        time = window[:, 0]
        window = np.concatenate([time.reshape(time.shape[0], 1), window[::-1, 1:]], axis=1)
    else:
        window = window[::-1, :]

    if preceding_label and label == 'CLOSE':
        new_label = 'LONG'
        if preceding_label == 'LONG':
            new_label = 'SHORT'
    else:
        new_label = action_map[label]

    # if label != 'HOLD':
    #     print('p label: {} old label: {} new label: {}'.format(preceding_label, label, new_label))

    return window, new_label


def reverse_dataset(dataset, time_included=False, numeric_labels=False):
    """dataset format: [{'data': data, 'label': label}, ...]"""
    preceding_label = None
    out = []
    for data in dataset:
        label = data['label']
        if numeric_labels:
            label = labels_map_reversed[label]

        new_data, new_label = reverse_window(data['data'], label, preceding_label=preceding_label,
                                             time_included=time_included)
        if label != 'HOLD':
            preceding_label = label

        if numeric_labels:
            # flip back to numeric
            new_label = labels_map[new_label]

        out.append({'data': new_data, 'label': new_label})
    return out[::-1]


def jitter_window(window, epsilon=.001):
    """Class preserving. Takes a bar window as input and 'jitters' each bar and returns output
    Does not alter any labels"""
    pass


def smote_balance(x, y, random_state=42, **kwargs):
    if 'sampling_strategy' in kwargs:
        strat = kwargs['sampling_strategy']
        # convert float values to total number of labels
        total = len(x)
        for k, v in strat.items():
            strat[k] = int(total * v)
        kwargs['sampling_strategy'] = strat

    smote = SMOTE(random_state=random_state, **kwargs)
    c = Counter(y)
    print('Distribution before: {}'.format(c.most_common()))
    x, y = smote.fit_resample(x, y)
    c = Counter(y)
    print('New distribution: {}'.format(c.most_common()))
    return x, y
