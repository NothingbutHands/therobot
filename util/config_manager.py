import json
import os
import shutil
import sys
import time
from json import JSONDecodeError

from attrdict import AttrDict
from tensorflow import keras

from util.utils import l_AYER, Indicator

default_settings = dict(
    training_data_fp='annotations.json',
    layers=[512, 256],
    model_name='genesis',
    output_activation='relu',  # activation method used for output layer
    learning_rate=10**-8,
    label_radius=2,  # number of bars before/after to include in LONG/SHORT/CLOSE annotation, 1 = disabled
    save_every_x_steps=100,  # save model checkpoint every x steps
    n_classes=4,  # long, short, close, hold
    labels=['0', '1', '2', '3'],
    str_labels=dict(HOLD='0', LONG='1', SHORT='2', CLOSE='3'),
    epochs=100,
    dropout=.2,  # estimator dropout
    window_size=80,  # total number of input bars
    batch_size=2048,  # number of training examples per step
    num_parallel_calls=12,
    inherits_from=[],
    test_split=.15,  # percentage of total to holdout for eval
    shuffle=True,  # whether to shuffle the dataset or not
    seed=1337,  # how to grow a dairy farm
    truncate=True,  # whether to truncate dataset to include up to last annotation
    include_time=False,  # include time as feature
    track_position_state=True,  # whether or not to include a flag (0=no, 1) to indicate if a position is currently open
    compute_weight_ratios=True,  # compute weight ratios and apply weight_column
    optimizer='adam',
    as_df=False,
    log_volume=False,  # whether or not to normalize the volume column
    log_per_window=True,  # whether or not to norm using whole dataset or just for each window
    numeric_labels=False,  # whether or not to return numeric labels or string labels in output
    augment=None,  # specify data augmentation functions to apply to the dataset
    use_noise=False,  # whether or not to include noise layers
    early_stopping=dict(
        monitor='val_auc',  # value to monitor for early stopping
        min_delta=10 ** -5,  # minimum improvement required
        verbose=1,  # output verbosity
        patience=100,  # wait x epochs before determining if stop eraly
        mode='auto',  # mode to evaluate 'monitor' value. max = maximize value, min = minimize value
        restore_best_weights=True
    ),  # Keras early stopping configurations
    feature_length=5,  # OHLC + Volume -- Do not edit: auto updated based on configuration
    indicators=(),  # iterable of Indicator class objects
)


optimizer_lookup = {
    'adam': keras.optimizers.Adam,
    'adagrad': keras.optimizers.Adagrad,
}


def save_config(name, config):
    out_dir = os.path.normpath('util/configs/{0}'.format(name))
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    with open(os.path.normpath('{}/{}.config'.format(out_dir, name)), 'w') as f:
        print(config)
        json.dump(config, f, indent=4, default=lambda x: x.to_json())


def create_default_config():
    save_config('default', default_settings)
    return AttrDict(default_settings)


def update_feature_length(c):
    """Sets the feature_length value based on config"""
    if c['include_time']:
        c.update(feature_length=6)
    else:
        c.update(feature_length=5)
    return c


def create_custom_config(name, **overrides):
    """overrides: kwargs to override default_settings
    name: name given to config file written to util/configs/"""
    cfg = default_settings.copy()
    cfg.update(model_name=name, config_path='util/configs/{0}/'.format(name), **overrides)
    cfg.update(plot_path=os.path.join(cfg['config_path'], 'plots'))

    model_dir = 'models/{}/'.format(name)
    model_path = '{}/{}.h5'.format(model_dir, name)

    cfg['inherits_from'] = []
    cfg['model_path'] = model_path
    cfg['model_dir_path'] = model_dir

    update_feature_length(cfg)
    save_config(name, cfg)

    cfg = norm_paths(cfg)
    init_dirs(cfg)

    return AttrDict(cfg)


def init_dirs(cfg):
    time.sleep(.5)
    for k, v in cfg.items():
        if k.endswith('_path'):
            try:
                os.mkdir(v)
            except: pass


def norm_paths(cfg):
    c = cfg.copy()
    for k, v in cfg.items():
        if k.endswith('_path'):
            c[k] = os.path.normpath(v)
    return c


def create_config_from_template(name, template, **overrides):
    if template:
        parent_cfg = load_config(template)
    else:
        # no parent config specified
        return create_custom_config(name, **overrides)

    if 'inherits_from' in overrides:
        print('Warn: inherits_from key specified. This value cannot be overridden. Ignoring ...')
        del overrides['inherits_from']

    template_model_dir = 'models/{}/'.format(template)
    template_model_path = '{}/{}.h5'.format(template_model_dir, template)
    model_dir = 'models/{}/'.format(name)
    model_path = '{}/{}.h5'.format(model_dir, name)

    cfg = parent_cfg.copy()
    cfg.update(**overrides)
    cfg['config_path'] = 'util/configs/{0}/'.format(name)
    cfg['plot_path'] = '{}/plots/'.format(cfg['config_path'])
    cfg['inherits_from'].append(template)
    cfg['model_path'] = model_path
    cfg['model_dir_path'] = model_dir
    cfg['parent_model_path'] = template_model_path

    update_feature_length(cfg)
    save_config(name, cfg)

    cfg = norm_paths(cfg)
    init_dirs(cfg)

    return AttrDict(cfg)


def delete_config(name):
    try:
        time.sleep(.5)
        return shutil.rmtree(os.path.normpath('util/configs/{0}'.format(name)))
    except:
        time.sleep(1.2)
        return shutil.rmtree(os.path.normpath('util/configs/{0}'.format(name)))


def load_config(name):
    with open(os.path.normpath('util/configs/{0}/{0}.config'.format(name))) as f:
        cfg = json.load(f)
        layers = cfg['layers']
        layer_objs = []
        for l in layers:
            layer_objs.append(l_AYER(l))

        indicators = cfg['indicators']
        ind_objs = []
        for i in indicators:
            ind_objs.append(Indicator(i))

        cfg['layers'] = layer_objs
        cfg['indicators'] = ind_objs
        cfg = norm_paths(cfg)

        init_dirs(cfg)

        return AttrDict(cfg)


def config_exists(name):
    fp = os.path.normpath('util/configs/{0}/{0}.config'.format(name))
    if os.path.exists(fp):
        try:
            json.load(open(fp))
            return True
        except JSONDecodeError:
            # thrown when loading a json file which contains no data
            return False
    return False


def config_select_prompt(model_name, inherit_from, **custom_configuration):
    """Checks for a config file, prompts the user to overwrite/etc."""
    overwrite_config = False
    if config_exists(model_name):
        sys.stdout.flush()
        sys.stderr.flush()
        
        if len(sys.argv) > 1 and str(sys.argv[-1]).lower() == '-y':
            print('-y passed, forcing overwrite of existing config "{}"'.format(model_name))
            r = 'y'
        else:
            r = input('Config "{}" already exists, overwrite? [y/N/q]: '.format(model_name))

        if r.lower() == 'q':
            print('Exiting ...')
            sys.exit(0)
        elif r.lower() != 'y':
            # load previous config and set flag to load model checkpoint
            cfg = load_config(model_name)
            cfg['load_model'] = True
            return cfg
        else:
            # Delete previous model checkpoint (if any) and delete  existing config
            try:
                shutil.rmtree(os.path.normpath('models/{}'.format(model_name)))
            except FileNotFoundError:
                pass
            except NotADirectoryError:
                os.remove(os.path.normpath('models/{}'.format(model_name)))
            delete_config(model_name)
            overwrite_config = True

    cfg_exists = config_exists(model_name)
    if overwrite_config or not cfg_exists:
        cfg = create_config_from_template(model_name, inherit_from, **custom_configuration)
        cfg.load_model = False
        if inherit_from:
            cfg.load_model = True
        return cfg
